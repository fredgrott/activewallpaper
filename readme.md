ActiveWallpaper
---

# ActiveWallpaper

ActiveWallPaper is an android library project to help developers develop
Android Live Wallpaper Applications.

# Project Lead

Fred Grott's blog can be found at:

[GrottWorkShop](http://shareme.github.com)

# License

Licensed under Apache 2.0 License


# Credits


