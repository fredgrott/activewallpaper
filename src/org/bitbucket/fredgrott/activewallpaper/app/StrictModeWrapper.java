/*******************************************************************************
 * Copyright 2012 fredgrott
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package org.bitbucket.fredgrott.activewallpaper.app;

import android.os.StrictMode;


/**
 * Strict Mode Wrapper.
 *
 * @author fredgrott
 */
public class StrictModeWrapper {
	   static {
	       try {
	           Class.forName("android.os.StrictMode", true, Thread.currentThread().getContextClassLoader());
	       } catch (Exception ex) {
	           throw new RuntimeException(ex);
	       }
	   }
	 
	   /**
   	 * Check available.
   	 */
   	public static void checkAvailable() {  }
	 
	   /**
   	 * Enable defaults.
   	 */
   	public static void enableDefaults() {
	       StrictMode.enableDefaults();
	    }
}
