/*
 * Copyright 2011 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.bitbucket.fredgrott.activewallpaper.app;

import java.lang.reflect.InvocationTargetException;

import org.bitbucket.fredgrott.activewallpaper.log.LogUtility;




/**
 * The Class ReflectionUtils.
 */
public class ReflectionUtils {
    
	/** The TAG. */
	@SuppressWarnings("unused")
	private static String Tag = LogUtility.setLOGTAG("ReflectionUtils");
	
    /**
     * Try invoke.
     *
     * @param target the target
     * @param methodName the method name
     * @param args the args
     * @return the object
     */
    public static Object tryInvoke(Object target, String methodName, Object... args) {
        Class<?>[] argTypes = new Class<?>[args.length];
        for (int i = 0; i < args.length; i++) {
            argTypes[i] = args[i].getClass();
        }

        return tryInvoke(target, methodName, argTypes, args);
    }

    /**
     * Try invoke.
     *
     * @param target the target
     * @param methodName the method name
     * @param argTypes the arg types
     * @param args the args
     * @return the object
     */
    public static Object tryInvoke(Object target, String methodName, Class<?>[] argTypes,
            Object... args) {
        try {
            return target.getClass().getMethod(methodName, argTypes).invoke(target, args);
        } catch (NoSuchMethodException e) {
        	//TODO
        } catch (IllegalAccessException e) {
        	//TODO
        } catch (InvocationTargetException e) {
        	//TODO
        }

        return null;
    }

    /**
     * Call with default.
     *
     * @param <E> the element type
     * @param target the target
     * @param methodName the method name
     * @param defaultValue the default value
     * @return the e
     */
    @SuppressWarnings("unchecked")
	public static <E> E callWithDefault(Object target, String methodName, E defaultValue) {
        try {
            return (E) target.getClass().getMethod(methodName, (Class[]) null).invoke(target);
        } catch (NoSuchMethodException e) {
        	//TODO
        } catch (IllegalAccessException e) {
        	//TODO
        } catch (InvocationTargetException e) {
        	//TODO
        }

        return defaultValue;
    }
}