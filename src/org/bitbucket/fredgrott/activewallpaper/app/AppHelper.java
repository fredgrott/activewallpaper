/*******************************************************************************
 * Copyright 2012 fredgrott
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package org.bitbucket.fredgrott.activewallpaper.app;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

import org.bitbucket.fredgrott.activewallpaper.cache.GenericStore;
import org.bitbucket.fredgrott.activewallpaper.cache.ImageCache;
import org.bitbucket.fredgrott.activewallpaper.cache.ObjectCache;
import org.bitbucket.fredgrott.activewallpaper.log.AppLogger;







import android.app.Application;
import android.content.Context;
import android.content.pm.ApplicationInfo;

/**
 * The Class AppHelper.
 */
public class AppHelper extends Application {

	
	/** need to initialize,as cannot have blank filed(java5). */
	public static int  androidAPILEVEL = -1;
	
	/** The Constant codenameJELLYBEAN. */
	public static final int  CODENAMEJELLYBEAN = 16;
	
	/** The Constant codenameICS. */
	public static final int CODENAMEICS = 14;
	
	/** The Constant codenameHONEYCOMB. */
	public static final int CODENAMEHONEYCOMB = 11;
	
	/** The Constant codenameGINGERBREAD. */
	public static final int CODENAMEGINGERBREAD =  9;
	
	/** The Constant codenameFROYO. */
	public static final int CODENAMEFROYO = 8;
	
	/** The Constant codenameECLAIR. */
	public static final int CODENAMEECLAIR = 7;
	
	/** The Constant codenameDONUT. */
	public static final int CODENAMEDONUT = 4;
	
	/** The SUPPORT s_ ics. */
	public static boolean supportsICS = false;
    
    /** The SUPPORT s_ jellybean. */
    public static boolean supportsJELLYBEAN = false;
	
	/** The SUPPORT s_ honeycomb. */
	public static boolean supportsHONEYCOMB = false;
	
	/** The SUPPORT s_ gingerbread. */
	public static boolean supportsGINGERBREAD = false;
	
	/** The SUPPORT s_ froyo. */
	public static boolean supportsFROYO = false;
	
	/** The SUPPORT s_ eclair. */
	public static boolean supportsECLAIR = false;
	
	/** The SUPPORT s_ donut. */
	public static boolean supportsDONUT = false;
	
	
	/** The strict mode available. */
	private static boolean strictModeAvailable;
	
	/** The m app tag. */
	public static String mAPPTAG;
	
	/** The app instance. */
	private static AppHelper appINSTANCE = null;
	
	/** The Constant CORE_POOL_SIZE. */
    private static final int CORE_POOL_SIZE = 5;
    
    /** The m executor service. */
    private ExecutorService mExecutorService;
    
    /** The Constant sThreadFactory. */
    private static final ThreadFactory STHREADFACTORY = new ThreadFactory() {
        private final AtomicInteger mCount = new AtomicInteger(1);

        public Thread newThread(Runnable r) {
            return new Thread(r, "Application thread #" + mCount.getAndIncrement());
        }
    };
    
    /** The m low memory listeners. */
    private ArrayList<WeakReference<OnLowMemoryListener>> mLowMemoryListeners;
    
    /** The context objects. */
    private HashMap<String, WeakReference<Context>> contextOBJECTS = new HashMap<String, WeakReference<Context>>();
	
	
	//we set apiSupport flags to be used 
	static {
		
		 androidAPILEVEL = android.os.Build.VERSION.SDK_INT;
		 /**
		  * we can just use the result of a comparing two ints which will also be a 
		  * boolean result to set the supports vars, that way we get rid of unnecessary if statements
		  * and thus gives us a performance tweak.  You see this as one of many examples of
		  * using java knowledge to get a performance boost in mobile java and its not the only one.
		  */
		 supportsICS = androidAPILEVEL >= CODENAMEICS;
	        supportsHONEYCOMB = androidAPILEVEL >= CODENAMEHONEYCOMB;
	        supportsGINGERBREAD = androidAPILEVEL >= CODENAMEGINGERBREAD;
	        supportsFROYO = androidAPILEVEL >= CODENAMEFROYO;
	        supportsECLAIR = androidAPILEVEL >= CODENAMEECLAIR;
	        supportsDONUT = androidAPILEVEL >= CODENAMEDONUT;
	        supportsJELLYBEAN = androidAPILEVEL >= CODENAMEJELLYBEAN;
	}
	 //We need as public static as we will override in each application
    //class as:
   // String mainCacheDirName_AppCache = AppHelper.setMainCacheDirName_AppCache("ourDirName");
   //at the beginning of your app class
      /** The main cache dir name_ app cache. */
 	public static String mainCACHEDIRNAMEAPPCACHE = ".ActiveWallpaper_appCache";
      
      /**
       * Sets the main cache dir name_ app cache.
       *
       * @param dirName the main cache dir name_ app cache
       */
      public static void setMAINCACHEDIRNAMEAPPCACHE(String dirName) {
      	AppHelper.mainCACHEDIRNAMEAPPCACHE = dirName;
      }
      
      // --- object cache ---
      /** The object cache. */
      private ObjectCache objectCache = null;
      
      /** The INITIA l_ capacit y_object cache. */
      public static int initialcapacityObjectCache = 25;
      
      /** The DEFAUL t_ poo l_ siz e_object cache. */
      public static int defaultpoolsizeObjectCache = 3;
      
      /** The DEFAUL t_ tt l_ minute s_object cache. */
      public static int DEFAULT_TTL_MINUTES_objectCache = 1 * 60;  //mins
      
      /**
       * Sets the initia l_ capacit y_object cache.
       *
       * @param mInt the INITIA l_ capacit y_object cache
       */
      public static void setINITIAL_CAPACITY_objectCache(int mInt) {
   	   initialcapacityObjectCache = mInt;
      }
      
      /**
       * Sets the defaul t_ poo l_ siz e_object cache.
       *
       * @param mInt the DEFAUL t_ poo l_ siz e_object cache
       */
      public static void setDEFAULT_POOL_SIZE_objectCache(int mInt) {
   	   defaultpoolsizeObjectCache = mInt;
      }
      
      /**
       * Sets the defaul t_ tt l_ minute s_object cache.
       *
       * @param mInt the DEFAUL t_ tt l_ minute s_object cache
       */
      public static void setDEFAULT_TTL_MINUTES_objectCache(int mInt) {
   	   DEFAULT_TTL_MINUTES_objectCache = mInt;
      }
      
      /**
       * Gets the object cache.
       *
       * @return the object cache
       */
      public ObjectCache getObjectCache() {
  		
  		if (objectCache == null) {
  			AppLogger.d(mAPPTAG, "objectCache: init NEW");
  			objectCache = new ObjectCache(initialcapacityObjectCache, DEFAULT_TTL_MINUTES_objectCache, defaultpoolsizeObjectCache);
  			objectCache.setCacheDirName(mainCACHEDIRNAMEAPPCACHE); // main cache dir name
  			objectCache.enableDiskCache(this, ObjectCache.DISK_CACHE_SDCARD);
              
              if (!GenericStore.isCustomKeyExist(GenericStore.TYPE_MEMDISKCACHE, GenericStore.KEY_OBJECTCACHEFUPATH, this)) {
  	        	GenericStore.setCustomData(
  	        			GenericStore.TYPE_SHAREDPREF
  	        			, GenericStore.KEY_OBJECTCACHEFUPATH 
  	        			, objectCache.getDiskCacheDirectory()
  	        			, this);
              }
  		}
  		
  		return (objectCache);
  	}
      
      // --- image cache ---
      /** The image cache. */
      private ImageCache imageCache = null;
      
      /** The INITIA l_ capacit y_ image cache. */
      public static int INITIAL_CAPACITY_ImageCache = 25;
      
      /** The DEFAUL t_ poo l_ siz e_ image cache. */
      public static int DEFAULT_POOL_SIZE_ImageCache = 3;
      
      /** The DEFAUL t_ tt l_ minute s_ image cache. */
      public static int DEFAULT_TTL_MINUTES_ImageCache = 1 * 60; // mins
      
      /**
       * Sets the initia l_ capacit y_ image cache.
       *
       * @param mInt the INITIA l_ capacit y_ image cache
       */
      public static void setINITIAL_CAPACITY_ImageCache(int mInt) {
   	   INITIAL_CAPACITY_ImageCache = mInt;
      }
      
      /**
       * Sets the defaul t_ poo l_ siz e_ image cache.
       *
       * @param mInt the DEFAUL t_ poo l_ siz e_ image cache
       */
      public static void setDEFAULT_POOL_SIZE_ImageCache(int mInt) {
   	   DEFAULT_POOL_SIZE_ImageCache = mInt;
      }
      
      /**
       * Sets the defaul t_ tt l_ minute s_ image cache.
       *
       * @param mInt the DEFAUL t_ tt l_ minute s_ image cache
       */
      public static void setDEFAULT_TTL_MINUTES_ImageCache(int mInt) {
   	   DEFAULT_TTL_MINUTES_ImageCache = mInt;
      }
      
      /**
       * Gets the image cache.
       *
       * @return the image cache
       */
      public ImageCache getImageCache() {
  		
  		if (imageCache == null) {
  			AppLogger.d(mAPPTAG, "imageCache: init NEW");
  			imageCache = new ImageCache(INITIAL_CAPACITY_ImageCache, DEFAULT_TTL_MINUTES_ImageCache, DEFAULT_POOL_SIZE_ImageCache);
  			imageCache.setCacheDirName(mainCACHEDIRNAMEAPPCACHE); // main cache dir name
  			imageCache.enableDiskCache(this, ObjectCache.DISK_CACHE_SDCARD);
  			
  			if (!GenericStore.isCustomKeyExist(GenericStore.TYPE_MEMDISKCACHE, GenericStore.KEY_IMAGECACHEFUPATH, this)) {
  	        	GenericStore.setCustomData(
  	        			GenericStore.TYPE_SHAREDPREF
  	        			, GenericStore.KEY_IMAGECACHEFUPATH 
  	        			, imageCache.getDiskCacheDirectory()
  	        			, this);
  			}
  		}
  		
  		return (imageCache);
  	}
      
      /**
       * Clear image cache.
       */
      public void clearImageCache() {
  		int itemCount = imageCache.removeAllObjects();
  		AppLogger.d(mAPPTAG, "cleared " 
  				+ itemCount 
  				+ " items from ImageCache"
  				+ "|" + imageCache.getDiskCacheDirectory()
  				);
  	}
  	
	  /**
	   * Clear object cache.
	   */
	  public void clearObjectCache() {
  		int itemCount = objectCache.removeAllObjects();
  		AppLogger.d(mAPPTAG, "cleared " 
  				+ itemCount
  				+ " items from ObjectCache"
  				+ "|" + objectCache.getDiskCacheDirectory()
  				);
  	}
  	
	  /**
	   * Clear all cache.
	   */
	  public void clearAllCache() {
  		
  		clearObjectCache();
  		clearImageCache();
  	}
	

	/**
	 * The Constructor.
	 */
	public AppHelper() {
		try {
            StrictModeWrapper.checkAvailable();
            strictModeAvailable = true;
        } catch (Throwable throwable) {
            strictModeAvailable = false;
        }
	 mLowMemoryListeners = new ArrayList<WeakReference<OnLowMemoryListener>>();
	 
	}
	
/**
 * The Interface OnLowMemoryListener.
 */
 interface OnLowMemoryListener {
        
        /**
         * Callback to be invoked when the system needs memory.
         */
        void onLowMemoryReceived();
    }
    
    /**
     * Register on low memory listener.
     *
     * @param listener the listener
     */
    public void registerOnLowMemoryListener(OnLowMemoryListener listener) {
        if (listener != null) {
            mLowMemoryListeners.add(new WeakReference<OnLowMemoryListener>(listener));
        }
    }
    
    /**
     * Unregister on low memory listener.
     *
     * @param listener the listener
     */
    public void unregisterOnLowMemoryListener(OnLowMemoryListener listener) {
        if (listener != null) {
            int i = 0;
            while (i < mLowMemoryListeners.size()) {
                final OnLowMemoryListener l = mLowMemoryListeners.get(i).get();
                if (l == null || l == listener) {
                    mLowMemoryListeners.remove(i);
                } else {
                    i++;
                }
            }
        }
    }
    
    /**
     * On low memory.
     *
     * @see android.app.Application#onLowMemory()
     */
    @Override
    public void onLowMemory() {
        super.onLowMemory();
        int i = 0;
        while (i < mLowMemoryListeners.size()) {
            final OnLowMemoryListener listener = mLowMemoryListeners.get(i).get();
            if (listener == null) {
                mLowMemoryListeners.remove(i);
            } else {
                listener.onLowMemoryReceived();
                i++;
            }
        }
        
        AppLogger.w(mAPPTAG, "onLowMemory() -> clearImageCacheFu!!!");
		// clearing image cache should be enough, feel free to also clear object cache
		clearImageCache();
    }
    
    /**
     * On trim memory.
     *
     * @param level the level
     * @see  android.app.Application#onTrimMemory(int)
     */
    @Override
   public  void onTrimMemory(int level) {
   	
   	super.onTrimMemory(level);
   }
	
	
    /**
     * Gets the application context.
     *
     * @return the application context
     * @see  android.content.ContextWrapper#getApplicationContext()
     */
	@Override
	public Context getApplicationContext() {
		return appINSTANCE.getApplicationContext();
	}
	
	/**
     * Gets the executor.
     *
     * @return the executor
     */
    public ExecutorService getExecutor() {
        if (mExecutorService == null) {
            mExecutorService = Executors.newFixedThreadPool(CORE_POOL_SIZE, STHREADFACTORY);
        }
        return mExecutorService;
    }
	
	/**
	 * Sets the m app tag.
	 *
	 * @param ourAppTag the our app tag
	 * @return the string
	 */
	public static String setMAppTag(String ourAppTag) {
		return mAPPTAG = ourAppTag;
	}
	
	/**
	 * Gets the m app tag.
	 *
	 * @return the m app tag
	 */
	public static String getMAppTag() {
		return mAPPTAG;
	}
	
	/**
	 * Check instance.
	 */
	private static void checkInstance() {
        if (appINSTANCE == null) {
            throw new IllegalStateException("Application not created yet!");
        }
    }
	
	/**
	 * Gets the app instance.
	 *
	 * @return the app instance
	 */
	public static AppHelper getAppInstance() {
		checkInstance();
        return appINSTANCE;
	}

	
	
	/**
	 * Gets the application info.
	 *
	 * @return the application info
	 * @see  android.content.ContextWrapper#getApplicationInfo()
	 */
	@Override
	public ApplicationInfo getApplicationInfo() {
		// TODO Auto-generated method stub
		return super.getApplicationInfo();
	}
	
	
	/**
	 * Gets the wallpaper desired minimum height.
	 *
	 * @return the wallpaper desired minimum height
	 * @see android.content.ContextWrapper#getWallpaperDesiredMinimumHeight()
	 */
	@Override
	public int getWallpaperDesiredMinimumHeight() {
		
		return super.getWallpaperDesiredMinimumHeight();
	}
	
	
	/**
	 * Gets the wallpaper desired minimum width.
	 *
	 * @return the wallpaper desired minimum width
	 * @see  android.content.ContextWrapper#getWallpaperDesiredMinimumWidth()
	 */
	@Override
	public int getWallpaperDesiredMinimumWidth() {
		
		return super.getWallpaperDesiredMinimumWidth();
	}
	
	 /**
     * Gets the active context.
     *
     * @param className the class name
     * @return the active context
     */
    public synchronized Context getActiveContext(String className) {
        WeakReference<Context> ref = contextOBJECTS.get(className);
        if (ref == null) {
            return null;
        }

        final Context c = ref.get();
        if (c == null) { // If the WeakReference is no longer valid, ensure it is removed.
            contextOBJECTS.remove(className);
        }
        return c;
        
    }

    /**
     * Sets the active context.
     *
     * @param className the class name
     * @param context the context
     */
    public synchronized void setActiveContext(String className, Context context) {
        WeakReference<Context> ref = new WeakReference<Context>(context);
        this.contextOBJECTS.put(className, ref);
    }

    /**
     * Reset active context.
     *
     * @param className the class name
     */
    public synchronized void resetActiveContext(String className) {
        contextOBJECTS.remove(className);
    }
    
    /**
     * Helps us retrieve and AppContext and refer to it a non static
     * way.
     *
     * @return the app context
     */
	 public static Context getAppContext() {
		
		return appINSTANCE.getApplicationContext();
	}
	 
	
	 /**
 	 * On create.
 	 *
 	 * @see  android.app.Application#onCreate()
 	 */
 	@Override
		public void onCreate() {
			//API 9 or greater allows strict mode to
	     	// help in debugging
	     	if (strictModeAvailable) {
	             // check if android:debuggable is set to true
	             int applicationFlags = getApplicationInfo().flags;
	             if ((applicationFlags & ApplicationInfo.FLAG_DEBUGGABLE) != 0) {
	                 StrictModeWrapper.enableDefaults();
	             }
	         }
	     	super.onCreate();
	     	//provide an instance for our static accessors
	         appINSTANCE = this;
	         
	         //rest of our setup methods  goes here
	         setUpCache();
	         setUpSensors();
		}
		
		/**
		 * Override this method to customize whether you want just an imageCache
		 * initialized or both an imageCache and objectCache initialized.
		 */
		public void setUpCache() {
			// initialize CACHE once App is created, this does init/create only ONCE!
					objectCache = getObjectCache();
					imageCache = getImageCache();
		}
		
		/**
		 * Sets the up sensors.
		 */
		public void setUpSensors() {
			
		}
		
		/**
	     * <p>
	     * Invoked if the application is about to close. Application close is being defined as the
	     * transition of the last running Activity of the current application to the Android home screen
	     * using the BACK button. You can leverage this method to perform cleanup logic such as freeing
	     * resources whenever your user "exits" your app using the back button.
	     * </p>
	     * <p>
	     * Note that you must not rely on this callback as a general purpose "exit" handler, since
	     * Android does not give any guarantees as to when exactly the process hosting an application is
	     * being terminated. In other words, your application can be terminated at any point in time, in
	     * which case this method will NOT be invoked.
	     * </p>
	     */
	    public void onClose() {
	        // NO-OP by default
	    }
	    
	    /**
	     * Obviously, if you have a set of resources to unregister(ie listeners) you
	     * should probably override this and define which listeners to 
	     * unregister.
	     */
	    @Override
	    public void onTerminate() {
	    	
	    	super.onTerminate();
	    	AppLogger.w(mAPPTAG, "onTerminate() -> Clearing all cache!!!");
			clearAllCache();
	    }
	 	
	    
	    
		
    
}
