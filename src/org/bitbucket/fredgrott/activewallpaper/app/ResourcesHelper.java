/*******************************************************************************
 * Copyright 2012 fredgrott
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package org.bitbucket.fredgrott.activewallpaper.app;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import org.bitbucket.fredgrott.activewallpaper.log.LogUtility;




/**
 * The Class ResourcesHelper.
 */
public class ResourcesHelper {
	
	/** The TAG. */
	@SuppressWarnings("unused")
	private static String Tag = LogUtility.setLOGTAG("ResourcesHelper");
	
	/**
	 * Gets the res id.
	 *
	 * @param ourResName the our res name
	 * @return the res id
	 */
	public static int getResID(String ourResName) {
		return  AppHelper.getAppContext().getResources().getIdentifier(ourResName, "id", AppHelper.getAppContext().getPackageName());
	}
	
	/**
	 * Gets the res raw id.
	 *
	 * @param ourResName the our res name
	 * @return the res raw id
	 */
	public static int getResRawID(String ourResName) {
		return AppHelper.getAppContext().getResources().getIdentifier(ourResName,  "raw", AppHelper.getAppContext().getPackageName());
	}
	
	/**
	 * Load raw resource.
	 *
	 * @param resourceID the resource id
	 * @return the string
	 * @throws Exception the exception
	 */
	public static String loadRawResource(int resourceID) throws Exception  {
		InputStream is = AppHelper.getAppContext().getResources().openRawResource(resourceID);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] buf = new byte[1024];
		int len;
		while ((len = is.read(buf)) != -1) {
			baos.write(buf, 0, len);
		}
		return baos.toString();
		
	}

}
