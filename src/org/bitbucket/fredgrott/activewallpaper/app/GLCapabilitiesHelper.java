/*******************************************************************************
 * Copyright 2012 fredgrott
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package org.bitbucket.fredgrott.activewallpaper.app;

import javax.microedition.khronos.opengles.GL10;

import org.bitbucket.fredgrott.activewallpaper.log.AppLogger;




/**
 * GLCapabilitiesHelper class designed for extension.
 * Can be used as is or if you need more extend it and override
 * methods and add your own.
 * 
 * To extend and override, override the checkGLEntensions method
 * and add stuff to it such as:
 * <code>
 * public static void checkGLExtensions(GL10 gl){
 *        String extensions = gl.glGetString(GL10.GL_EXTENSIONS);
 *		
 *		AppLogger.d("GLCapabilities", extensions);
 *		
 *		nonPowerOfTwoTextures = extensions.contains("ARB_texture_non_power_of_two");
 *
 *     //your stuff to add goes here and than add the extra vars above this method
 *	}
 * </code>
 * 
 * @author fredgrott
 *
 */
public class GLCapabilitiesHelper {
	
	/** The non power of two textures. */
	private static boolean nonPowerOfTwoTextures;

	/**
	 * The Constructor.
	 */
	public GLCapabilitiesHelper() { }
	
	/**
	 * The Constructor.
	 *
	 * @param gl the gl
	 */
	public GLCapabilitiesHelper(GL10 gl) {
		checkGLExtensions(gl);
	}
	
	
	/**
	 * Supports non power of two textures.
	 *
	 * @return true, if supports non power of two textures
	 */
	public boolean supportsNonPowerOfTwoTextures() {
		return nonPowerOfTwoTextures;
	}
	
	/**
	 * Check gl extensions.
	 *
	 * @param gl the gl
	 */
	public static void checkGLExtensions(GL10 gl) {
         String extensions = gl.glGetString(GL10.GL_EXTENSIONS);
		
		AppLogger.d("GLCapabilities", extensions);
		
		nonPowerOfTwoTextures = extensions.contains("ARB_texture_non_power_of_two");
	}

}
