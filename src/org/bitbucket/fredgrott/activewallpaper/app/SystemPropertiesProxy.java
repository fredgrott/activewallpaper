/*******************************************************************************
 * Copyright 2012 fredgrott
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package org.bitbucket.fredgrott.activewallpaper.app;

import java.io.File;
import java.lang.reflect.Method;

import org.bitbucket.fredgrott.activewallpaper.log.LogUtility;



import android.content.Context;
import dalvik.system.DexFile;


/**
 * Originally suggested in SO answer:
 * 
 * http://stackoverflow.com/questions/2641111/where-is-android-os-systemproperties
 * 
 * Reflection was used because Runtime.getRuntime().exec("getprop " + key) some times causes lock.
 * 
 * 
 * @author fredgrott
 *
 */
public final class SystemPropertiesProxy {
	
	/** The TAG. */
	@SuppressWarnings("unused")
	private static String mTag = LogUtility.setLOGTAG("SystemPropertiesProxy");

/**
 * This class cannot be instantiated.
 */
private SystemPropertiesProxy() {

}

    /**
     * Get the value for the given key.
     *
     * @param context the context
     * @param key the key
     * @return an empty string if the key isn't found
     * 
     */
    @SuppressWarnings("unchecked")
	public static String get(Context context, String key)  {

        String ret = "";

        try {

          ClassLoader cl = context.getClassLoader(); 
          @SuppressWarnings("rawtypes")
          Class SystemProperties = cl.loadClass("android.os.SystemProperties");

          //Parameters Types
          @SuppressWarnings("rawtypes")
              Class[] paramTypes = new Class[1];
          paramTypes[0] = String.class;

          Method get = SystemProperties.getMethod("get", paramTypes);

          //Parameters
          Object[] params = new Object[1];
          params[0] = new String(key);

          ret = (String) get.invoke(SystemProperties, params);

        } catch (IllegalArgumentException iAE) {
            throw iAE;
        } catch (Exception e) {
            ret = "";
            //TODO
        }

        return ret;

    }

    /**
     * Get the value for the given key.
     *
     * @param context the context
     * @param key the key
     * @param def the def
     * @return if the key isn't found, return def if it isn't null, or an empty string otherwise
     * 
     */
    @SuppressWarnings("unchecked")
	public static String get(Context context, String key, String def) {

        String ret = def;

        try {

          ClassLoader cl = context.getClassLoader(); 
          @SuppressWarnings("rawtypes")
          Class SystemProperties = cl.loadClass("android.os.SystemProperties");

          //Parameters Types
          @SuppressWarnings("rawtypes")
              Class[] paramTypes = new Class[2];
          paramTypes[0] = String.class;
          paramTypes[1] = String.class;          

          Method get = SystemProperties.getMethod("get", paramTypes);

          //Parameters
          Object[] params = new Object[2];
          params[0] = new String(key);
          params[1] = new String(def);

          ret= (String) get.invoke(SystemProperties, params);

        } catch (IllegalArgumentException iAE) {
            throw iAE;
        } catch (Exception e)  {
            ret = def;
            //TODO
        }

        return ret;

    }

    /**
     * Get the value for the given key, and return as an integer.
     *
     * @param context the context
     * @param key the key to lookup
     * @param def a default value to return
     * @return the key parsed as an integer, or def if the key isn't found or
     * cannot be parsed
     * 
     */
    @SuppressWarnings("unchecked")
	public static Integer getInt(Context context, String key, int def)  {

        Integer ret = def;

        try {

          ClassLoader cl = context.getClassLoader(); 
          @SuppressWarnings("rawtypes")
          Class SystemProperties = cl.loadClass("android.os.SystemProperties");

          //Parameters Types
          @SuppressWarnings("rawtypes")
              Class[] paramTypes = new Class[2];
          paramTypes[0] = String.class;
          paramTypes[1] = int.class;  

          Method getInt = SystemProperties.getMethod("getInt", paramTypes);

          //Parameters
          Object[] params = new Object[2];
          params[0] = new String(key);
          //changed per lint, performance
          params[1] =  Integer.valueOf(def);
          

          ret = (Integer) getInt.invoke(SystemProperties, params);

        } catch (IllegalArgumentException iAE) {
            throw iAE;
        } catch (Exception e) {
            ret = def;
            //TODO
        }

        return ret;

    }

    /**
     * Get the value for the given key, and return as a long.
     *
     * @param context the context
     * @param key the key to lookup
     * @param def a default value to return
     * @return the key parsed as a long, or def if the key isn't found or
     * cannot be parsed
     * 
     */
    @SuppressWarnings("unchecked")
	public static Long getLong(Context context, String key, long def)  {

        Long ret = def;

        try {

          ClassLoader cl = context.getClassLoader();
          @SuppressWarnings("rawtypes")
              Class SystemProperties= cl.loadClass("android.os.SystemProperties");

          //Parameters Types
          @SuppressWarnings("rawtypes")
              Class[] paramTypes = new Class[2];
          paramTypes[0] = String.class;
          paramTypes[1] = long.class;  

          Method getLong = SystemProperties.getMethod("getLong", paramTypes);

          //Parameters
          Object[] params = new Object[2];
          params[0] = new String(key);
          //changed per lint, performance
          params[1] =  Long.valueOf(def);

          ret = (Long) getLong.invoke(SystemProperties, params);

        } catch (IllegalArgumentException iAE) {
            throw iAE;
        } catch (Exception e) {
            ret = def;
            //TODO
        }

        return ret;

    }

    /**
     * Get the value for the given key, returned as a boolean.
     * Values 'n', 'no', '0', 'false' or 'off' are considered false.
     * Values 'y', 'yes', '1', 'true' or 'on' are considered true.
     * (case insensitive).
     * If the key does not exist, or has any other value, then the default
     * result is returned.
     *
     * @param context the context
     * @param key the key to lookup
     * @param def a default value to return
     * @return the key parsed as a boolean, or def if the key isn't found or is
     * not able to be parsed as a boolean.
     * 
     */
    @SuppressWarnings("unchecked")
	public static Boolean getBoolean(Context context, String key, boolean def)  {

        Boolean ret = def;

        try {

          ClassLoader cl = context.getClassLoader(); 
          @SuppressWarnings("rawtypes")
          Class SystemProperties = cl.loadClass("android.os.SystemProperties");

          //Parameters Types
          @SuppressWarnings("rawtypes")
              Class[] paramTypes = new Class[2];
          paramTypes[0] = String.class;
          paramTypes[1] = boolean.class;  

          Method getBoolean = SystemProperties.getMethod("getBoolean", paramTypes);

          //Parameters         
          Object[] params = new Object[2];
          params[0] = new String(key);
          //changed per lint, performance
          params[1] =  Boolean.valueOf(def);

          ret= (Boolean) getBoolean.invoke(SystemProperties, params);

        } catch (IllegalArgumentException iAE) {
            throw iAE;
        } catch (Exception e) {
            ret= def;
            //TODO
        }

        return ret;

    }

    /**
     * Set the value for the given key.
     *
     * @param context the context
     * @param key the key
     * @param val the val
     * 
     */
    @SuppressWarnings("unchecked")
	public static void set(Context context, String key, String val)  {

        try {

          @SuppressWarnings("unused")
          DexFile df = new DexFile(new File("/system/app/Settings.apk"));
          @SuppressWarnings("unused")
          ClassLoader cl = context.getClassLoader(); 
          @SuppressWarnings("rawtypes")
          Class SystemProperties = Class.forName("android.os.SystemProperties");

          //Parameters Types
          @SuppressWarnings("rawtypes")
              Class[] paramTypes = new Class[2];
          paramTypes[0] = String.class;
          paramTypes[1] = String.class;  

          Method set = SystemProperties.getMethod("set", paramTypes);

          //Parameters         
          Object[] params = new Object[2];
          params[0] = new String(key);
          params[1] = new String(val);

          set.invoke(SystemProperties, params);

        } catch (IllegalArgumentException iAE) {
            throw iAE;
        } catch (Exception e) {
            //TODO
        }

    }
}
