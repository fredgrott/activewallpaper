/*
 * Copyright (C) 2007 Google Inc. Licensed under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0 Unless required by applicable law
 * or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 */

package org.bitbucket.fredgrott.activewallpaper.collections;

import java.util.Map.Entry;

import org.bitbucket.fredgrott.activewallpaper.log.LogUtility;



/**
 * Implementation of the {@code equals}, {@code hashCode}, and {@code toString}
 * methods of {@code Entry}.
 *
 * @param <K> the key type
 * @param <V> the value type
 * @author Jared Levy
 */
abstract class AbstractMapEntry<K, V> implements Entry<K, V> {
	
	/** The TAG. */
	public static String TAG = LogUtility.setLOGTAG("AbstractMapEntry");

   
	/**
	 * @see java.util.Map.Entry#getKey()
	 */
    public abstract K getKey();

   
    /**
     * @see java.util.Map.Entry#getValue()
     */
    public abstract V getValue();

   
    /**
     * @see java.util.Map.Entry#setValue(Object)
     */
    public V setValue(V value) {
        throw new UnsupportedOperationException();
    }

    
    /**
     * @see java.lang.Object#equals(Object)
     */
    @Override
    public boolean equals(Object object) {
        if (object instanceof Entry) {
            Entry<?, ?> that = (Entry<?, ?>) object;
            return Objects.equal(this.getKey(), that.getKey())
                    && Objects.equal(this.getValue(), that.getValue());
        }
        return false;
    }

    
    /**
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        K k = getKey();
        V v = getValue();
        return ((k == null) ? 0 : k.hashCode()) ^ ((v == null) ? 0 : v.hashCode());
    }

    /**
     * Returns a string representation of the form <code>{key}={value}</code>.
     *
     * @return the string
     */
    @Override
    public String toString() {
        return getKey() + "=" + getValue();
    }
}