package org.bitbucket.fredgrott.activewallpaper.svg;

import org.bitbucket.fredgrott.activewallpaper.log.LogUtility;


/**
 * Runtime exception thrown when there is a problem parsing an SVG.
 *
 * @author Larva Labs, LLC
 */
@SuppressWarnings("serial")
public class SVGParseException extends RuntimeException {
	
	/** The TAG. */
	public static String TAG = LogUtility.setLOGTAG("SVGParseException");

    /**
     * The Constructor.
     *
     * @param s the s
     */
    public SVGParseException(String s) {
        super(s);
    }

    /**
     * The Constructor.
     *
     * @param s the s
     * @param throwable the throwable
     */
    public SVGParseException(String s, Throwable throwable) {
        super(s, throwable);
    }

    /**
     * The Constructor.
     *
     * @param throwable the throwable
     */
    public SVGParseException(Throwable throwable) {
        super(throwable);
    }
}
