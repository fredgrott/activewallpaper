/*******************************************************************************
 * Copyright 2012 fredgrott
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package org.bitbucket.fredgrott.activewallpaper.log;



import android.opengl.GLSurfaceView;
import android.opengl.GLSurfaceView.GLWrapper;
import android.util.Log;

/**
 * The Class GLLogger.
 *
 * @author fredgrott
 */
public class GLLogger {
    /**
     * Gl debug.
     *
     * @param gl
     *            the gl
     * @param glWrapper
     *            the gl wrapper
     * @param debugFlags
     *            the debug flags
     * @param logTag
     *            the lO g_ tag
     */
    public final void glDebug(GLSurfaceView gl,
            GLWrapper glWrapper, int debugFlags, String logTag) {
        if (Log.isLoggable(logTag, Log.DEBUG)) {
            gl.setGLWrapper(glWrapper);
            gl.setDebugFlags(debugFlags);

        } else {
            AppLogger.i(logTag, "Log.d not enabled");
        }
    }
}

