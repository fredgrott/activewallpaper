/*******************************************************************************
 * Copyright 2012 fredgrott
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package org.bitbucket.fredgrott.activewallpaper.log;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * We have two  or more use cases:
 * 
 * USE CASE 1: Log on emulator running as debug either through the build script or
 *             IDE plugin debug-as
 * USE CASE 2: Log on device connected via USB cable
 * 
 * USE CASE 3: Log on production device, specific log statement kept in by 
 *             calling LotUtils.setClassName("Class you know does not get obfuscated") 
 *             before calling/defining the log statement.
 *             
 * Hence we cannot use the simplistic check the Build flags for debuggable and thus this 
 * non obvious solution.
 * 
 * @author fredgrott
 *
 */
public class LogUtility {
	
	/** The DEFAUL t_ tag. */
	static String DEFAULT_TAG = null;
	
	/** The depth. */
	static int depth = 4;
	
	/** The class for name default. */
	static String classForNameDefault = "org.bitbucket.fredgrott.activewallpaper.log.AppLogWTFObfuscate";
	// this class gets obfuscated so its our trigger not log in production
	/** The class for name. */
	static String classForName = classForNameDefault;
     
	
	/**
	 * Sets the lOGTAG.
	 *
	 * @param ourClass the new lOGTAG
	 */
	@SuppressWarnings("rawtypes")
	public static String setLOGTAG(Class ourClass){
		return DEFAULT_TAG = ourClass.getSimpleName();
	}
	
	public static String setLOGTAG(String ourString) {
		return DEFAULT_TAG = ourString;
	}
	
	/**
	 * Gets the dEFAUL t_ tag.
	 *
	 * @return the dEFAUL t_ tag
	 */
	public static String getDEFAULTTAG() {
		return DEFAULT_TAG;
	}
	
	/**
	 * Sets the log depth.
	 *
	 * @param logdepth the new log depth
	 */
	public static void setLogDepth(int logdepth) {
		depth = logdepth;
	}
	
	/**
	 * If you need to keep a log statement in production code
	 * set this to a class that you have set Proguard not to strip out,
	 * an example might be android.util.Log
	 * 
	 * Because you  have used this method to set it right before the log
	 * call you obviously need to set it back.
	 *
	 * @param ourClass the new class for name
	 */
	public static void setClassForName(String ourClass){
		classForName = ourClass;
	}
	
	
	/**
	 * We have several ways to strip out Logging for production applications.
	 * And this is a common error that developer will forget to use one of those
	 * methods to strip out the logging in production code.
	 * 
	 * Thus, we set the var we use in Class.forName to a class we know most
	 * times will be obfuscated. We than define a setter to set that var, this way
	 * the developer can configure whether or not he wants some log of a class left
	 * behind in a production application on an individual log statement basis.
	 *
	 * @param methodName the method name
	 * @param tag the tag
	 * @param message the message
	 */
	@SuppressWarnings("rawtypes")
    public static void callLogger(String methodName, String tag, String message) {
           final StackTraceElement[] ste = Thread.currentThread().getStackTrace();
           try {
                  Class cls = Class.forName(classForName);
                  @SuppressWarnings("unchecked")
				Method method = cls.getMethod(methodName, String.class,       String.class);
                  method.invoke(null, tag, getTrace(ste) + message);
           } catch (ClassNotFoundException e) {
                  //production
           } catch (IllegalArgumentException e) {
                  //production
           } catch (SecurityException e) {
                  //production
           } catch (IllegalAccessException e) {
                  //production
           } catch (InvocationTargetException e) {
                  //production
           } catch (NoSuchMethodException e) {
                  //production
           }
    }

    /**
     * Gets the trace.
     *
     * @param ste the ste
     * @return the trace
     */
    public static String getTrace(StackTraceElement[] ste) {
           return"[" + getClassName(ste) + "][" + getMethodName(ste) + "][" + getLineNumber(ste) + "] ";
    }

    /**
     * Gets the class package.
     *
     * @param ste the ste
     * @return the class package
     */
    public static String getClassPackage(StackTraceElement[] ste) {
           return ste[depth].getClassName();
    }

    /**
     * Gets the class name.
     *
     * @param ste the ste
     * @return the class name
     */
    public static String getClassName(StackTraceElement[] ste) {
           String[] temp = ste[depth].getClassName().split("\\.");
           return temp[temp.length - 1];
    }

    /**
     * Gets the method name.
     *
     * @param ste the ste
     * @return the method name
     */
    public static String getMethodName(StackTraceElement[] ste) {
           return ste[depth].getMethodName();
    }

    /**
     * Gets the line number.
     *
     * @param ste the ste
     * @return the line number
     */
    public static int getLineNumber(StackTraceElement[] ste) {
           return ste[depth].getLineNumber();
    }
	
	
}
