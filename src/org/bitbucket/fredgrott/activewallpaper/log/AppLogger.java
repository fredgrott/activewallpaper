/*******************************************************************************
 * Copyright 2012 fredgrott
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package org.bitbucket.fredgrott.activewallpaper.log;

;

/**
 * We are not calling androoid.util.Log methods 
 * directly as we use a stack trace set of methods to get
 * the full details of our log stuff rather than just a single line of
 * fluff.
 * 
 * 
 * @author fredgrott
 *
 */
public class AppLogger {
	
	
	/**
	 * I.
	 *
	 * @param message the message
	 */
	public static void i(String message) {
        LogUtility.callLogger("i", LogUtility.getDEFAULTTAG(), message);
 }

 /**
  * I.
  *
  * @param message the message
  * @param tag the tag
  */
 public static void i(String message, String tag) {
        LogUtility.callLogger("i", tag, message);
 }

 /**
  * D.
  *
  * @param message the message
  */
 public static void d(String message) {
        LogUtility.callLogger("d", LogUtility.getDEFAULTTAG(), message);
 }

 /**
  * D.
  *
  * @param message the message
  * @param tag the tag
  */
 public static void d(String message, String tag) {
        LogUtility.callLogger("d", tag, message);
 }

 /**
  * E.
  *
  * @param message the message
  */
 public static void e(String message) {
        LogUtility.callLogger("e", LogUtility.getDEFAULTTAG(), message);
 }
 
 

 /**
  * E.
  *
  * @param message the message
  * @param tag the tag
  */
 public static void e(String message, String tag) {
        LogUtility.callLogger("e", tag, message);
 }

 /**
  * W.
  *
  * @param message the message
  */
 public static void w(String message) {
        LogUtility.callLogger("w", LogUtility.getDEFAULTTAG(), message);
 }

 /**
  * W.
  *
  * @param message the message
  * @param tag the tag
  */
 public static void w(String message, String tag) {
        LogUtility.callLogger("w", tag, message);
 }

 /**
  * V.
  *
  * @param message the message
  */
 public static void v(String message) {
        LogUtility.callLogger("v", LogUtility.getDEFAULTTAG(), message);
 }

 /**
  * V.
  *
  * @param message the message
  * @param tag the tag
  */
 public static void v(String message, String tag) {
        LogUtility.callLogger("v", tag, message);
 }


}
