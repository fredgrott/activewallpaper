/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.bitbucket.fredgrott.activewallpaper.log;

import java.io.IOException;
import java.io.Writer;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.CharBuffer;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.nio.ShortBuffer;
import java.util.Arrays;

import javax.microedition.khronos.opengles.GL;



/**
 * A wrapper that logs all GL calls (and results) in human-readable form.
 *
 */
class GLLogWrapper extends GLWrapperBase {
    

    /** The Constant FORMAT_INT. */
    private static final int FORMAT_INT = 0;
    
    /** The Constant FORMAT_FLOAT. */
    private static final int FORMAT_FLOAT = 1;
    
    /** The Constant FORMAT_FIXED. */
    private static final int FORMAT_FIXED = 2;

    /**
     * Instantiates a new gL log wrapper.
     *
     * @param gl the gl
     * @param log the log
     * @param logArgumentNames the log argument names
     */
    public GLLogWrapper(GL gl, Writer log, boolean logArgumentNames) {
        super(gl);
        mLog = log;
        mLogArgumentNames = logArgumentNames;
    }

    /**
     * Check error.
     */
    private void checkError() {
        int glError;
        if ((glError = mgl.glGetError()) != 0) {
            String errorMessage = "glError: " + Integer.toString(glError);
            logLine(errorMessage);
        }
    }

    /**
     * Log line.
     *
     * @param message the message
     */
    private void logLine(String message) {
        log(message + '\n');
    }

    /**
     * Log.
     *
     * @param message the message
     */
    private void log(String message) {
        try {
            mLog.write(message);
        } catch (IOException e) {
            // Ignore exception, keep on trying
        }
    }

    /**
     * Begin.
     *
     * @param name the name
     */
    private void begin(String name) {
        log(name + '(');
        mArgCount = 0;
    }

    /**
     * Arg.
     *
     * @param name the name
     * @param value the value
     */
    private void arg(String name, String value) {
        if (mArgCount++ > 0) {
            log(", ");
        }
        if (mLogArgumentNames) {
            log(name + "=");
        }
        log(value);
    }

    /**
     * End.
     */
    private void end() {
        log(");\n");
        flush();
    }

    /**
     * Flush.
     */
    private void flush() {
        try {
            mLog.flush();
        } catch (IOException e) {
            mLog = null;
        }
    }

    /**
     * Arg.
     *
     * @param name the name
     * @param value the value
     */
    private void arg(String name, boolean value) {
        arg(name, Boolean.toString(value));
    }

    /**
     * Arg.
     *
     * @param name the name
     * @param value the value
     */
    private void arg(String name, int value) {
        arg(name, Integer.toString(value));
    }

    /**
     * Arg.
     *
     * @param name the name
     * @param value the value
     */
    private void arg(String name, float value) {
        arg(name, Float.toString(value));
    }

    /**
     * Returns.
     *
     * @param result the result
     */
    private void returns(String result) {
        log(") returns " + result + ";\n");
        flush();
    }

    /**
     * Returns.
     *
     * @param result the result
     */
    private void returns(int result) {
        returns(Integer.toString(result));
    }

    /**
     * Arg.
     *
     * @param name the name
     * @param n the n
     * @param arr the arr
     * @param offset the offset
     */
    private void arg(String name, int n, int[] arr, int offset) {
        arg(name, toString(n, FORMAT_INT, arr, offset));
    }

    /**
     * Arg.
     *
     * @param name the name
     * @param n the n
     * @param arr the arr
     * @param offset the offset
     */
    private void arg(String name, int n, short[] arr, int offset) {
        arg(name, toString(n, arr, offset));
    }

    /**
     * Arg.
     *
     * @param name the name
     * @param n the n
     * @param arr the arr
     * @param offset the offset
     */
    private void arg(String name, int n, float[] arr, int offset) {
        arg(name, toString(n, arr, offset));
    }

    /**
     * Formatted append.
     *
     * @param buf the buf
     * @param value the value
     * @param format the format
     */
    private void formattedAppend(StringBuilder buf, int value, int format) {
        switch (format) {
        case FORMAT_INT:
            buf.append(value);
            break;
        case FORMAT_FLOAT:
            buf.append(Float.intBitsToFloat(value));
            break;
        case FORMAT_FIXED:
            buf.append(value / 65536.0f);
            break;
        }
    }

    /**
     * To string.
     *
     * @param n the n
     * @param format the format
     * @param arr the arr
     * @param offset the offset
     * @return the string
     */
    private String toString(int n, int format, int[] arr, int offset) {
        StringBuilder buf = new StringBuilder();
        buf.append("{\n");
        int arrLen = arr.length;
        for (int i = 0; i < n; i++) {
            int index = offset + i;
            buf.append(" [" + index + "] = ");
            if (index < 0 || index >= arrLen) {
                buf.append("out of bounds");
            } else {
                formattedAppend(buf, arr[index], format);
            }
            buf.append('\n');
        }
        buf.append("}");
        return buf.toString();
    }

    /**
     * To string.
     *
     * @param n the n
     * @param arr the arr
     * @param offset the offset
     * @return the string
     */
    private String toString(int n, short[] arr, int offset) {
        StringBuilder buf = new StringBuilder();
        buf.append("{\n");
        int arrLen = arr.length;
        for (int i = 0; i < n; i++) {
            int index = offset + i;
            buf.append(" [" + index + "] = ");
            if (index < 0 || index >= arrLen) {
                buf.append("out of bounds");
            } else {
                buf.append(arr[index]);
            }
            buf.append('\n');
        }
        buf.append("}");
        return buf.toString();
    }

    /**
     * To string.
     *
     * @param n the n
     * @param arr the arr
     * @param offset the offset
     * @return the string
     */
    private String toString(int n, float[] arr, int offset) {
        StringBuilder buf = new StringBuilder();
        buf.append("{\n");
        int arrLen = arr.length;
        for (int i = 0; i < n; i++) {
            int index = offset + i;
            buf.append("[" + index + "] = ");
            if (index < 0 || index >= arrLen) {
                buf.append("out of bounds");
            } else {
                buf.append(arr[index]);
            }
            buf.append('\n');
        }
        buf.append("}");
        return buf.toString();
    }

    /**
     * To string.
     *
     * @param n the n
     * @param buf the buf
     * @return the string
     */
    private String toString(int n, FloatBuffer buf) {
        StringBuilder builder = new StringBuilder();
        builder.append("{\n");
        for (int i = 0; i < n; i++) {
            builder.append(" [" + i + "] = " + buf.get(i) + '\n');
        }
        builder.append("}");
        return builder.toString();
    }

    /**
     * To string.
     *
     * @param n the n
     * @param format the format
     * @param buf the buf
     * @return the string
     */
    private String toString(int n, int format, IntBuffer buf) {
        StringBuilder builder = new StringBuilder();
        builder.append("{\n");
        for (int i = 0; i < n; i++) {
            builder.append(" [" + i + "] = ");
            formattedAppend(builder, buf.get(i), format);
            builder.append('\n');
        }
        builder.append("}");
        return builder.toString();
    }

    /**
     * To string.
     *
     * @param n the n
     * @param buf the buf
     * @return the string
     */
    private String toString(int n, ShortBuffer buf) {
        StringBuilder builder = new StringBuilder();
        builder.append("{\n");
        for (int i = 0; i < n; i++) {
            builder.append(" [" + i + "] = " + buf.get(i) + '\n');
        }
        builder.append("}");
        return builder.toString();
    }

    /**
     * Arg.
     *
     * @param name the name
     * @param n the n
     * @param buf the buf
     */
    private void arg(String name, int n, FloatBuffer buf) {
        arg(name, toString(n, buf));
    }

    /**
     * Arg.
     *
     * @param name the name
     * @param n the n
     * @param buf the buf
     */
    private void arg(String name, int n, IntBuffer buf) {
        arg(name, toString(n, FORMAT_INT, buf));
    }

    /**
     * Arg.
     *
     * @param name the name
     * @param n the n
     * @param buf the buf
     */
    private void arg(String name, int n, ShortBuffer buf) {
        arg(name, toString(n, buf));
    }

    /**
     * Arg pointer.
     *
     * @param size the size
     * @param type the type
     * @param stride the stride
     * @param pointer the pointer
     */
    private void argPointer(int size, int type, int stride, Buffer pointer) {
        arg("size", size);
        arg("type", getPointerTypeName(type));
        arg("stride", stride);
        arg("pointer", pointer.toString());
    }

    /**
     * Gets the hex.
     *
     * @param value the value
     * @return the hex
     */
    private static String getHex(int value) {
        return "0x" + Integer.toHexString(value);
    }

    /**
     * Gets the error string.
     *
     * @param error the error
     * @return the error string
     */
    public static String getErrorString(int error) {
        switch (error) {
        case GL_NO_ERROR:
            return "GL_NO_ERROR";
        case GL_INVALID_ENUM:
            return "GL_INVALID_ENUM";
        case GL_INVALID_VALUE:
            return "GL_INVALID_VALUE";
        case GL_INVALID_OPERATION:
            return "GL_INVALID_OPERATION";
        case GL_STACK_OVERFLOW:
            return "GL_STACK_OVERFLOW";
        case GL_STACK_UNDERFLOW:
            return "GL_STACK_UNDERFLOW";
        case GL_OUT_OF_MEMORY:
            return "GL_OUT_OF_MEMORY";
        default:
            return getHex(error);
        }
    }

    /**
     * Gets the clear buffer mask.
     *
     * @param mask the mask
     * @return the clear buffer mask
     */
    private String getClearBufferMask(int mask) {
        StringBuilder b = new StringBuilder();
        if ((mask & GL_DEPTH_BUFFER_BIT) != 0) {
            b.append("GL_DEPTH_BUFFER_BIT");
            mask &= ~GL_DEPTH_BUFFER_BIT;
        }
        if ((mask & GL_STENCIL_BUFFER_BIT) != 0) {
            if (b.length() > 0) {
                b.append(" | ");
            }
            b.append("GL_STENCIL_BUFFER_BIT");
            mask &= ~GL_STENCIL_BUFFER_BIT;
        }
        if ((mask & GL_COLOR_BUFFER_BIT) != 0) {
            if (b.length() > 0) {
                b.append(" | ");
            }
            b.append("GL_COLOR_BUFFER_BIT");
            mask &= ~GL_COLOR_BUFFER_BIT;
        }
        if (mask != 0) {
            if (b.length() > 0) {
                b.append(" | ");
            }
            b.append(getHex(mask));
        }
        return b.toString();
    }

    /**
     * Gets the factor.
     *
     * @param factor the factor
     * @return the factor
     */
    private String getFactor(int factor) {
        switch(factor) {
        case GL_ZERO:
            return "GL_ZERO";
        case GL_ONE:
            return "GL_ONE";
        case GL_SRC_COLOR:
            return "GL_SRC_COLOR";
        case GL_ONE_MINUS_SRC_COLOR:
            return "GL_ONE_MINUS_SRC_COLOR";
        case GL_DST_COLOR:
            return "GL_DST_COLOR";
        case GL_ONE_MINUS_DST_COLOR:
            return "GL_ONE_MINUS_DST_COLOR";
        case GL_SRC_ALPHA:
            return "GL_SRC_ALPHA";
        case GL_ONE_MINUS_SRC_ALPHA:
            return "GL_ONE_MINUS_SRC_ALPHA";
        case GL_DST_ALPHA:
            return "GL_DST_ALPHA";
        case GL_ONE_MINUS_DST_ALPHA:
            return "GL_ONE_MINUS_DST_ALPHA";
        case GL_SRC_ALPHA_SATURATE:
            return "GL_SRC_ALPHA_SATURATE";

        default:
            return getHex(factor);
        }
    }

    /**
     * Gets the shade model.
     *
     * @param model the model
     * @return the shade model
     */
    private String getShadeModel(int model) {
        switch(model) {
        case GL_FLAT:
            return "GL_FLAT";
        case GL_SMOOTH:
            return "GL_SMOOTH";
        default:
            return getHex(model);
        }
    }

    /**
     * Gets the texture target.
     *
     * @param target the target
     * @return the texture target
     */
    private String getTextureTarget(int target) {
        switch (target) {
        case GL_TEXTURE_2D:
            return "GL_TEXTURE_2D";
        default:
            return getHex(target);
        }
    }

    /**
     * Gets the texture env target.
     *
     * @param target the target
     * @return the texture env target
     */
    private String getTextureEnvTarget(int target) {
        switch (target) {
        case GL_TEXTURE_ENV:
            return "GL_TEXTURE_ENV";
        default:
            return getHex(target);
        }
    }

    /**
     * Gets the texture env p name.
     *
     * @param pname the pname
     * @return the texture env p name
     */
    private String getTextureEnvPName(int pname) {
        switch (pname) {
        case GL_TEXTURE_ENV_MODE:
            return "GL_TEXTURE_ENV_MODE";
        case GL_TEXTURE_ENV_COLOR:
            return "GL_TEXTURE_ENV_COLOR";
        default:
            return getHex(pname);
        }
    }

    /**
     * Gets the texture env param count.
     *
     * @param pname the pname
     * @return the texture env param count
     */
    private int getTextureEnvParamCount(int pname) {
        switch (pname) {
        case GL_TEXTURE_ENV_MODE:
            return 1;
        case GL_TEXTURE_ENV_COLOR:
            return 4;
        default:
            return 0;
        }
    }

    /**
     * Gets the texture env param name.
     *
     * @param param the param
     * @return the texture env param name
     */
    private String getTextureEnvParamName(float param) {
        int iparam = (int) param;
        if (param == (float) iparam) {
            switch (iparam) {
            case GL_REPLACE:
                return "GL_REPLACE";
            case GL_MODULATE:
                return "GL_MODULATE";
            case GL_DECAL:
                return "GL_DECAL";
            case GL_BLEND:
                return "GL_BLEND";
            case GL_ADD:
                return "GL_ADD";
            case GL_COMBINE:
                return "GL_COMBINE";
            default:
                return getHex(iparam);
            }
        }
        return Float.toString(param);
    }

    /**
     * Gets the matrix mode.
     *
     * @param matrixMode the matrix mode
     * @return the matrix mode
     */
    private String getMatrixMode(int matrixMode) {
        switch (matrixMode) {
        case GL_MODELVIEW:
            return "GL_MODELVIEW";
        case GL_PROJECTION:
            return "GL_PROJECTION";
        case GL_TEXTURE:
            return "GL_TEXTURE";
        default:
            return getHex(matrixMode);
        }
    }

    /**
     * Gets the client state.
     *
     * @param clientState the client state
     * @return the client state
     */
    private String getClientState(int clientState) {
        switch (clientState) {
        case GL_COLOR_ARRAY:
            return "GL_COLOR_ARRAY";
        case GL_VERTEX_ARRAY:
            return "GL_VERTEX_ARRAY";
        case GL_NORMAL_ARRAY:
            return "GL_NORMAL_ARRAY";
        case GL_TEXTURE_COORD_ARRAY:
            return "GL_TEXTURE_COORD_ARRAY";
        default:
            return getHex(clientState);
        }
    }

    /**
     * Gets the cap.
     *
     * @param cap the cap
     * @return the cap
     */
    private String getCap(int cap) {
        switch (cap) {
        case GL_FOG:
            return "GL_FOG";
        case GL_LIGHTING:
            return "GL_LIGHTING";
        case GL_TEXTURE_2D:
            return "GL_TEXTURE_2D";
        case GL_CULL_FACE:
            return "GL_CULL_FACE";
        case GL_ALPHA_TEST:
            return "GL_ALPHA_TEST";
        case GL_BLEND:
            return "GL_BLEND";
        case GL_COLOR_LOGIC_OP:
            return "GL_COLOR_LOGIC_OP";
        case GL_DITHER:
            return "GL_DITHER";
        case GL_STENCIL_TEST:
            return "GL_STENCIL_TEST";
        case GL_DEPTH_TEST:
            return "GL_DEPTH_TEST";
        case GL_LIGHT0:
            return "GL_LIGHT0";
        case GL_LIGHT1:
            return "GL_LIGHT1";
        case GL_LIGHT2:
            return "GL_LIGHT2";
        case GL_LIGHT3:
            return "GL_LIGHT3";
        case GL_LIGHT4:
            return "GL_LIGHT4";
        case GL_LIGHT5:
            return "GL_LIGHT5";
        case GL_LIGHT6:
            return "GL_LIGHT6";
        case GL_LIGHT7:
            return "GL_LIGHT7";
        case GL_POINT_SMOOTH:
            return "GL_POINT_SMOOTH";
        case GL_LINE_SMOOTH:
            return "GL_LINE_SMOOTH";
        case GL_COLOR_MATERIAL:
            return "GL_COLOR_MATERIAL";
        case GL_NORMALIZE:
            return "GL_NORMALIZE";
        case GL_RESCALE_NORMAL:
            return "GL_RESCALE_NORMAL";
        case GL_VERTEX_ARRAY:
            return "GL_VERTEX_ARRAY";
        case GL_NORMAL_ARRAY:
            return "GL_NORMAL_ARRAY";
        case GL_COLOR_ARRAY:
            return "GL_COLOR_ARRAY";
        case GL_TEXTURE_COORD_ARRAY:
            return "GL_TEXTURE_COORD_ARRAY";
        case GL_MULTISAMPLE:
            return "GL_MULTISAMPLE";
        case GL_SAMPLE_ALPHA_TO_COVERAGE:
            return "GL_SAMPLE_ALPHA_TO_COVERAGE";
        case GL_SAMPLE_ALPHA_TO_ONE:
            return "GL_SAMPLE_ALPHA_TO_ONE";
        case GL_SAMPLE_COVERAGE:
            return "GL_SAMPLE_COVERAGE";
        case GL_SCISSOR_TEST:
            return "GL_SCISSOR_TEST";
        default:
            return getHex(cap);
        }
    }

    /**
     * Gets the texture p name.
     *
     * @param pname the pname
     * @return the texture p name
     */
    private String getTexturePName(int pname) {
        switch (pname) {
        case GL_TEXTURE_MAG_FILTER:
            return "GL_TEXTURE_MAG_FILTER";
        case GL_TEXTURE_MIN_FILTER:
            return "GL_TEXTURE_MIN_FILTER";
        case GL_TEXTURE_WRAP_S:
            return "GL_TEXTURE_WRAP_S";
        case GL_TEXTURE_WRAP_T:
            return "GL_TEXTURE_WRAP_T";
        case GL_GENERATE_MIPMAP:
            return "GL_GENERATE_MIPMAP";
        case GL_TEXTURE_CROP_RECT_OES:
            return "GL_TEXTURE_CROP_RECT_OES";
        default:
            return getHex(pname);
        }
    }

    /**
     * Gets the texture param name.
     *
     * @param param the param
     * @return the texture param name
     */
    private String getTextureParamName(float param) {
        int iparam = (int) param;
        if (param == (float) iparam) {
            switch (iparam) {
            case GL_CLAMP_TO_EDGE:
                return "GL_CLAMP_TO_EDGE";
            case GL_REPEAT:
                return "GL_REPEAT";
            case GL_NEAREST:
                return "GL_NEAREST";
            case GL_LINEAR:
                return "GL_LINEAR";
            case GL_NEAREST_MIPMAP_NEAREST:
                return "GL_NEAREST_MIPMAP_NEAREST";
            case GL_LINEAR_MIPMAP_NEAREST:
                return "GL_LINEAR_MIPMAP_NEAREST";
            case GL_NEAREST_MIPMAP_LINEAR:
                return "GL_NEAREST_MIPMAP_LINEAR";
            case GL_LINEAR_MIPMAP_LINEAR:
                return "GL_LINEAR_MIPMAP_LINEAR";
            default:
                return getHex(iparam);
            }
        }
        return Float.toString(param);
    }

    /**
     * Gets the fog p name.
     *
     * @param pname the pname
     * @return the fog p name
     */
    private String getFogPName(int pname) {
        switch (pname) {
        case GL_FOG_DENSITY:
            return "GL_FOG_DENSITY";
        case GL_FOG_START:
            return "GL_FOG_START";
        case GL_FOG_END:
            return "GL_FOG_END";
        case GL_FOG_MODE:
            return "GL_FOG_MODE";
        case GL_FOG_COLOR:
            return "GL_FOG_COLOR";
        default:
            return getHex(pname);
        }
    }

    /**
     * Gets the fog param count.
     *
     * @param pname the pname
     * @return the fog param count
     */
    private int getFogParamCount(int pname) {
        switch (pname) {
        case GL_FOG_DENSITY:
            return 1;
        case GL_FOG_START:
            return 1;
        case GL_FOG_END:
            return 1;
        case GL_FOG_MODE:
            return 1;
        case GL_FOG_COLOR:
            return 4;
        default:
            return 0;
        }
    }

    /**
     * Gets the begin mode.
     *
     * @param mode the mode
     * @return the begin mode
     */
    private String getBeginMode(int mode) {
        switch (mode) {
        case GL_POINTS:
            return "GL_POINTS";
        case GL_LINES:
            return "GL_LINES";
        case GL_LINE_LOOP:
            return "GL_LINE_LOOP";
        case GL_LINE_STRIP:
            return "GL_LINE_STRIP";
        case GL_TRIANGLES:
            return "GL_TRIANGLES";
        case GL_TRIANGLE_STRIP:
            return "GL_TRIANGLE_STRIP";
        case GL_TRIANGLE_FAN:
            return "GL_TRIANGLE_FAN";
        default:
            return getHex(mode);
        }
    }

    /**
     * Gets the index type.
     *
     * @param type the type
     * @return the index type
     */
    private String getIndexType(int type) {
        switch (type) {
        case GL_UNSIGNED_SHORT:
            return "GL_UNSIGNED_SHORT";
        case GL_UNSIGNED_BYTE:
            return "GL_UNSIGNED_BYTE";
        default:
            return getHex(type);
        }
    }

    /**
     * Gets the integer state name.
     *
     * @param pname the pname
     * @return the integer state name
     */
    private String getIntegerStateName(int pname) {
        switch (pname) {
        case GL_ALPHA_BITS:
            return "GL_ALPHA_BITS";
        case GL_ALIASED_LINE_WIDTH_RANGE:
            return "GL_ALIASED_LINE_WIDTH_RANGE";
        case GL_ALIASED_POINT_SIZE_RANGE:
            return "GL_ALIASED_POINT_SIZE_RANGE";
        case GL_BLUE_BITS:
            return "GL_BLUE_BITS";
        case GL_COMPRESSED_TEXTURE_FORMATS:
            return "GL_COMPRESSED_TEXTURE_FORMATS";
        case GL_DEPTH_BITS:
            return "GL_DEPTH_BITS";
        case GL_GREEN_BITS:
            return "GL_GREEN_BITS";
        case GL_MAX_ELEMENTS_INDICES:
            return "GL_MAX_ELEMENTS_INDICES";
        case GL_MAX_ELEMENTS_VERTICES:
            return "GL_MAX_ELEMENTS_VERTICES";
        case GL_MAX_LIGHTS:
            return "GL_MAX_LIGHTS";
        case GL_MAX_TEXTURE_SIZE:
            return "GL_MAX_TEXTURE_SIZE";
        case GL_MAX_VIEWPORT_DIMS:
            return "GL_MAX_VIEWPORT_DIMS";
        case GL_MAX_MODELVIEW_STACK_DEPTH:
            return "GL_MAX_MODELVIEW_STACK_DEPTH";
        case GL_MAX_PROJECTION_STACK_DEPTH:
            return "GL_MAX_PROJECTION_STACK_DEPTH";
        case GL_MAX_TEXTURE_STACK_DEPTH:
            return "GL_MAX_TEXTURE_STACK_DEPTH";
        case GL_MAX_TEXTURE_UNITS:
            return "GL_MAX_TEXTURE_UNITS";
        case GL_NUM_COMPRESSED_TEXTURE_FORMATS:
            return "GL_NUM_COMPRESSED_TEXTURE_FORMATS";
        case GL_RED_BITS:
            return "GL_RED_BITS";
        case GL_SMOOTH_LINE_WIDTH_RANGE:
            return "GL_SMOOTH_LINE_WIDTH_RANGE";
        case GL_SMOOTH_POINT_SIZE_RANGE:
            return "GL_SMOOTH_POINT_SIZE_RANGE";
        case GL_STENCIL_BITS:
            return "GL_STENCIL_BITS";
        case GL_SUBPIXEL_BITS:
            return "GL_SUBPIXEL_BITS";

        case GL_MODELVIEW_MATRIX_FLOAT_AS_INT_BITS_OES:
            return "GL_MODELVIEW_MATRIX_FLOAT_AS_INT_BITS_OES";
        case GL_PROJECTION_MATRIX_FLOAT_AS_INT_BITS_OES:
            return "GL_PROJECTION_MATRIX_FLOAT_AS_INT_BITS_OES";
        case GL_TEXTURE_MATRIX_FLOAT_AS_INT_BITS_OES:
            return "GL_TEXTURE_MATRIX_FLOAT_AS_INT_BITS_OES";

        default:
            return getHex(pname);
        }
    }

    /**
     * Gets the integer state size.
     *
     * @param pname the pname
     * @return the integer state size
     */
    private int getIntegerStateSize(int pname) {
        switch (pname) {
        case GL_ALPHA_BITS:
            return 1;
        case GL_ALIASED_LINE_WIDTH_RANGE:
            return 2;
        case GL_ALIASED_POINT_SIZE_RANGE:
            return 2;
        case GL_BLUE_BITS:
            return 1;
        case GL_COMPRESSED_TEXTURE_FORMATS:
            // Have to ask the implementation for the size
        {
            int[] buffer = new int[1];
            mgl.glGetIntegerv(GL_NUM_COMPRESSED_TEXTURE_FORMATS, buffer, 0);
            return buffer[0];
        }
        case GL_DEPTH_BITS:
            return 1;
        case GL_GREEN_BITS:
            return 1;
        case GL_MAX_ELEMENTS_INDICES:
            return 1;
        case GL_MAX_ELEMENTS_VERTICES:
            return 1;
        case GL_MAX_LIGHTS:
            return 1;
        case GL_MAX_TEXTURE_SIZE:
            return 1;
        case GL_MAX_VIEWPORT_DIMS:
            return 2;
        case GL_MAX_MODELVIEW_STACK_DEPTH:
            return 1;
        case GL_MAX_PROJECTION_STACK_DEPTH:
            return 1;
        case GL_MAX_TEXTURE_STACK_DEPTH:
            return 1;
        case GL_MAX_TEXTURE_UNITS:
            return 1;
        case GL_NUM_COMPRESSED_TEXTURE_FORMATS:
            return 1;
        case GL_RED_BITS:
            return 1;
        case GL_SMOOTH_LINE_WIDTH_RANGE:
            return 2;
        case GL_SMOOTH_POINT_SIZE_RANGE:
            return 2;
        case GL_STENCIL_BITS:
            return 1;
        case GL_SUBPIXEL_BITS:
            return 1;

        case GL_MODELVIEW_MATRIX_FLOAT_AS_INT_BITS_OES:
        case GL_PROJECTION_MATRIX_FLOAT_AS_INT_BITS_OES:
        case GL_TEXTURE_MATRIX_FLOAT_AS_INT_BITS_OES:
            return 16;

        default:
            return 0;
        }
    }

    /**
     * Gets the integer state format.
     *
     * @param pname the pname
     * @return the integer state format
     */
    private int getIntegerStateFormat(int pname) {
        switch (pname) {
        case GL_MODELVIEW_MATRIX_FLOAT_AS_INT_BITS_OES:
        case GL_PROJECTION_MATRIX_FLOAT_AS_INT_BITS_OES:
        case GL_TEXTURE_MATRIX_FLOAT_AS_INT_BITS_OES:
            return FORMAT_FLOAT;

        default:
            return FORMAT_INT;
        }
    }

    /**
     * Gets the hint target.
     *
     * @param target the target
     * @return the hint target
     */
    private String getHintTarget(int target) {
        switch (target) {
        case GL_FOG_HINT:
            return "GL_FOG_HINT";
        case GL_LINE_SMOOTH_HINT:
            return "GL_LINE_SMOOTH_HINT";
        case GL_PERSPECTIVE_CORRECTION_HINT:
            return "GL_PERSPECTIVE_CORRECTION_HINT";
        case GL_POINT_SMOOTH_HINT:
            return "GL_POINT_SMOOTH_HINT";
        case GL_POLYGON_SMOOTH_HINT:
            return "GL_POLYGON_SMOOTH_HINT";
        case GL_GENERATE_MIPMAP_HINT:
            return "GL_GENERATE_MIPMAP_HINT";
        default:
            return getHex(target);
        }
    }

    /**
     * Gets the hint mode.
     *
     * @param mode the mode
     * @return the hint mode
     */
    private String getHintMode(int mode) {
        switch (mode) {
        case GL_FASTEST:
            return "GL_FASTEST";
        case GL_NICEST:
            return "GL_NICEST";
        case GL_DONT_CARE:
            return "GL_DONT_CARE";
        default:
            return getHex(mode);
        }
    }

    /**
     * Gets the face name.
     *
     * @param face the face
     * @return the face name
     */
    private String getFaceName(int face) {
        switch (face) {
        case GL_FRONT_AND_BACK:
            return "GL_FRONT_AND_BACK";
        default:
            return getHex(face);
        }
    }

    /**
     * Gets the material p name.
     *
     * @param pname the pname
     * @return the material p name
     */
    private String getMaterialPName(int pname) {
        switch (pname) {
        case GL_AMBIENT:
            return "GL_AMBIENT";
        case GL_DIFFUSE:
            return "GL_DIFFUSE";
        case GL_SPECULAR:
            return "GL_SPECULAR";
        case GL_EMISSION:
            return "GL_EMISSION";
        case GL_SHININESS:
            return "GL_SHININESS";
        case GL_AMBIENT_AND_DIFFUSE:
            return "GL_AMBIENT_AND_DIFFUSE";
        default:
            return getHex(pname);
        }
    }

    /**
     * Gets the material param count.
     *
     * @param pname the pname
     * @return the material param count
     */
    private int getMaterialParamCount(int pname) {
        switch (pname) {
        case GL_AMBIENT:
            return 4;
        case GL_DIFFUSE:
            return 4;
        case GL_SPECULAR:
            return 4;
        case GL_EMISSION:
            return 4;
        case GL_SHININESS:
            return 1;
        case GL_AMBIENT_AND_DIFFUSE:
            return 4;
        default:
            return 0;
        }
    }

    /**
     * Gets the light name.
     *
     * @param light the light
     * @return the light name
     */
    private String getLightName(int light) {
        if (light >= GL_LIGHT0 && light <= GL_LIGHT7) {
            return "GL_LIGHT" + Integer.toString(light);
        }
        return getHex(light);
    }

    /**
     * Gets the light p name.
     *
     * @param pname the pname
     * @return the light p name
     */
    private String getLightPName(int pname) {
        switch (pname) {
        case GL_AMBIENT:
            return "GL_AMBIENT";
        case GL_DIFFUSE:
            return "GL_DIFFUSE";
        case GL_SPECULAR:
            return "GL_SPECULAR";
        case GL_POSITION:
            return "GL_POSITION";
        case GL_SPOT_DIRECTION:
            return "GL_SPOT_DIRECTION";
        case GL_SPOT_EXPONENT:
            return "GL_SPOT_EXPONENT";
        case GL_SPOT_CUTOFF:
            return "GL_SPOT_CUTOFF";
        case GL_CONSTANT_ATTENUATION:
            return "GL_CONSTANT_ATTENUATION";
        case GL_LINEAR_ATTENUATION:
            return "GL_LINEAR_ATTENUATION";
        case GL_QUADRATIC_ATTENUATION:
            return "GL_QUADRATIC_ATTENUATION";
        default:
            return getHex(pname);
        }
    }

    /**
     * Gets the light param count.
     *
     * @param pname the pname
     * @return the light param count
     */
    private int getLightParamCount(int pname) {
        switch (pname) {
        case GL_AMBIENT:
            return 4;
        case GL_DIFFUSE:
            return 4;
        case GL_SPECULAR:
            return 4;
        case GL_POSITION:
            return 4;
        case GL_SPOT_DIRECTION:
            return 3;
        case GL_SPOT_EXPONENT:
            return 1;
        case GL_SPOT_CUTOFF:
            return 1;
        case GL_CONSTANT_ATTENUATION:
            return 1;
        case GL_LINEAR_ATTENUATION:
            return 1;
        case GL_QUADRATIC_ATTENUATION:
            return 1;
        default:
            return 0;
        }
    }

    /**
     * Gets the light model p name.
     *
     * @param pname the pname
     * @return the light model p name
     */
    private String getLightModelPName(int pname) {
        switch (pname) {
        case GL_LIGHT_MODEL_AMBIENT:
            return "GL_LIGHT_MODEL_AMBIENT";
        case GL_LIGHT_MODEL_TWO_SIDE:
            return "GL_LIGHT_MODEL_TWO_SIDE";
        default:
            return getHex(pname);
        }
    }

    /**
     * Gets the light model param count.
     *
     * @param pname the pname
     * @return the light model param count
     */
    private int getLightModelParamCount(int pname) {
        switch (pname) {
        case GL_LIGHT_MODEL_AMBIENT:
            return 4;
        case GL_LIGHT_MODEL_TWO_SIDE:
            return 1;
        default:
            return 0;
        }
    }

    /**
     * Gets the pointer type name.
     *
     * @param type the type
     * @return the pointer type name
     */
    private String getPointerTypeName(int type) {
        switch (type) {
        case GL_BYTE:
            return "GL_BYTE";
        case GL_UNSIGNED_BYTE:
            return "GL_UNSIGNED_BYTE";
        case GL_SHORT:
            return "GL_SHORT";
        case GL_FIXED:
            return "GL_FIXED";
        case GL_FLOAT:
            return "GL_FLOAT";
        default:
            return getHex(type);
        }
    }

    /**
     * To byte buffer.
     *
     * @param byteCount the byte count
     * @param input the input
     * @return the byte buffer
     */
    private ByteBuffer toByteBuffer(int byteCount, Buffer input) {
        ByteBuffer result = null;
        boolean convertWholeBuffer = (byteCount < 0);
        if (input instanceof ByteBuffer) {
            ByteBuffer input2 = (ByteBuffer) input;
            int position = input2.position();
            if (convertWholeBuffer) {
                byteCount = input2.limit() - position;
            }
            result = ByteBuffer.allocate(byteCount).order(input2.order());
            for (int i = 0; i < byteCount; i++) {
                result.put(input2.get());
            }
            input2.position(position);
        } else if (input instanceof CharBuffer) {
            CharBuffer input2 = (CharBuffer) input;
            int position = input2.position();
            if (convertWholeBuffer) {
                byteCount = (input2.limit() - position) * 2;
            }
            result = ByteBuffer.allocate(byteCount).order(input2.order());
            CharBuffer result2 = result.asCharBuffer();
            for (int i = 0; i < byteCount / 2; i++) {
                result2.put(input2.get());
            }
            input2.position(position);
        } else if (input instanceof ShortBuffer) {
            ShortBuffer input2 = (ShortBuffer) input;
            int position = input2.position();
            if (convertWholeBuffer) {
                byteCount = (input2.limit() - position)* 2;
            }
            result = ByteBuffer.allocate(byteCount).order(input2.order());
            ShortBuffer result2 = result.asShortBuffer();
            for (int i = 0; i < byteCount / 2; i++) {
                result2.put(input2.get());
            }
            input2.position(position);
        } else if (input instanceof IntBuffer) {
            IntBuffer input2 = (IntBuffer) input;
            int position = input2.position();
            if (convertWholeBuffer) {
                byteCount = (input2.limit() - position) * 4;
            }
            result = ByteBuffer.allocate(byteCount).order(input2.order());
            IntBuffer result2 = result.asIntBuffer();
            for (int i = 0; i < byteCount / 4; i++) {
                result2.put(input2.get());
            }
            input2.position(position);
        } else if (input instanceof FloatBuffer) {
            FloatBuffer input2 = (FloatBuffer) input;
            int position = input2.position();
            if (convertWholeBuffer) {
                byteCount = (input2.limit() - position) * 4;
            }
            result = ByteBuffer.allocate(byteCount).order(input2.order());
            FloatBuffer result2 = result.asFloatBuffer();
            for (int i = 0; i < byteCount / 4; i++) {
                result2.put(input2.get());
            }
            input2.position(position);
        } else if (input instanceof DoubleBuffer) {
            DoubleBuffer input2 = (DoubleBuffer) input;
            int position = input2.position();
            if (convertWholeBuffer) {
                byteCount = (input2.limit() - position) * 8;
            }
            result = ByteBuffer.allocate(byteCount).order(input2.order());
            DoubleBuffer result2 = result.asDoubleBuffer();
            for (int i = 0; i < byteCount / 8; i++) {
                result2.put(input2.get());
            }
            input2.position(position);
        } else if (input instanceof LongBuffer) {
            LongBuffer input2 = (LongBuffer) input;
            int position = input2.position();
            if (convertWholeBuffer) {
                byteCount = (input2.limit() - position) * 8;
            }
            result = ByteBuffer.allocate(byteCount).order(input2.order());
            LongBuffer result2 = result.asLongBuffer();
            for (int i = 0; i < byteCount / 8; i++) {
                result2.put(input2.get());
            }
            input2.position(position);
        } else {
            throw new RuntimeException("Unimplemented Buffer subclass.");
        }
        result.rewind();
        // The OpenGL API will interpret the result in hardware byte order,
        // so we better do that as well:
        result.order(ByteOrder.nativeOrder());
        return result;
    }

    /**
     * To char indices.
     *
     * @param count the count
     * @param type the type
     * @param indices the indices
     * @return the char[]
     */
    private char[] toCharIndices(int count, int type, Buffer indices) {
        char[] result = new char[count];
        switch (type) {
        case GL_UNSIGNED_BYTE: {
            ByteBuffer byteBuffer = toByteBuffer(count, indices);
            byte[] array = byteBuffer.array();
            int offset = byteBuffer.arrayOffset();
            for (int i = 0; i < count; i++) {
                result[i] = (char) (0xff & array[offset + i]);
            }
        }
            break;
        case GL_UNSIGNED_SHORT: {
            CharBuffer charBuffer;
            if (indices instanceof CharBuffer) {
                charBuffer = (CharBuffer) indices;
            } else {
                ByteBuffer byteBuffer = toByteBuffer(count * 2, indices);
                charBuffer = byteBuffer.asCharBuffer();
            }
            int oldPosition = charBuffer.position();
            charBuffer.position(0);
            charBuffer.get(result);
            charBuffer.position(oldPosition);
        }
            break;
        default:
            // Don't throw an exception, because we don't want logging to
            // change the behavior.
            break;
        }
        return result;
    }

    /**
     * Do array element.
     *
     * @param builder the builder
     * @param enabled the enabled
     * @param name the name
     * @param pointer the pointer
     * @param index the index
     */
    private void doArrayElement(StringBuilder builder, boolean enabled,
            String name, PointerInfo pointer, int index) {
        if (!enabled) {
            return;
        }
        builder.append(" ");
        builder.append(name + ":{");
        if (pointer == null || pointer.mTempByteBuffer == null ) {
            builder.append("undefined }");
            return;
        }
        if (pointer.mStride < 0) {
            builder.append("invalid stride");
            return;
        }

        int stride = pointer.getStride();
        ByteBuffer byteBuffer = pointer.mTempByteBuffer;
        int size = pointer.mSize;
        int type = pointer.mType;
        int sizeofType = pointer.sizeof(type);
        int byteOffset = stride * index;
        for (int i = 0; i < size; i++) {
            if (i > 0) {
                builder.append(", ");
            }
            switch (type) {
            case GL_BYTE: {
                byte d = byteBuffer.get(byteOffset);
                builder.append(Integer.toString(d));
            }
                break;
            case GL_UNSIGNED_BYTE: {
                byte d = byteBuffer.get(byteOffset);
                builder.append(Integer.toString(0xff & d));
            }
                break;
            case GL_SHORT: {
                ShortBuffer shortBuffer = byteBuffer.asShortBuffer();
                short d = shortBuffer.get(byteOffset / 2);
                builder.append(Integer.toString(d));
            }
                break;
            case GL_FIXED: {
                IntBuffer intBuffer = byteBuffer.asIntBuffer();
                int d = intBuffer.get(byteOffset / 4);
                builder.append(Integer.toString(d));
            }
                break;
            case GL_FLOAT: {
                FloatBuffer intBuffer = byteBuffer.asFloatBuffer();
                float d = intBuffer.get(byteOffset / 4);
                builder.append(Float.toString(d));
            }
                break;
            default:
                builder.append("?");
                break;
            }
            byteOffset += sizeofType;
        }
        builder.append("}");
    }

    /**
     * Do element.
     *
     * @param builder the builder
     * @param ordinal the ordinal
     * @param vertexIndex the vertex index
     */
    private void doElement(StringBuilder builder, int ordinal, int vertexIndex) {
        builder.append(" [" + ordinal + " : " + vertexIndex + "] =");
        doArrayElement(builder, mVertexArrayEnabled, "v", mVertexPointer,
                vertexIndex);
        doArrayElement(builder, mNormalArrayEnabled, "n", mNormalPointer,
                vertexIndex);
        doArrayElement(builder, mColorArrayEnabled, "c", mColorPointer,
                vertexIndex);
        doArrayElement(builder, mTextureCoordArrayEnabled, "t",
                mTexCoordPointer, vertexIndex);
        builder.append("\n");
        // Vertex
        // Normal
        // Color
        // TexCoord
    }

    /**
     * Bind arrays.
     */
    private void bindArrays() {
        if (mColorArrayEnabled)
            mColorPointer.bindByteBuffer();
        if (mNormalArrayEnabled)
            mNormalPointer.bindByteBuffer();
        if (mTextureCoordArrayEnabled)
            mTexCoordPointer.bindByteBuffer();
        if (mVertexArrayEnabled)
            mVertexPointer.bindByteBuffer();
    }

    /**
     * Unbind arrays.
     */
    private void unbindArrays() {
        if (mColorArrayEnabled)
            mColorPointer.unbindByteBuffer();
        if (mNormalArrayEnabled)
            mNormalPointer.unbindByteBuffer();
        if (mTextureCoordArrayEnabled)
            mTexCoordPointer.unbindByteBuffer();
        if (mVertexArrayEnabled)
            mVertexPointer.unbindByteBuffer();
    }

    /**
     * Start log indices.
     */
    private void startLogIndices() {
        mStringBuilder = new StringBuilder();
        mStringBuilder.append("\n");
        bindArrays();
    }

    /**
     * End log indices.
     */
    private void endLogIndices() {
        log(mStringBuilder.toString());
        unbindArrays();
    }

    // ---------------------------------------------------------------------
    // GL10 methods:

    
    /**
     * Gl active texture.
     *
     * @param texture the texture
     * @see{@link javax.microedition.khronos.opengles.GL10#glActiveTexture(int)}
     */
    public void glActiveTexture(int texture) {
        begin("glActiveTexture");
        arg("texture", texture);
        end();
        mgl.glActiveTexture(texture);
        checkError();
    }

    
    /**
     * Gl alpha func.
     *
     * @param func the func
     * @param ref the ref
     * @see{@link javax.microedition.khronos.opengles.GL10#glAlphaFunc(int, float)}
     */
    public void glAlphaFunc(int func, float ref) {
        begin("glAlphaFunc");
        arg("func", func);
        arg("ref", ref);
        end();
        mgl.glAlphaFunc(func, ref);
        checkError();
    }

    
    /**
     * Gl alpha funcx.
     *
     * @param func the func
     * @param ref the ref
     * @see{@link javax.microedition.khronos.opengles.GL10#glAlphaFuncx(int, int)}
     */
    public void glAlphaFuncx(int func, int ref) {
        begin("glAlphaFuncx");
        arg("func", func);
        arg("ref", ref);
        end();
        mgl.glAlphaFuncx(func, ref);
        checkError();
    }

    
    /**
     * Gl bind texture.
     *
     * @param target the target
     * @param texture the texture
     * @see javax.microedition.khronos.opengles.GL10#glBindTexture(int, int)
     */
    public void glBindTexture(int target, int texture) {
        begin("glBindTexture");
        arg("target", getTextureTarget(target));
        arg("texture", texture);
        end();
        mgl.glBindTexture(target, texture);
        checkError();
    }

   
    /**
     * Gl blend func.
     *
     * @param sfactor the sfactor
     * @param dfactor the dfactor
     * @see javax.microedition.khronos.opengles.GL10#glBlendFunc(int,int)
     */
    public void glBlendFunc(int sfactor, int dfactor) {
        begin("glBlendFunc");
        arg("sfactor", getFactor(sfactor));
        arg("dfactor", getFactor(dfactor));
        end();

        mgl.glBlendFunc(sfactor, dfactor);
        checkError();
    }

   
    /**
     * Gl clear.
     *
     * @param mask the mask
     * @see javax.microedition.khronos.opengles.GL10#glClear(int)
     */
    public void glClear(int mask) {
        begin("glClear");
        arg("mask", getClearBufferMask(mask));
        end();

        mgl.glClear(mask);
        checkError();
    }

   
    /**
     * Gl clear color.
     *
     * @param red the red
     * @param green the green
     * @param blue the blue
     * @param alpha the alpha
     * @see javax.microedition.khronos.opengles.GL10#glClearColor(float, float, float, float)
     */
    public void glClearColor(float red, float green, float blue, float alpha) {
        begin("glClearColor");
        arg("red", red);
        arg("green", green);
        arg("blue", blue);
        arg("alpha", alpha);
        end();

        mgl.glClearColor(red, green, blue, alpha);
        checkError();
    }

  
    /**
     * Gl clear colorx.
     *
     * @param red the red
     * @param green the green
     * @param blue the blue
     * @param alpha the alpha
     * @see javax.microdedition.khronos.opengles.GL10#glClearColorx(int, int, int, int)
     */
    public void glClearColorx(int red, int green, int blue, int alpha) {
        begin("glClearColor");
        arg("red", red);
        arg("green", green);
        arg("blue", blue);
        arg("alpha", alpha);
        end();

        mgl.glClearColorx(red, green, blue, alpha);
        checkError();
    }

    
    /**
     * Gl clear depthf.
     *
     * @param depth the depth
     * @see javax.microedition.khronos.opengles.GL10#glClearDepthf(float)
     */
    public void glClearDepthf(float depth) {
        begin("glClearDepthf");
        arg("depth", depth);
        end();

        mgl.glClearDepthf(depth);
        checkError();
    }

   
    /**
     * Gl clear depthx.
     *
     * @param depth the depth
     * @see javax.microedition.khronos.opengles.GL10#glClearDepthx(int)
     */
    public void glClearDepthx(int depth) {
        begin("glClearDepthx");
        arg("depth", depth);
        end();

        mgl.glClearDepthx(depth);
        checkError();
    }

    
    /**
     * Gl clear stencil.
     *
     * @param s the s
     * @see javax.microedition.khronos.opengles.GL10#glClearStencil(int)
     */
    public void glClearStencil(int s) {
        begin("glClearStencil");
        arg("s", s);
        end();

        mgl.glClearStencil(s);
        checkError();
    }

    
    /**
     * Gl client active texture.
     *
     * @param texture the texture
     * @see javax.microedition.khronos.opengles.GL10#glClientActiveTexture(int)
     */
    public void glClientActiveTexture(int texture) {
        begin("glClientActiveTexture");
        arg("texture", texture);
        end();

        mgl.glClientActiveTexture(texture);
        checkError();
    }

   
    /**
     * Gl color4f.
     *
     * @param red the red
     * @param green the green
     * @param blue the blue
     * @param alpha the alpha
     * @see javax.microedition.khronos.opengles.GL10#glColor4f(float, float, float, float)
     */
    public void glColor4f(float red, float green, float blue, float alpha) {
        begin("glColor4f");
        arg("red", red);
        arg("green", green);
        arg("blue", blue);
        arg("alpha", alpha);
        end();

        mgl.glColor4f(red, green, blue, alpha);
        checkError();
    }

    
    /**
     * Gl color4x.
     *
     * @param red the red
     * @param green the green
     * @param blue the blue
     * @param alpha the alpha
     * @see javax.microedition.khronos.opengles.GL10#glColor4x(int, int, int, int)
     */
    public void glColor4x(int red, int green, int blue, int alpha) {
        begin("glColor4x");
        arg("red", red);
        arg("green", green);
        arg("blue", blue);
        arg("alpha", alpha);
        end();

        mgl.glColor4x(red, green, blue, alpha);
        checkError();
    }

   
    /**
     * Gl color mask.
     *
     * @param red the red
     * @param green the green
     * @param blue the blue
     * @param alpha the alpha
     * @see javax.microedition.khronos.opengles.GL10#glColorMack(boolean, boolean, boolean, boolean)
     */
    public void glColorMask(boolean red, boolean green, boolean blue,
            boolean alpha) {
        begin("glColorMask");
        arg("red", red);
        arg("green", green);
        arg("blue", blue);
        arg("alpha", alpha);
        end();

        mgl.glColorMask(red, green, blue, alpha);
        checkError();
    }

    
    /**
     * Gl color pointer.
     *
     * @param size the size
     * @param type the type
     * @param stride the stride
     * @param pointer the pointer
     * @see javax.microedition.khronos.opengles.GL10#glColorPointer(int, int, int, java.nio.Buffer)
     */
    public void glColorPointer(int size, int type, int stride, Buffer pointer) {
        begin("glColorPointer");
        argPointer(size, type, stride, pointer);
        end();
        mColorPointer = new PointerInfo(size, type, stride, pointer);

        mgl.glColorPointer(size, type, stride, pointer);
        checkError();
    }

   
    /**
     * Gl compressed tex image2 d.
     *
     * @param target the target
     * @param level the level
     * @param internalformat the internalformat
     * @param width the width
     * @param height the height
     * @param border the border
     * @param imageSize the image size
     * @param data the data
     * @see javax.microedition.khronos.opengles.GL10#glCompressedTexImage2D(int, int, int, int, int, int, int, java.nio.Buffer)
     */
    public void glCompressedTexImage2D(int target, int level,
            int internalformat, int width, int height, int border,
            int imageSize, Buffer data) {
        begin("glCompressedTexImage2D");
        arg("target", getTextureTarget(target));
        arg("level", level);
        arg("internalformat", internalformat);
        arg("width", width);
        arg("height", height);
        arg("border", border);
        arg("imageSize", imageSize);
        arg("data", data.toString());
        end();

        mgl.glCompressedTexImage2D(target, level, internalformat, width,
                height, border, imageSize, data);
        checkError();
    }

    
    /**
     * Gl compressed tex sub image2 d.
     *
     * @param target the target
     * @param level the level
     * @param xoffset the xoffset
     * @param yoffset the yoffset
     * @param width the width
     * @param height the height
     * @param format the format
     * @param imageSize the image size
     * @param data the data
     * @see javax.microedition.khronos.opengles.GL10#glCompressedtexSubImage2D(int, int, int, int, int, int, int,java.nio.Buffer)
     */
    public void glCompressedTexSubImage2D(int target, int level, int xoffset,
            int yoffset, int width, int height, int format, int imageSize,
            Buffer data) {
        begin("glCompressedTexSubImage2D");
        arg("target", getTextureTarget(target));
        arg("level", level);
        arg("xoffset", xoffset);
        arg("yoffset", yoffset);
        arg("width", width);
        arg("height", height);
        arg("format", format);
        arg("imageSize", imageSize);
        arg("data", data.toString());
        end();

        mgl.glCompressedTexSubImage2D(target, level, xoffset, yoffset, width,
                height, format, imageSize, data);
        checkError();
    }

    
    /**
     * Gl copy tex image2 d.
     *
     * @param target the target
     * @param level the level
     * @param internalformat the internalformat
     * @param x the x
     * @param y the y
     * @param width the width
     * @param height the height
     * @param border the border
     * @see javax.microedition.khronos.opengles.GL10#glCopyTexImage2D(int, int, int, int, int, int,int,int)
     */
    public void glCopyTexImage2D(int target, int level, int internalformat,
            int x, int y, int width, int height, int border) {
        begin("glCopyTexImage2D");
        arg("target", getTextureTarget(target));
        arg("level", level);
        arg("internalformat", internalformat);
        arg("x", x);
        arg("y", y);
        arg("width", width);
        arg("height", height);
        arg("border", border);
        end();

        mgl.glCopyTexImage2D(target, level, internalformat, x, y, width,
                height, border);
        checkError();
    }

   
    /**
     * Gl copy tex sub image2 d.
     *
     * @param target the target
     * @param level the level
     * @param xoffset the xoffset
     * @param yoffset the yoffset
     * @param x the x
     * @param y the y
     * @param width the width
     * @param height the height
     * @see javax.microedition.khronos.opengles.GL10#glCopyTexSubImage2D(int, int, int, int, int, int, int)
     */
    public void glCopyTexSubImage2D(int target, int level, int xoffset,
            int yoffset, int x, int y, int width, int height) {
        begin("glCopyTexSubImage2D");
        arg("target", getTextureTarget(target));
        arg("level", level);
        arg("xoffset", xoffset);
        arg("yoffset", yoffset);
        arg("x", x);
        arg("y", y);
        arg("width", width);
        arg("height", height);
        end();

        mgl.glCopyTexSubImage2D(target, level, xoffset, yoffset, x, y, width,
                height);
        checkError();
    }

   
    /**
     * Gl cull face.
     *
     * @param mode the mode
     * @see javax.microedition.khronos.opengles.GL10#glCullFace(int)
     */
    public void glCullFace(int mode) {
        begin("glCullFace");
        arg("mode", mode);
        end();

        mgl.glCullFace(mode);
        checkError();
    }

   
    /**
     * Gl delete textures.
     *
     * @param n the n
     * @param textures the textures
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL10#glDeleteTextures(int, int[], int)
     */
    public void glDeleteTextures(int n, int[] textures, int offset) {
        begin("glDeleteTextures");
        arg("n", n);
        arg("textures", n, textures, offset);
        arg("offset", offset);
        end();

        mgl.glDeleteTextures(n, textures, offset);
        checkError();
    }

   
    /**
     * Gl delete textures.
     *
     * @param n the n
     * @param textures the textures
     * @see javax.microedition.khronos.opengles.GL10#glDeleteTextures(int, java.nio.Buffer)
     */
    public void glDeleteTextures(int n, IntBuffer textures) {
        begin("glDeleteTextures");
        arg("n", n);
        arg("textures", n, textures);
        end();

        mgl.glDeleteTextures(n, textures);
        checkError();
    }

   
    /**
     * Gl depth func.
     *
     * @param func the func
     * @see javax.microedition.khronos.opengles.GL10#glDepthfunc(int)
     */
    public void glDepthFunc(int func) {
        begin("glDepthFunc");
        arg("func", func);
        end();

        mgl.glDepthFunc(func);
        checkError();
    }

   
    /**
     * Gl depth mask.
     *
     * @param flag the flag
     * @see javax.microedition.khronos.opengles.GL10#glDepthMask(boolean)
     */
    public void glDepthMask(boolean flag) {
        begin("glDepthMask");
        arg("flag", flag);
        end();

        mgl.glDepthMask(flag);
        checkError();
    }

   
    /**
     * Gl depth rangef.
     *
     * @param near the near
     * @param far the far
     * @see javax.microedition.khronos.opengles.GL10#glDepthRangef(float, float)
     */
    public void glDepthRangef(float near, float far) {
        begin("glDepthRangef");
        arg("near", near);
        arg("far", far);
        end();

        mgl.glDepthRangef(near, far);
        checkError();
    }

    
    /**
     * Gl depth rangex.
     *
     * @param near the near
     * @param far the far
     * @see java.xmicroedition.khronos.opengles.GL10#glDepthRangex(int, int)
     */
    public void glDepthRangex(int near, int far) {
        begin("glDepthRangex");
        arg("near", near);
        arg("far", far);
        end();

        mgl.glDepthRangex(near, far);
        checkError();
    }

    
    /**
     * Gl disable.
     *
     * @param cap the cap
     * @see javax.microedition.khronos.opengles.GL10#glDisable(int)
     */
    public void glDisable(int cap) {
        begin("glDisable");
        arg("cap", getCap(cap));
        end();

        mgl.glDisable(cap);
        checkError();
    }

   
    /**
     * Gl disable client state.
     *
     * @param array the array
     * @see javax.microedition.khronos.opengles.GL10#glDisableClientState(int)
     */
    public void glDisableClientState(int array) {
        begin("glDisableClientState");
        arg("array", getClientState(array));
        end();

        switch (array) {
        case GL_COLOR_ARRAY:
            mColorArrayEnabled = false;
            break;
        case GL_NORMAL_ARRAY:
            mNormalArrayEnabled = false;
            break;
        case GL_TEXTURE_COORD_ARRAY:
            mTextureCoordArrayEnabled = false;
            break;
        case GL_VERTEX_ARRAY:
            mVertexArrayEnabled = false;
            break;
        }
        mgl.glDisableClientState(array);
        checkError();
    }

    
    /**
     * Gl draw arrays.
     *
     * @param mode the mode
     * @param first the first
     * @param count the count
     * @see javax.microedition.khronos.opengles.GL10#glDrawArrays(int, int, int)
     */
    public void glDrawArrays(int mode, int first, int count) {
        begin("glDrawArrays");
        arg("mode", mode);
        arg("first", first);
        arg("count", count);
        startLogIndices();
        for (int i = 0; i < count; i++) {
            doElement(mStringBuilder, i, first + i);
        }
        endLogIndices();
        end();

        mgl.glDrawArrays(mode, first, count);
        checkError();
    }

    
    /**
     * Gl draw elements.
     *
     * @param mode the mode
     * @param count the count
     * @param type the type
     * @param indices the indices
     * @see javax.microedition.khronos.opengles.GL10#glDrawElements(int, int, int, java.nio.Buffer)
     */
    public void glDrawElements(int mode, int count, int type, Buffer indices) {
        begin("glDrawElements");
        arg("mode", getBeginMode(mode));
        arg("count", count);
        arg("type", getIndexType(type));
        char[] indexArray = toCharIndices(count, type, indices);
        int indexArrayLength = indexArray.length;
        startLogIndices();
        for (int i = 0; i < indexArrayLength; i++) {
            doElement(mStringBuilder, i, indexArray[i]);
        }
        endLogIndices();
        end();

        mgl.glDrawElements(mode, count, type, indices);
        checkError();
    }

   
    /**
     * Gl enable.
     *
     * @param cap the cap
     * @see javax.microedition.khronos.opengles.GL10#glEnable(int)
     */
    public void glEnable(int cap) {
        begin("glEnable");
        arg("cap", getCap(cap));
        end();

        mgl.glEnable(cap);
        checkError();
    }

    
    /**
     * Gl enable client state.
     *
     * @param array the array
     * @see javax.microedition.khronos.opengles.GL10#glEnableClientState(int)
     */
    public void glEnableClientState(int array) {
        begin("glEnableClientState");
        arg("array", getClientState(array));
        end();

        switch (array) {
        case GL_COLOR_ARRAY:
            mColorArrayEnabled = true;
            break;
        case GL_NORMAL_ARRAY:
            mNormalArrayEnabled = true;
            break;
        case GL_TEXTURE_COORD_ARRAY:
            mTextureCoordArrayEnabled = true;
            break;
        case GL_VERTEX_ARRAY:
            mVertexArrayEnabled = true;
            break;
        }
        mgl.glEnableClientState(array);
        checkError();
    }

    
    /**
     * Gl finish.
     *
     * @see javax.microedition.khronos.opengles.GL10#glFinish()
     */
    public void glFinish() {
        begin("glFinish");
        end();

        mgl.glFinish();
        checkError();
    }

   
    /**
     * Gl flush.
     *
     * @see javax.microedition.khronos.opengles.GL10#glFlush()
     */
    public void glFlush() {
        begin("glFlush");
        end();

        mgl.glFlush();
        checkError();
    }

    
    /**
     * Gl fogf.
     *
     * @param pname the pname
     * @param param the param
     * @see javax.microedition.khronos.opengles.GL10#glFogf(int, float)
     */
    public void glFogf(int pname, float param) {
        begin("glFogf");
        arg("pname", pname);
        arg("param", param);
        end();

        mgl.glFogf(pname, param);
        checkError();
    }

    
    /**
     * Gl fogfv.
     *
     * @param pname the pname
     * @param params the params
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL10#glFogfv(int, float[], int)
     */
    public void glFogfv(int pname, float[] params, int offset) {
        begin("glFogfv");
        arg("pname", getFogPName(pname));
        arg("params", getFogParamCount(pname), params, offset);
        arg("offset", offset);
        end();

        mgl.glFogfv(pname, params, offset);
        checkError();
    }

    
    /**
     * Gl fogfv.
     *
     * @param pname the pname
     * @param params the params
     * @see javax.microedition.khronos.opengles.GL10#glFogfv(int, java.nio.FloatBuffer)
     */
    public void glFogfv(int pname, FloatBuffer params) {
        begin("glFogfv");
        arg("pname", getFogPName(pname));
        arg("params", getFogParamCount(pname), params);
        end();

        mgl.glFogfv(pname, params);
        checkError();
    }

   
    /**
     * Gl fogx.
     *
     * @param pname the pname
     * @param param the param
     * @see java.xmicroedition.khronos.opengles.GL10#glFogx(int, int)
     */
    public void glFogx(int pname, int param) {
        begin("glFogx");
        arg("pname", getFogPName(pname));
        arg("param", param);
        end();

        mgl.glFogx(pname, param);
        checkError();
    }

   
    /**
     * Gl fogxv.
     *
     * @param pname the pname
     * @param params the params
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL10#glFogxc(int, int[], int)
     */
    public void glFogxv(int pname, int[] params, int offset) {
        begin("glFogxv");
        arg("pname", getFogPName(pname));
        arg("params", getFogParamCount(pname), params, offset);
        arg("offset", offset);
        end();

        mgl.glFogxv(pname, params, offset);
        checkError();
    }

   
    /**
     * Gl fogxv.
     *
     * @param pname the pname
     * @param params the params
     * @see javax.microedition.khronos.opengles.GL10#glFogxv(int, java.nio.IntBuffer)
     */
    public void glFogxv(int pname, IntBuffer params) {
        begin("glFogxv");
        arg("pname", getFogPName(pname));
        arg("params", getFogParamCount(pname), params);
        end();

        mgl.glFogxv(pname, params);
        checkError();
    }

    
    /**
     * Gl front face.
     *
     * @param mode the mode
     * @see javax.microedition.khronos.opengles.GL10#glFontface(int)
     */
    public void glFrontFace(int mode) {
        begin("glFrontFace");
        arg("mode", mode);
        end();

        mgl.glFrontFace(mode);
        checkError();
    }

    
    /**
     * Gl frustumf.
     *
     * @param left the left
     * @param right the right
     * @param bottom the bottom
     * @param top the top
     * @param near the near
     * @param far the far
     * @see javax.microedition.khronos.opengles.GL10#glFrustumf(float, float, float, float, float, float)
     */
    public void glFrustumf(float left, float right, float bottom, float top,
            float near, float far) {
        begin("glFrustumf");
        arg("left", left);
        arg("right", right);
        arg("bottom", bottom);
        arg("top", top);
        arg("near", near);
        arg("far", far);
        end();

        mgl.glFrustumf(left, right, bottom, top, near, far);
        checkError();
    }

   
    /**
     * Gl frustumx.
     *
     * @param left the left
     * @param right the right
     * @param bottom the bottom
     * @param top the top
     * @param near the near
     * @param far the far
     * @see javax.microedition.khronos.opengles.GL10#glFrustumx(int, int, int, int, int, int)
     */
    public void glFrustumx(int left, int right, int bottom, int top, int near,
            int far) {
        begin("glFrustumx");
        arg("left", left);
        arg("right", right);
        arg("bottom", bottom);
        arg("top", top);
        arg("near", near);
        arg("far", far);
        end();

        mgl.glFrustumx(left, right, bottom, top, near, far);
        checkError();
    }

   
    /**
     * Gl gen textures.
     *
     * @param n the n
     * @param textures the textures
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL10#glGenTextures(int, int[], int)
     */
    public void glGenTextures(int n, int[] textures, int offset) {
        begin("glGenTextures");
        arg("n", n);
        arg("textures", Arrays.toString(textures));
        arg("offset", offset);

        mgl.glGenTextures(n, textures, offset);

        returns(toString(n, FORMAT_INT, textures, offset));

        checkError();
    }

   
    /**
     * Gl gen textures.
     *
     * @param n the n
     * @param textures the textures
     * @see javax.microedition.khronos.opengles.GL10#glGenTextures(int, java.nio.IntBuffer)
     */
    public void glGenTextures(int n, IntBuffer textures) {
        begin("glGenTextures");
        arg("n", n);
        arg("textures", textures.toString());

        mgl.glGenTextures(n, textures);

        returns(toString(n, FORMAT_INT, textures));

        checkError();
    }

   
    /**
     * Gl get error.
     *
     * @return the int
     * @see javax.microedition.khronos.opengles.GL10#glGetError()
     */
    public int glGetError() {
        begin("glGetError");

        int result = mgl.glGetError();

        returns(result);

        return result;
    }

   
    /**
     * Gl get integerv.
     *
     * @param pname the pname
     * @param params the params
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL10glGetIntegerv(int, int[], int)
     */
    public void glGetIntegerv(int pname, int[] params, int offset) {
        begin("glGetIntegerv");
        arg("pname", getIntegerStateName(pname));
        arg("params", Arrays.toString(params));
        arg("offset", offset);

        mgl.glGetIntegerv(pname, params, offset);

        returns(toString(getIntegerStateSize(pname),
                getIntegerStateFormat(pname), params, offset));

        checkError();
    }

   
    /**
     * Gl get integerv.
     *
     * @param pname the pname
     * @param params the params
     * @see javax.microedition.khronos.opengles.GL10#glGetIntegerv(int, java.nio.IntBuffer)
     */
    public void glGetIntegerv(int pname, IntBuffer params) {
        begin("glGetIntegerv");
        arg("pname", getIntegerStateName(pname));
        arg("params", params.toString());

        mgl.glGetIntegerv(pname, params);

        returns(toString(getIntegerStateSize(pname),
                getIntegerStateFormat(pname), params));

        checkError();
    }

   
    /**
     * Gl get string.
     *
     * @param name the name
     * @return the string
     * @see javax.microedition.khrnos.opengles.GL10#glGetString(int)
     */
    public String glGetString(int name) {
        begin("glGetString");
        arg("name", name);

        String result = mgl.glGetString(name);

        returns(result);

        checkError();
        return result;
    }

   
    /**
     * Gl hint.
     *
     * @param target the target
     * @param mode the mode
     * @see javax.microedition.khronos.opengles.GL10#glHint(int, int)
     */
    public void glHint(int target, int mode) {
        begin("glHint");
        arg("target", getHintTarget(target));
        arg("mode", getHintMode(mode));
        end();

        mgl.glHint(target, mode);
        checkError();
    }

    
    /**
     * Gl light modelf.
     *
     * @param pname the pname
     * @param param the param
     * @see javax.microedition.khronos.opengles.GL10#glLightModelf(int, float)
     */
    public void glLightModelf(int pname, float param) {
        begin("glLightModelf");
        arg("pname", getLightModelPName(pname));
        arg("param", param);
        end();

        mgl.glLightModelf(pname, param);
        checkError();
    }

    
    /**
     * Gl light modelfv.
     *
     * @param pname the pname
     * @param params the params
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL10#glLightModelfv(int, float[], int)
     */
    public void glLightModelfv(int pname, float[] params, int offset) {
        begin("glLightModelfv");
        arg("pname", getLightModelPName(pname));
        arg("params", getLightModelParamCount(pname), params, offset);
        arg("offset", offset);
        end();

        mgl.glLightModelfv(pname, params, offset);
        checkError();
    }

    
    /**
     * Gl light modelfv.
     *
     * @param pname the pname
     * @param params the params
     * @see javax.microedition.khronos.opengles.GL10#glLightModelfv(int, java.nio.FloatBuffer)
     */
    public void glLightModelfv(int pname, FloatBuffer params) {
        begin("glLightModelfv");
        arg("pname", getLightModelPName(pname));
        arg("params", getLightModelParamCount(pname), params);
        end();

        mgl.glLightModelfv(pname, params);
        checkError();
    }

   
    /**
     * Gl light modelx.
     *
     * @param pname the pname
     * @param param the param
     * @see javax.microedition.khronos.opengles.GL10#glLightModelx(int, int)
     */
    public void glLightModelx(int pname, int param) {
        begin("glLightModelx");
        arg("pname", getLightModelPName(pname));
        arg("param", param);
        end();

        mgl.glLightModelx(pname, param);
        checkError();
    }

   
    /**
     * Gl light modelxv.
     *
     * @param pname the pname
     * @param params the params
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL10#glLightModelxv(int, int[], int)
     */
    public void glLightModelxv(int pname, int[] params, int offset) {
        begin("glLightModelxv");
        arg("pname", getLightModelPName(pname));
        arg("params", getLightModelParamCount(pname), params, offset);
        arg("offset", offset);
        end();

        mgl.glLightModelxv(pname, params, offset);
        checkError();
    }

    
    /**
     * Gl light modelxv.
     *
     * @param pname the pname
     * @param params the params
     * @see javax.microedition.khronos.opengles.Gl10#glLightModelx(int, java.nio.IntBuffer)
     */
    public void glLightModelxv(int pname, IntBuffer params) {
        begin("glLightModelfv");
        arg("pname", getLightModelPName(pname));
        arg("params", getLightModelParamCount(pname), params);
        end();

        mgl.glLightModelxv(pname, params);
        checkError();
    }

    
    /**
     * Gl lightf.
     *
     * @param light the light
     * @param pname the pname
     * @param param the param
     * @see javax.microedition.khronos.opengles.GL10#glLightf(int, int, float)
     */
    public void glLightf(int light, int pname, float param) {
        begin("glLightf");
        arg("light", getLightName(light));
        arg("pname", getLightPName(pname));
        arg("param", param);
        end();

        mgl.glLightf(light, pname, param);
        checkError();
    }

   
    /**
     * Gl lightfv.
     *
     * @param light the light
     * @param pname the pname
     * @param params the params
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL10#glLightfv(int, int, float[])
     */
    public void glLightfv(int light, int pname, float[] params, int offset) {
        begin("glLightfv");
        arg("light", getLightName(light));
        arg("pname", getLightPName(pname));
        arg("params", getLightParamCount(pname), params, offset);
        arg("offset", offset);
        end();

        mgl.glLightfv(light, pname, params, offset);
        checkError();
    }

   
    /**
     * Gl lightfv.
     *
     * @param light the light
     * @param pname the pname
     * @param params the params
     * @see javax.microedition.khronos.opengles.GL10#glLightfv(int, int, java.nio.FloatBuffer)
     */
    public void glLightfv(int light, int pname, FloatBuffer params) {
        begin("glLightfv");
        arg("light", getLightName(light));
        arg("pname", getLightPName(pname));
        arg("params", getLightParamCount(pname), params);
        end();

        mgl.glLightfv(light, pname, params);
        checkError();
    }

    
    /**
     * Gl lightx.
     *
     * @param light the light
     * @param pname the pname
     * @param param the param
     * @see javax.microedition.khronos.opengles.GL10#glLightx(int, int, int)
     */
    public void glLightx(int light, int pname, int param) {
        begin("glLightx");
        arg("light", getLightName(light));
        arg("pname", getLightPName(pname));
        arg("param", param);
        end();

        mgl.glLightx(light, pname, param);
        checkError();
    }

   
    /**
     * Gl lightxv.
     *
     * @param light the light
     * @param pname the pname
     * @param params the params
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL10#glLightxv(int, int, int[], int)
     */
    public void glLightxv(int light, int pname, int[] params, int offset) {
        begin("glLightxv");
        arg("light", getLightName(light));
        arg("pname", getLightPName(pname));
        arg("params", getLightParamCount(pname), params, offset);
        arg("offset", offset);
        end();

        mgl.glLightxv(light, pname, params, offset);
        checkError();
    }

    
    /**
     * Gl lightxv.
     *
     * @param light the light
     * @param pname the pname
     * @param params the params
     * @see javax.microedition.khronos.opengles.GL10#glLightxv(int, int, java.nio.IntBuffer)
     */
    public void glLightxv(int light, int pname, IntBuffer params) {
        begin("glLightxv");
        arg("light", getLightName(light));
        arg("pname", getLightPName(pname));
        arg("params", getLightParamCount(pname), params);
        end();

        mgl.glLightxv(light, pname, params);
        checkError();
    }

    
    /**
     * Gl line width.
     *
     * @param width the width
     * @see javax.microedition.khronos.opengles.GL10#glLineWidth(float)
     */
    public void glLineWidth(float width) {
        begin("glLineWidth");
        arg("width", width);
        end();

        mgl.glLineWidth(width);
        checkError();
    }

    
    /**
     * Gl line widthx.
     *
     * @param width the width
     * @see javax.microedition.khronos.opengles.GL10#glLineWidthx(int)
     */
    public void glLineWidthx(int width) {
        begin("glLineWidthx");
        arg("width", width);
        end();

        mgl.glLineWidthx(width);
        checkError();
    }

    
    /**
     * Gl load identity.
     *
     * @see javax.microedition.khronos.opengles.GL10#glLoadIdentity()
     */
    public void glLoadIdentity() {
        begin("glLoadIdentity");
        end();

        mgl.glLoadIdentity();
        checkError();
    }

    
    /**
     * Gl load matrixf.
     *
     * @param m the m
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL10#glLoadMatrixf(float[], int)
     */
    public void glLoadMatrixf(float[] m, int offset) {
        begin("glLoadMatrixf");
        arg("m", 16, m, offset);
        arg("offset", offset);
        end();

        mgl.glLoadMatrixf(m, offset);
        checkError();
    }

    
    /**
     * Gl load matrixf.
     *
     * @param m the m
     * @see javax.microedition.khronos.opengles.GL10#glLoadMatrixf(java.nio.FloatBuffer)
     */
    public void glLoadMatrixf(FloatBuffer m) {
        begin("glLoadMatrixf");
        arg("m", 16, m);
        end();

        mgl.glLoadMatrixf(m);
        checkError();
    }

   
    /**
     * Gl load matrixx.
     *
     * @param m the m
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL10#glLoadMatrixx(int[], int)
     */
    public void glLoadMatrixx(int[] m, int offset) {
        begin("glLoadMatrixx");
        arg("m", 16, m, offset);
        arg("offset", offset);
        end();

        mgl.glLoadMatrixx(m, offset);
        checkError();
    }

   
    /**
     * Gl load matrixx.
     *
     * @param m the m
     * @see javax.microedition.khronos.opengles.GL103glLoadMatrixx(java.nio.IntBuffer)
     */
    public void glLoadMatrixx(IntBuffer m) {
        begin("glLoadMatrixx");
        arg("m", 16, m);
        end();

        mgl.glLoadMatrixx(m);
        checkError();
    }

   
    /**
     * Gl logic op.
     *
     * @param opcode the opcode
     * @see  javax.microedition.khronos.opengles.GL10#glLogicOp(int)
     */
    public void glLogicOp(int opcode) {
        begin("glLogicOp");
        arg("opcode", opcode);
        end();

        mgl.glLogicOp(opcode);
        checkError();
    }

   
    /**
     * Gl materialf.
     *
     * @param face the face
     * @param pname the pname
     * @param param the param
     * @see javax.microedition.khronos.opengles.GL10#glMaterialf(int, int, float)
     */
    public void glMaterialf(int face, int pname, float param) {
        begin("glMaterialf");
        arg("face", getFaceName(face));
        arg("pname", getMaterialPName(pname));
        arg("param", param);
        end();

        mgl.glMaterialf(face, pname, param);
        checkError();
    }

    
    /**
     * Gl materialfv.
     *
     * @param face the face
     * @param pname the pname
     * @param params the params
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL10#glMaterialfv(int, int, float[], int)
     */
    public void glMaterialfv(int face, int pname, float[] params, int offset) {
        begin("glMaterialfv");
        arg("face", getFaceName(face));
        arg("pname", getMaterialPName(pname));
        arg("params", getMaterialParamCount(pname), params, offset);
        arg("offset", offset);
        end();

        mgl.glMaterialfv(face, pname, params, offset);
        checkError();
    }

    
    /**
     * Gl materialfv.
     *
     * @param face the face
     * @param pname the pname
     * @param params the params
     * @see javax.microedition.khronos.opengles.GL10#glMaterialfv(int, int, java.nio.FloatBuffer)
     */
    public void glMaterialfv(int face, int pname, FloatBuffer params) {
        begin("glMaterialfv");
        arg("face", getFaceName(face));
        arg("pname", getMaterialPName(pname));
        arg("params", getMaterialParamCount(pname), params);
        end();

        mgl.glMaterialfv(face, pname, params);
        checkError();
    }

    
    /**
     * Gl materialx.
     *
     * @param face the face
     * @param pname the pname
     * @param param the param
     * @see  javax.microedition.khronos.opengles.GL10#glMaterialx(int, int, int)
     */
    public void glMaterialx(int face, int pname, int param) {
        begin("glMaterialx");
        arg("face", getFaceName(face));
        arg("pname", getMaterialPName(pname));
        arg("param", param);
        end();

        mgl.glMaterialx(face, pname, param);
        checkError();
    }

    
    /**
     * Gl materialxv.
     *
     * @param face the face
     * @param pname the pname
     * @param params the params
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL10#glMaterialxv(int, int, int[]. int)
     */
    public void glMaterialxv(int face, int pname, int[] params, int offset) {
        begin("glMaterialxv");
        arg("face", getFaceName(face));
        arg("pname", getMaterialPName(pname));
        arg("params", getMaterialParamCount(pname), params, offset);
        arg("offset", offset);
        end();

        mgl.glMaterialxv(face, pname, params, offset);
        checkError();
    }

    
    /**
     * Gl materialxv.
     *
     * @param face the face
     * @param pname the pname
     * @param params the params
     * @see javax.microedition.khronos.opengles.GL10#glMaterialxv(int, int, java.nio.IntBuffer)
     */
    public void glMaterialxv(int face, int pname, IntBuffer params) {
        begin("glMaterialxv");
        arg("face", getFaceName(face));
        arg("pname", getMaterialPName(pname));
        arg("params", getMaterialParamCount(pname), params);
        end();

        mgl.glMaterialxv(face, pname, params);
        checkError();
    }

  
    /**
     * Gl matrix mode.
     *
     * @param mode the mode
     * @see  javax.microedition.khronos.opengles.GL10#glMatrixMode(int)
     */
    public void glMatrixMode(int mode) {
        begin("glMatrixMode");
        arg("mode", getMatrixMode(mode));
        end();

        mgl.glMatrixMode(mode);
        checkError();
    }

    
    /**
     * Gl mult matrixf.
     *
     * @param m the m
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL10#glMultMatrixf(float[], int)
     */
    public void glMultMatrixf(float[] m, int offset) {
        begin("glMultMatrixf");
        arg("m", 16, m, offset);
        arg("offset", offset);
        end();

        mgl.glMultMatrixf(m, offset);
        checkError();
    }

    
    /**
     * Gl mult matrixf.
     *
     * @param m the m
     * @see javax.microedition.khronos.opengles.GL10#glMultMatrixf(java.nio.FloatBuffer)
     */
    public void glMultMatrixf(FloatBuffer m) {
        begin("glMultMatrixf");
        arg("m", 16, m);
        end();

        mgl.glMultMatrixf(m);
        checkError();
    }

    
    /**
     * Gl mult matrixx.
     *
     * @param m the m
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL10#glMultMatrixx(int[], int)
     */
    public void glMultMatrixx(int[] m, int offset) {
        begin("glMultMatrixx");
        arg("m", 16, m, offset);
        arg("offset", offset);
        end();

        mgl.glMultMatrixx(m, offset);
        checkError();
    }

   
    /**
     * Gl mult matrixx.
     *
     * @param m the m
     * @see javax.microedition.khronos.opengles.GL10#glMultMatrixx(java.nio.IntBuffer)
     */
    public void glMultMatrixx(IntBuffer m) {
        begin("glMultMatrixx");
        arg("m", 16, m);
        end();

        mgl.glMultMatrixx(m);
        checkError();
    }

    
    /**
     * Gl multi tex coord4f.
     *
     * @param target the target
     * @param s the s
     * @param t the t
     * @param r the r
     * @param q the q
     * @see  javax.microedition.khronos.opengles.GL10#glMultiTexCoord4f(int, float, float, float, float)
     */
    public void glMultiTexCoord4f(int target, float s, float t, float r, float q) {
        begin("glMultiTexCoord4f");
        arg("target", target);
        arg("s", s);
        arg("t", t);
        arg("r", r);
        arg("q", q);
        end();

        mgl.glMultiTexCoord4f(target, s, t, r, q);
        checkError();
    }

    
    /**
     * Gl multi tex coord4x.
     *
     * @param target the target
     * @param s the s
     * @param t the t
     * @param r the r
     * @param q the q
     * @see javax.microedition.khronos.opengles.GL10#glMultiTexCoord4x(int, int, int, int, int)
     */
    public void glMultiTexCoord4x(int target, int s, int t, int r, int q) {
        begin("glMultiTexCoord4x");
        arg("target", target);
        arg("s", s);
        arg("t", t);
        arg("r", r);
        arg("q", q);
        end();

        mgl.glMultiTexCoord4x(target, s, t, r, q);
        checkError();
    }

    
    /**
     * Gl normal3f.
     *
     * @param nx the nx
     * @param ny the ny
     * @param nz the nz
     * @see javax.microedition.khronos.opengles.GL10#glNormal3f(float, float, float)
     */
    public void glNormal3f(float nx, float ny, float nz) {
        begin("glNormal3f");
        arg("nx", nx);
        arg("ny", ny);
        arg("nz", nz);
        end();

        mgl.glNormal3f(nx, ny, nz);
        checkError();
    }

   
    /**
     * Gl normal3x.
     *
     * @param nx the nx
     * @param ny the ny
     * @param nz the nz
     * @see javax.microedition.khronos.opengles.GL10#glNormal3x(int, int, int)
     */
    public void glNormal3x(int nx, int ny, int nz) {
        begin("glNormal3x");
        arg("nx", nx);
        arg("ny", ny);
        arg("nz", nz);
        end();

        mgl.glNormal3x(nx, ny, nz);
        checkError();
    }

    
    /**
     * Gl normal pointer.
     *
     * @param type the type
     * @param stride the stride
     * @param pointer the pointer
     * @see javax.microedition.khronos.opengles.GL10#glNormalPointer(int, int, javax.nio.Buffer)
     */
    public void glNormalPointer(int type, int stride, Buffer pointer) {
        begin("glNormalPointer");
        arg("type", type);
        arg("stride", stride);
        arg("pointer", pointer.toString());
        end();
        mNormalPointer = new PointerInfo(3, type, stride, pointer);

        mgl.glNormalPointer(type, stride, pointer);
        checkError();
    }

    
    /**
     * Gl orthof.
     *
     * @param left the left
     * @param right the right
     * @param bottom the bottom
     * @param top the top
     * @param near the near
     * @param far the far
     * @see javax.microedition.khronos.opengles.GL10#glOrthof(float, float, float, float, float, float)
     */
    public void glOrthof(float left, float right, float bottom, float top,
            float near, float far) {
        begin("glOrthof");
        arg("left", left);
        arg("right", right);
        arg("bottom", bottom);
        arg("top", top);
        arg("near", near);
        arg("far", far);
        end();

        mgl.glOrthof(left, right, bottom, top, near, far);
        checkError();
    }

    
    /**
     * Gl orthox.
     *
     * @param left the left
     * @param right the right
     * @param bottom the bottom
     * @param top the top
     * @param near the near
     * @param far the far
     * @see javax.miroedition.khronos.opengles.GL10#glOrthox(int, int, int, int, int)
     */
    public void glOrthox(int left, int right, int bottom, int top, int near,
            int far) {
        begin("glOrthox");
        arg("left", left);
        arg("right", right);
        arg("bottom", bottom);
        arg("top", top);
        arg("near", near);
        arg("far", far);
        end();

        mgl.glOrthox(left, right, bottom, top, near, far);
        checkError();
    }

    
    /**
     * Gl pixel storei.
     *
     * @param pname the pname
     * @param param the param
     * @see javax.microedition.khronos.opengles.GL10#glPixelStorei(int, int)
     */
    public void glPixelStorei(int pname, int param) {
        begin("glPixelStorei");
        arg("pname", pname);
        arg("param", param);
        end();

        mgl.glPixelStorei(pname, param);
        checkError();
    }

    
    /**
     * Gl point size.
     *
     * @param size the size
     * @see javax.microedition.khronos.opengles.GL10#glPointSize(float)
     */
    public void glPointSize(float size) {
        begin("glPointSize");
        arg("size", size);
        end();

        mgl.glPointSize(size);
        checkError();
    }

   
    /**
     * Gl point sizex.
     *
     * @param size the size
     * @see javax.microedition.khronos.opengles.GL10#glPointSizex(int)
     */
    public void glPointSizex(int size) {
        begin("glPointSizex");
        arg("size", size);
        end();

        mgl.glPointSizex(size);
        checkError();
    }

    
    /**
     * Gl polygon offset.
     *
     * @param factor the factor
     * @param units the units
     * @see javax.microedition.khronos.opengles.GL10#glPolygonOffset(float,float)
     */
    public void glPolygonOffset(float factor, float units) {
        begin("glPolygonOffset");
        arg("factor", factor);
        arg("units", units);
        end();
        mgl.glPolygonOffset(factor, units);
        checkError();
    }

   
    /**
     * Gl polygon offsetx.
     *
     * @param factor the factor
     * @param units the units
     * @see javax.microedition.khronos.opengles.GL10#glPolygonOffsetx(int, int)
     */
    public void glPolygonOffsetx(int factor, int units) {
        begin("glPolygonOffsetx");
        arg("factor", factor);
        arg("units", units);
        end();

        mgl.glPolygonOffsetx(factor, units);
        checkError();
    }

   
    /**
     * Gl pop matrix.
     *
     * @see javax.microedition.khronos.opengles.GL10#glPopMatrix()
     */
    public void glPopMatrix() {
        begin("glPopMatrix");
        end();

        mgl.glPopMatrix();
        checkError();
    }

   
    /**
     * Gl push matrix.
     *
     * @see javax.microedition.khronos.opengles.GL10#glPushMatrix()
     */
    public void glPushMatrix() {
        begin("glPushMatrix");
        end();

        mgl.glPushMatrix();
        checkError();
    }

    
    /**
     * Gl read pixels.
     *
     * @param x the x
     * @param y the y
     * @param width the width
     * @param height the height
     * @param format the format
     * @param type the type
     * @param pixels the pixels
     * @see javax.microedition.khronos.opengles.GL10#glReadPixels(int, int, int, int,int, int, java.nio.Buffer)
     */
    public void glReadPixels(int x, int y, int width, int height, int format,
            int type, Buffer pixels) {
        begin("glReadPixels");
        arg("x", x);
        arg("y", y);
        arg("width", width);
        arg("height", height);
        arg("format", format);
        arg("type", type);
        arg("pixels", pixels.toString());
        end();

        mgl.glReadPixels(x, y, width, height, format, type, pixels);
        checkError();
    }

    
    /**
     * Gl rotatef.
     *
     * @param angle the angle
     * @param x the x
     * @param y the y
     * @param z the z
     * @see javax.microedition.khronos.opengles.GL10#glRotatef(float, float, float, float)
     */
    public void glRotatef(float angle, float x, float y, float z) {
        begin("glRotatef");
        arg("angle", angle);
        arg("x", x);
        arg("y", y);
        arg("z", z);
        end();

        mgl.glRotatef(angle, x, y, z);
        checkError();
    }

    
    /**
     * Gl rotatex.
     *
     * @param angle the angle
     * @param x the x
     * @param y the y
     * @param z the z
     * @see javax.microedition.khronos.opengles.GL10#glRotatex(int, int, int, int)
     */
    public void glRotatex(int angle, int x, int y, int z) {
        begin("glRotatex");
        arg("angle", angle);
        arg("x", x);
        arg("y", y);
        arg("z", z);
        end();

        mgl.glRotatex(angle, x, y, z);
        checkError();
    }

   
    /**
     * Gl sample coverage.
     *
     * @param value the value
     * @param invert the invert
     * @see javax.microedition.khronos.opengles.GL10#glSampleCoverage(float, boolean)
     */
    public void glSampleCoverage(float value, boolean invert) {
        begin("glSampleCoveragex");
        arg("value", value);
        arg("invert", invert);
        end();

        mgl.glSampleCoverage(value, invert);
        checkError();
    }

    
    /**
     * Gl sample coveragex.
     *
     * @param value the value
     * @param invert the invert
     * @see javax.microedition.khronos.opengles.GL10#glSampleCoveragex(int, boolean)
     */
    public void glSampleCoveragex(int value, boolean invert) {
        begin("glSampleCoveragex");
        arg("value", value);
        arg("invert", invert);
        end();

        mgl.glSampleCoveragex(value, invert);
        checkError();
    }

    
    /**
     * Gl scalef.
     *
     * @param x the x
     * @param y the y
     * @param z the z
     * @see javax.microedition.khronos.opengles.GL10#glScalef(float, float, float)
     */
    public void glScalef(float x, float y, float z) {
        begin("glScalef");
        arg("x", x);
        arg("y", y);
        arg("z", z);
        end();

        mgl.glScalef(x, y, z);
        checkError();
    }

    
    /**
     * Gl scalex.
     *
     * @param x the x
     * @param y the y
     * @param z the z
     * @see javax.microedition.khronos.opengles.GL10#glScalex(int, int, int)
     */
    public void glScalex(int x, int y, int z) {
        begin("glScalex");
        arg("x", x);
        arg("y", y);
        arg("z", z);
        end();

        mgl.glScalex(x, y, z);
        checkError();
    }

   
    /**
     * Gl scissor.
     *
     * @param x the x
     * @param y the y
     * @param width the width
     * @param height the height
     * @see javax.microedition.khronos.opengles.GL10#glScissor(int, int, int)
     */
    public void glScissor(int x, int y, int width, int height) {
        begin("glScissor");
        arg("x", x);
        arg("y", y);
        arg("width", width);
        arg("height", height);
        end();

        mgl.glScissor(x, y, width, height);
        checkError();
    }

    
    /**
     * Gl shade model.
     *
     * @param mode the mode
     * @see javax.microedition.khronos.opengles.GL10#glShadeModel(int)
     */
    public void glShadeModel(int mode) {
        begin("glShadeModel");
        arg("mode", getShadeModel(mode));
        end();

        mgl.glShadeModel(mode);
        checkError();
    }

    
    /**
     * Gl stencil func.
     *
     * @param func the func
     * @param ref the ref
     * @param mask the mask
     * @see  javax.microedition.khronos.opengles.GL10#glStencilFunc(int, int, int)
     */
    public void glStencilFunc(int func, int ref, int mask) {
        begin("glStencilFunc");
        arg("func", func);
        arg("ref", ref);
        arg("mask", mask);
        end();

        mgl.glStencilFunc(func, ref, mask);
        checkError();
    }

   
    /**
     * Gl stencil mask.
     *
     * @param mask the mask
     * @see javax.microedition.khronos.opengles.GL10#glStencilMask(int)
     */
    public void glStencilMask(int mask) {
        begin("glStencilMask");
        arg("mask", mask);
        end();

        mgl.glStencilMask(mask);
        checkError();
    }

    
    /**
     * Gl stencil op.
     *
     * @param fail the fail
     * @param zfail the zfail
     * @param zpass the zpass
     * @see  javax.microedition.khronos.opengles.GL10#glStencilOp(int, int, int)
     */
    public void glStencilOp(int fail, int zfail, int zpass) {
        begin("glStencilOp");
        arg("fail", fail);
        arg("zfail", zfail);
        arg("zpass", zpass);
        end();

        mgl.glStencilOp(fail, zfail, zpass);
        checkError();
    }

    
    /**
     * Gl tex coord pointer.
     *
     * @param size the size
     * @param type the type
     * @param stride the stride
     * @param pointer the pointer
     * @see javax.microedition.khronos.opengles.GL10#glTexCoordPointer(int, int, int, java.nio.Buffer)
     */
    public void glTexCoordPointer(int size, int type, int stride, Buffer pointer) {
        begin("glTexCoordPointer");
        argPointer(size, type, stride, pointer);
        end();
        mTexCoordPointer = new PointerInfo(size, type, stride, pointer);

        mgl.glTexCoordPointer(size, type, stride, pointer);
        checkError();
    }

    
    /**
     * Gl tex envf.
     *
     * @param target the target
     * @param pname the pname
     * @param param the param
     * @see javax.microedition.khronos.opengles.GL10#glTexEnvf(int, int, float)
     */
    public void glTexEnvf(int target, int pname, float param) {
        begin("glTexEnvf");
        arg("target", getTextureEnvTarget(target));
        arg("pname", getTextureEnvPName(pname));
        arg("param", getTextureEnvParamName(param));
        end();

        mgl.glTexEnvf(target, pname, param);
        checkError();
    }

   
    /**
     * Gl tex envfv.
     *
     * @param target the target
     * @param pname the pname
     * @param params the params
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL10#glTexEnvfv(int, int, float[], int)
     */
    public void glTexEnvfv(int target, int pname, float[] params, int offset) {
        begin("glTexEnvfv");
        arg("target", getTextureEnvTarget(target));
        arg("pname", getTextureEnvPName(pname));
        arg("params", getTextureEnvParamCount(pname), params, offset);
        arg("offset", offset);
        end();

        mgl.glTexEnvfv(target, pname, params, offset);
        checkError();
    }

    
    /**
     * Gl tex envfv.
     *
     * @param target the target
     * @param pname the pname
     * @param params the params
     * @see javax.microedition.khronos.opengles.GL10#glTexenvfv(int, int, java.nio.FloatBuffer)
     */
    public void glTexEnvfv(int target, int pname, FloatBuffer params) {
        begin("glTexEnvfv");
        arg("target", getTextureEnvTarget(target));
        arg("pname", getTextureEnvPName(pname));
        arg("params", getTextureEnvParamCount(pname), params);
        end();

        mgl.glTexEnvfv(target, pname, params);
        checkError();
    }

    
    /**
     * Gl tex envx.
     *
     * @param target the target
     * @param pname the pname
     * @param param the param
     * @see  javax.microedition.khronos.opengles.GL10#glTexEnvx(int, int, int)
     */
    public void glTexEnvx(int target, int pname, int param) {
        begin("glTexEnvx");
        arg("target", getTextureEnvTarget(target));
        arg("pname", getTextureEnvPName(pname));
        arg("param", param);
        end();

        mgl.glTexEnvx(target, pname, param);
        checkError();
    }

    
    /**
     * Gl tex envxv.
     *
     * @param target the target
     * @param pname the pname
     * @param params the params
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL10#glTexEnvxy(int, int, int[], int)
     */
    public void glTexEnvxv(int target, int pname, int[] params, int offset) {
        begin("glTexEnvxv");
        arg("target", getTextureEnvTarget(target));
        arg("pname", getTextureEnvPName(pname));
        arg("params", getTextureEnvParamCount(pname), params, offset);
        arg("offset", offset);
        end();

        mgl.glTexEnvxv(target, pname, params, offset);
        checkError();
    }

    
    /**
     * Gl tex envxv.
     *
     * @param target the target
     * @param pname the pname
     * @param params the params
     * @see javax.microedition.khronos.opengles.GL10#glTexEnvxv(int, int, java.nio.IntBuffer)
     */
    public void glTexEnvxv(int target, int pname, IntBuffer params) {
        begin("glTexEnvxv");
        arg("target", getTextureEnvTarget(target));
        arg("pname", getTextureEnvPName(pname));
        arg("params", getTextureEnvParamCount(pname), params);
        end();

        mgl.glTexEnvxv(target, pname, params);
        checkError();
    }

    
    /**
     * Gl tex image2 d.
     *
     * @param target the target
     * @param level the level
     * @param internalformat the internalformat
     * @param width the width
     * @param height the height
     * @param border the border
     * @param format the format
     * @param type the type
     * @param pixels the pixels
     * @see  javax.microedition.khronos.opengles.GL10#glTexImage2D(int, int, int, int, int, int, int, int, java.nio.Buffer)
     */
    public void glTexImage2D(int target, int level, int internalformat,
            int width, int height, int border, int format, int type,
            Buffer pixels) {
        begin("glTexImage2D");
        arg("target", target);
        arg("level", level);
        arg("internalformat", internalformat);
        arg("width", width);
        arg("height", height);
        arg("border", border);
        arg("format", format);
        arg("type", type);
        arg("pixels", pixels.toString());
        end();

        mgl.glTexImage2D(target, level, internalformat, width, height, border,
                format, type, pixels);
        checkError();
    }

   
    /**
     * Gl tex parameterf.
     *
     * @param target the target
     * @param pname the pname
     * @param param the param
     * @see  javax.microedition.khronos.opengles.GL10#glTexParameterf(int, int,float)
     */
    public void glTexParameterf(int target, int pname, float param) {
        begin("glTexParameterf");
        arg("target", getTextureTarget(target));
        arg("pname", getTexturePName(pname));
        arg("param", getTextureParamName(param));
        end();

        mgl.glTexParameterf(target, pname, param);
        checkError();
    }

   
    /**
     * Gl tex parameterx.
     *
     * @param target the target
     * @param pname the pname
     * @param param the param
     * @see javax.microedition.khronos.opengles.GL10#glTexParameterx(int, int, int)
     */
    public void glTexParameterx(int target, int pname, int param) {
        begin("glTexParameterx");
        arg("target", getTextureTarget(target));
        arg("pname", getTexturePName(pname));
        arg("param", param);
        end();

        mgl.glTexParameterx(target, pname, param);
        checkError();
    }

    
    /**
     * Gl tex parameteriv.
     *
     * @param target the target
     * @param pname the pname
     * @param params the params
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL11#glTexParameteriv(int, int, int[], int)
     */
    public void glTexParameteriv(int target, int pname, int[] params, int offset) {
        begin("glTexParameteriv");
        arg("target", getTextureTarget(target));
        arg("pname", getTexturePName(pname));
        arg("params", 4, params, offset);
        end();

        mgl11.glTexParameteriv(target, pname, params, offset);
        checkError();
    }

    
    /**
     * Gl tex parameteriv.
     *
     * @param target the target
     * @param pname the pname
     * @param params the params
     * @see javax.microedition.khronos.opengles.GL11#glTexParameteriv(int, int, java.nio.IntBuffer)
     */
    public void glTexParameteriv(int target, int pname, IntBuffer params) {
        begin("glTexParameteriv");
        arg("target", getTextureTarget(target));
        arg("pname", getTexturePName(pname));
        arg("params", 4, params);
        end();

        mgl11.glTexParameteriv(target, pname, params);
        checkError();
    }

    
    /**
     * Gl tex sub image2 d.
     *
     * @param target the target
     * @param level the level
     * @param xoffset the xoffset
     * @param yoffset the yoffset
     * @param width the width
     * @param height the height
     * @param format the format
     * @param type the type
     * @param pixels the pixels
     * @see javax.microedition.khronos.opengles.GL10#glTexSubImage2D(int, int, int, int, int, int, int, java.nio.Buffer)
     */
    public void glTexSubImage2D(int target, int level, int xoffset,
            int yoffset, int width, int height, int format, int type,
            Buffer pixels) {
        begin("glTexSubImage2D");
        arg("target", getTextureTarget(target));
        arg("level", level);
        arg("xoffset", xoffset);
        arg("yoffset", yoffset);
        arg("width", width);
        arg("height", height);
        arg("format", format);
        arg("type", type);
        arg("pixels", pixels.toString());
        end();
        mgl.glTexSubImage2D(target, level, xoffset, yoffset, width, height,
                format, type, pixels);
        checkError();
    }

    /**
     * Gl translatef.
     *
     * @param x the x
     * @param y the y
     * @param z the z
     * @see javax.microedition.khronos.opengles.GL10#glTranslatef(float, float, float)
     */
    public void glTranslatef(float x, float y, float z) {
        begin("glTranslatef");
        arg("x", x);
        arg("y", y);
        arg("z", z);
        end();
        mgl.glTranslatef(x, y, z);
        checkError();
    }

    /**
     * Gl translatex.
     *
     * @param x the x
     * @param y the y
     * @param z the z
     * @see javax.microedition.khronos.opengles.GL10#glTranslatex(int, int, int)
     */
    public void glTranslatex(int x, int y, int z) {
        begin("glTranslatex");
        arg("x", x);
        arg("y", y);
        arg("z", z);
        end();
        mgl.glTranslatex(x, y, z);
        checkError();
    }

    /**
     * Gl vertex pointer.
     *
     * @param size the size
     * @param type the type
     * @param stride the stride
     * @param pointer the pointer
     * @see javax.microedition.khronos.opengles.GL10#glVertexPointer(int, int, int, java.nio.Buffer)
     */
    public void glVertexPointer(int size, int type, int stride, Buffer pointer) {
        begin("glVertexPointer");
        argPointer(size, type, stride, pointer);
        end();
        mVertexPointer = new PointerInfo(size, type, stride, pointer);
        mgl.glVertexPointer(size, type, stride, pointer);
        checkError();
    }

    /**
     * Gl viewport.
     *
     * @param x the x
     * @param y the y
     * @param width the width
     * @param height the height
     * @see javax.microedition.khronos.opengles.GL10#glViewport(int, int, int, int)
     */
    public void glViewport(int x, int y, int width, int height) {
        begin("glViewport");
        arg("x", x);
        arg("y", y);
        arg("width", width);
        arg("height", height);
        end();
        mgl.glViewport(x, y, width, height);
        checkError();
    }

    /**
     * Gl clip planef.
     *
     * @param plane the plane
     * @param equation the equation
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL11#glClipPlanef(int, float[], int)
     */
    public void glClipPlanef(int plane, float[] equation, int offset) {
        begin("glClipPlanef");
        arg("plane", plane);
        arg("equation", 4, equation, offset);
        arg("offset", offset);
        end();
        mgl11.glClipPlanef(plane, equation, offset);
        checkError();
    }

    /**
     * Gl clip planef.
     *
     * @param plane the plane
     * @param equation the equation
     * @see javax.microedition.khronos.opengles.GL11#glClipPlanef(int, java.nio.FloatBuffer)
     */
    public void glClipPlanef(int plane, FloatBuffer equation) {
        begin("glClipPlanef");
        arg("plane", plane);
        arg("equation", 4, equation);
        end();
        mgl11.glClipPlanef(plane, equation);
        checkError();
    }

    /**
     * Gl clip planex.
     *
     * @param plane the plane
     * @param equation the equation
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL11#glClipPlanex(int, int[], int)
     */
    public void glClipPlanex(int plane, int[] equation, int offset) {
        begin("glClipPlanex");
        arg("plane", plane);
        arg("equation", 4, equation, offset);
        arg("offset", offset);
        end();
        mgl11.glClipPlanex(plane, equation, offset);
        checkError();
    }

    /**
     * Gl clip planex.
     *
     * @param plane the plane
     * @param equation the equation
     * @see javax.microedition.khronos.opengles.GL11#glClipPlanex(int, java.nio.IntBuffer)
     */
    public void glClipPlanex(int plane, IntBuffer equation) {
        begin("glClipPlanef");
        arg("plane", plane);
        arg("equation", 4, equation);
        end();
        mgl11.glClipPlanex(plane, equation);
        checkError();
    }

    // Draw Texture Extension

    /**
     * Gl draw texf oes.
     *
     * @param x the x
     * @param y the y
     * @param z the z
     * @param width the width
     * @param height the height
     * @see javax.microedition.khronos.opengles.GL11Ext#glDrawTexfOES(float, float, float, float, float)
     */
    public void glDrawTexfOES(float x, float y, float z,
        float width, float height) {
        begin("glDrawTexfOES");
        arg("x", x);
        arg("y", y);
        arg("z", z);
        arg("width", width);
        arg("height", height);
        end();
        mgl11Ext.glDrawTexfOES(x, y, z, width, height);
        checkError();
    }

    /**
     * Gl draw texfv oes.
     *
     * @param coords the coords
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL11Ext#glDrawTexfvOES(float[], int)
     */
    public void glDrawTexfvOES(float[] coords, int offset) {
        begin("glDrawTexfvOES");
        arg("coords", 5, coords, offset);
        arg("offset", offset);
        end();
        mgl11Ext.glDrawTexfvOES(coords, offset);
        checkError();
    }

    /**
     * Gl draw texfv oes.
     *
     * @param coords the coords
     * @see javax.microedition.khronos.opengles.GL11Ext#glDrawTexfvOES(java.nio.FloatBuffer)
     */
    public void glDrawTexfvOES(FloatBuffer coords) {
        begin("glDrawTexfvOES");
        arg("coords", 5, coords);
        end();
        mgl11Ext.glDrawTexfvOES(coords);
        checkError();
    }

    /**
     * Gl draw texi oes.
     *
     * @param x the x
     * @param y the y
     * @param z the z
     * @param width the width
     * @param height the height
     * @see javax.microedition.khronos.opengles.GL11Ext#glDrawTexiOES(int, int, int, int, int)
     */
    public void glDrawTexiOES(int x, int y, int z, int width, int height) {
        begin("glDrawTexiOES");
        arg("x", x);
        arg("y", y);
        arg("z", z);
        arg("width", width);
        arg("height", height);
        end();
        mgl11Ext.glDrawTexiOES(x, y, z, width, height);
        checkError();
    }

    /**
     * Gl draw texiv oes.
     *
     * @param coords the coords
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL11Ext#glDrawTexivOES(int[], int)
     */
    public void glDrawTexivOES(int[] coords, int offset) {
        begin("glDrawTexivOES");
        arg("coords", 5, coords, offset);
        arg("offset", offset);
        end();
        mgl11Ext.glDrawTexivOES(coords, offset);
        checkError();
    }

    /**
     * Gl draw texiv oes.
     *
     * @param coords the coords
     * @see javax.microedition.khronos.opengles.GL11Ext#glDrawTexivOES(java.nio.IntBuffer)
     */
    public void glDrawTexivOES(IntBuffer coords) {
        begin("glDrawTexivOES");
        arg("coords", 5, coords);
        end();
        mgl11Ext.glDrawTexivOES(coords);
        checkError();
    }

    /**
     * Gl draw texs oes.
     *
     * @param x the x
     * @param y the y
     * @param z the z
     * @param width the width
     * @param height the height
     * @see javax.microedition.khronos.opengles.GL11Ext#glDrawTexsOES(short, short, short, short, short)
     */
    public void glDrawTexsOES(short x, short y, short z,
        short width, short height) {
        begin("glDrawTexsOES");
        arg("x", x);
        arg("y", y);
        arg("z", z);
        arg("width", width);
        arg("height", height);
        end();
        mgl11Ext.glDrawTexsOES(x, y, z, width, height);
        checkError();
    }

    /**
     * Gl draw texsv oes.
     *
     * @param coords the coords
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL11Ext#glDrawTexsvOES(short[], int)
     */
    public void glDrawTexsvOES(short[] coords, int offset) {
        begin("glDrawTexsvOES");
        arg("coords", 5, coords, offset);
        arg("offset", offset);
        end();
        mgl11Ext.glDrawTexsvOES(coords, offset);
        checkError();
    }

    /**
     * Gl draw texsv oes.
     *
     * @param coords the coords
     * @see javax.microedition.khronos.opengles.GL11Ext#glDrawTexsvOES(java.nio.ShortBuffer)
     */
    public void glDrawTexsvOES(ShortBuffer coords) {
        begin("glDrawTexsvOES");
        arg("coords", 5, coords);
        end();
        mgl11Ext.glDrawTexsvOES(coords);
        checkError();
    }

    /**
     * Gl draw texx oes.
     *
     * @param x the x
     * @param y the y
     * @param z the z
     * @param width the width
     * @param height the height
     * @see javax.microedition.khronos.opengles.GL11Ext#glDrawTexxOES(int, int, int, int, int)
     */
    public void glDrawTexxOES(int x, int y, int z, int width, int height) {
        begin("glDrawTexxOES");
        arg("x", x);
        arg("y", y);
        arg("z", z);
        arg("width", width);
        arg("height", height);
        end();
        mgl11Ext.glDrawTexxOES(x, y, z, width, height);
        checkError();
    }

    /**
     * Gl draw texxv oes.
     *
     * @param coords the coords
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL11Ext#glDrawTexxvOES(int[], int)
     */
    public void glDrawTexxvOES(int[] coords, int offset) {
        begin("glDrawTexxvOES");
        arg("coords", 5, coords, offset);
        arg("offset", offset);
        end();
        mgl11Ext.glDrawTexxvOES(coords, offset);
        checkError();
    }

    /**
     * Gl draw texxv oes.
     *
     * @param coords the coords
     * @see javax.microedition.khronos.opengles.GL11Ext#glDrawTexxvOES(java.nio.IntBuffer)
     */
    public void glDrawTexxvOES(IntBuffer coords) {
        begin("glDrawTexxvOES");
        arg("coords", 5, coords);
        end();
        mgl11Ext.glDrawTexxvOES(coords);
        checkError();
    }

    /**
     * Gl query matrixx oes.
     *
     * @param mantissa the mantissa
     * @param mantissaOffset the mantissa offset
     * @param exponent the exponent
     * @param exponentOffset the exponent offset
     * @return the int
     * @see javax.microedition.khronos.opengles.GL10Ext#glQueryMatrixxOES(int[], int, int[], int)
     */
    public int glQueryMatrixxOES(int[] mantissa, int mantissaOffset,
        int[] exponent, int exponentOffset) {
        begin("glQueryMatrixxOES");
        arg("mantissa", Arrays.toString(mantissa));
        arg("exponent", Arrays.toString(exponent));
        end();
        int valid = mgl10Ext.glQueryMatrixxOES(mantissa, mantissaOffset,
            exponent, exponentOffset);
        returns(toString(16, FORMAT_FIXED, mantissa, mantissaOffset));
        returns(toString(16, FORMAT_INT, exponent, exponentOffset));
        checkError();
        return valid;
    }

    /**
     * Gl query matrixx oes.
     *
     * @param mantissa the mantissa
     * @param exponent the exponent
     * @return the int
     * @see javax.microedition.khronos.opengles.GL10Ext#glQueryMatrixxOES(java.nio.IntBuffer, java.nio.IntBuffer)
     */
    public int glQueryMatrixxOES(IntBuffer mantissa, IntBuffer exponent) {
        begin("glQueryMatrixxOES");
        arg("mantissa", mantissa.toString());
        arg("exponent", exponent.toString());
        end();
        int valid = mgl10Ext.glQueryMatrixxOES(mantissa, exponent);
        returns(toString(16, FORMAT_FIXED, mantissa));
        returns(toString(16, FORMAT_INT, exponent));
        checkError();
        return valid;
    }

    /**
     * Gl bind buffer.
     *
     * @param target the target
     * @param buffer the buffer
     * @see javax.microedition.khronos.opengles.GL11#glBindBuffer(int, int)
     */
    public void glBindBuffer(int target, int buffer) {
        begin("glBindBuffer");
        arg("target", target);
        arg("buffer", buffer);
        end();
        mgl11.glBindBuffer(target, buffer);
        checkError();
    }

    /**
     * Gl buffer data.
     *
     * @param target the target
     * @param size the size
     * @param data the data
     * @param usage the usage
     * @see javax.microedition.khronos.opengles.GL11#glBufferData(int, int, java.nio.Buffer, int)
     */
    public void glBufferData(int target, int size, Buffer data, int usage) {
        begin("glBufferData");
        arg("target", target);
        arg("size", size);
        arg("data", data.toString());
        arg("usage", usage);
        end();
        mgl11.glBufferData(target, size, data, usage);
        checkError();
    }

    
    /**
     * Gl buffer sub data.
     *
     * @param target the target
     * @param offset the offset
     * @param size the size
     * @param data the data
     * @see javax.microedition.khronos.opengles.GL11#glBufferSubData(int, int, int, java.nio.Buffer)
     */
    public void glBufferSubData(int target, int offset, int size, Buffer data) {
        begin("glBufferSubData");
        arg("target", target);
        arg("offset", offset);
        arg("size", size);
        arg("data", data.toString());
        end();
        mgl11.glBufferSubData(target, offset, size, data);
        checkError();
    }

   
    /**
     * Gl color4ub.
     *
     * @param red the red
     * @param green the green
     * @param blue the blue
     * @param alpha the alpha
     * @see javax.microedition.khronos.opengles.GL11#glColor4ub(byte, byte, byte, byte)
     */
    public void glColor4ub(byte red, byte green, byte blue, byte alpha) {
        begin("glColor4ub");
        arg("red", red);
        arg("green", green);
        arg("blue", blue);
        arg("alpha", alpha);
        end();
        mgl11.glColor4ub(red, green, blue, alpha);
        checkError();
    }

    
    /**
     * Gl delete buffers.
     *
     * @param n the n
     * @param buffers the buffers
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL11#glDeleteBuffers(int, int[], int)
     */
    public void glDeleteBuffers(int n, int[] buffers, int offset) {
        begin("glDeleteBuffers");
        arg("n", n);
        arg("buffers", buffers.toString());
        arg("offset", offset);
        end();
        mgl11.glDeleteBuffers(n, buffers, offset);
        checkError();
    }

    
    /**
     * Gl delete buffers.
     *
     * @param n the n
     * @param buffers the buffers
     * @see javax.microedition.khronos.opengles.GL11#glDeleteBuffers(int, java.nio.IntBuffer)
     */
    public void glDeleteBuffers(int n, IntBuffer buffers) {
        begin("glDeleteBuffers");
        arg("n", n);
        arg("buffers", buffers.toString());
        end();
        mgl11.glDeleteBuffers(n, buffers);
        checkError();
    }

   
    /**
     * Gl gen buffers.
     *
     * @param n the n
     * @param buffers the buffers
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL11#glGenBuffers(int, int[], int)
     */
    public void glGenBuffers(int n, int[] buffers, int offset) {
        begin("glGenBuffers");
        arg("n", n);
        arg("buffers", buffers.toString());
        arg("offset", offset);
        end();
        mgl11.glGenBuffers(n, buffers, offset);
        checkError();
    }

    
    /**
     * Gl gen buffers.
     *
     * @param n the n
     * @param buffers the buffers
     * @see javax.microedition.khronos.opengles.GL11#glGenBuffers(int, java.nio.IntBuffer)
     */
    public void glGenBuffers(int n, IntBuffer buffers) {
        begin("glGenBuffers");
        arg("n", n);
        arg("buffers", buffers.toString());
        end();
        mgl11.glGenBuffers(n, buffers);
        checkError();
    }

    
    /**
     * Gl get booleanv.
     *
     * @param pname the pname
     * @param params the params
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL11#glGetBooleanv(int, boolean[], int)
     */
    public void glGetBooleanv(int pname, boolean[] params, int offset) {
        begin("glGetBooleanv");
        arg("pname", pname);
        arg("params", params.toString());
        arg("offset", offset);
        end();
        mgl11.glGetBooleanv(pname, params, offset);
        checkError();
    }

    
    /**
     * Gl get booleanv.
     *
     * @param pname the pname
     * @param params the params
     * @see javax.microedition.khronos.opengles.GL11#glGetBooleanv(int, java.nio.IntBuffer)
     */
    public void glGetBooleanv(int pname, IntBuffer params) {
        begin("glGetBooleanv");
        arg("pname", pname);
        arg("params", params.toString());
        end();
        mgl11.glGetBooleanv(pname, params);
        checkError();
    }

    
    /**
     * Gl get buffer parameteriv.
     *
     * @param target the target
     * @param pname the pname
     * @param params the params
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL11#glGetBufferParameteriv(int, int, int[], int)
     */
    public void glGetBufferParameteriv(int target, int pname, int[] params,
            int offset) {
        begin("glGetBufferParameteriv");
        arg("target", target);
        arg("pname", pname);
        arg("params", params.toString());
        arg("offset", offset);
        end();
        mgl11.glGetBufferParameteriv(target, pname, params, offset);
        checkError();
    }

    /**
     * Gl get buffer parameteriv.
     *
     * @param target the target
     * @param pname the pname
     * @param params the params
     * @see javax.microedition.khronos.opengles.GL11#glGetBufferParameteriv(int, int, java.nio.IntBuffer)
     */
    public void glGetBufferParameteriv(int target, int pname, IntBuffer params) {
        begin("glGetBufferParameteriv");
        arg("target", target);
        arg("pname", pname);
        arg("params", params.toString());
        end();
        mgl11.glGetBufferParameteriv(target, pname, params);
        checkError();
    }

    /**
     * Gl get clip planef.
     *
     * @param pname the pname
     * @param eqn the eqn
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL11#glGetClipPlanef(int, float[], int)
     */
    public void glGetClipPlanef(int pname, float[] eqn, int offset) {
        begin("glGetClipPlanef");
        arg("pname", pname);
        arg("eqn", eqn.toString());
        arg("offset", offset);
        end();
        mgl11.glGetClipPlanef(pname, eqn, offset);
        checkError();
    }

    /**
     * Gl get clip planef.
     *
     * @param pname the pname
     * @param eqn the eqn
     * @see javax.microedition.khronos.opengles.GL11#glGetClipPlanef(int, java.nio.FloatBuffer)
     */
    public void glGetClipPlanef(int pname, FloatBuffer eqn) {
        begin("glGetClipPlanef");
        arg("pname", pname);
        arg("eqn", eqn.toString());
        end();
        mgl11.glGetClipPlanef(pname, eqn);
        checkError();
    }

    /**
     * Gl get clip planex.
     *
     * @param pname the pname
     * @param eqn the eqn
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL11#glGetClipPlanex(int, int[], int)
     */
    public void glGetClipPlanex(int pname, int[] eqn, int offset) {
        begin("glGetClipPlanex");
        arg("pname", pname);
        arg("eqn", eqn.toString());
        arg("offset", offset);
        end();
        mgl11.glGetClipPlanex(pname, eqn, offset);
    }

    /**
     * Gl get clip planex.
     *
     * @param pname the pname
     * @param eqn the eqn
     * @see javax.microedition.khronos.opengles.GL11#glGetClipPlanex(int, java.nio.IntBuffer)
     */
    public void glGetClipPlanex(int pname, IntBuffer eqn) {
        begin("glGetClipPlanex");
        arg("pname", pname);
        arg("eqn", eqn.toString());
        end();
        mgl11.glGetClipPlanex(pname, eqn);
        checkError();
    }

    /**
     * Gl get fixedv.
     *
     * @param pname the pname
     * @param params the params
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL11#glGetFixedv(int, int[], int)
     */
    public void glGetFixedv(int pname, int[] params, int offset) {
        begin("glGetFixedv");
        arg("pname", pname);
        arg("params", params.toString());
        arg("offset", offset);
        end();
        mgl11.glGetFixedv(pname, params, offset);
    }

    /**
     * Gl get fixedv.
     *
     * @param pname the pname
     * @param params the params
     * @see javax.microedition.khronos.opengles.GL11#glGetFixedv(int, java.nio.IntBuffer)
     */
    public void glGetFixedv(int pname, IntBuffer params) {
        begin("glGetFixedv");
        arg("pname", pname);
        arg("params", params.toString());
        end();
        mgl11.glGetFixedv(pname, params);
        checkError();
    }

    /**
     * Gl get floatv.
     *
     * @param pname the pname
     * @param params the params
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL11#glGetFloatv(int, float[], int)
     */
    public void glGetFloatv(int pname, float[] params, int offset) {
        begin("glGetFloatv");
        arg("pname", pname);
        arg("params", params.toString());
        arg("offset", offset);
        end();
        mgl11.glGetFloatv(pname, params, offset);
    }

    /**
     * Gl get floatv.
     *
     * @param pname the pname
     * @param params the params
     * @see javax.microedition.khronos.opengles.GL11#glGetFloatv(int, java.nio.FloatBuffer)
     */
    public void glGetFloatv(int pname, FloatBuffer params) {
        begin("glGetFloatv");
        arg("pname", pname);
        arg("params", params.toString());
        end();
        mgl11.glGetFloatv(pname, params);
        checkError();
    }

    /**
     * Gl get lightfv.
     *
     * @param light the light
     * @param pname the pname
     * @param params the params
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL11#glGetLightfv(int, int, float[], int)
     */
    public void glGetLightfv(int light, int pname, float[] params, int offset) {
        begin("glGetLightfv");
        arg("light", light);
        arg("pname", pname);
        arg("params", params.toString());
        arg("offset", offset);
        end();
        mgl11.glGetLightfv(light, pname, params, offset);
        checkError();
    }

    /**
     * Gl get lightfv.
     *
     * @param light the light
     * @param pname the pname
     * @param params the params
     * @see javax.microedition.khronos.opengles.GL11#glGetLightfv(int, int, java.nio.FloatBuffer)
     */
    public void glGetLightfv(int light, int pname, FloatBuffer params) {
        begin("glGetLightfv");
        arg("light", light);
        arg("pname", pname);
        arg("params", params.toString());
        end();
        mgl11.glGetLightfv(light, pname, params);
        checkError();
    }

    /**
     * Gl get lightxv.
     *
     * @param light the light
     * @param pname the pname
     * @param params the params
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL11#glGetLightxv(int, int, int[], int)
     */
    public void glGetLightxv(int light, int pname, int[] params, int offset) {
        begin("glGetLightxv");
        arg("light", light);
        arg("pname", pname);
        arg("params", params.toString());
        arg("offset", offset);
        end();
        mgl11.glGetLightxv(light, pname, params, offset);
        checkError();
    }

    /**
     * Gl get lightxv.
     *
     * @param light the light
     * @param pname the pname
     * @param params the params
     * @see javax.microedition.khronos.opengles.GL11#glGetLightxv(int, int, java.nio.IntBuffer)
     */
    public void glGetLightxv(int light, int pname, IntBuffer params) {
        begin("glGetLightxv");
        arg("light", light);
        arg("pname", pname);
        arg("params", params.toString());
        end();
        mgl11.glGetLightxv(light, pname, params);
        checkError();
    }

    /**
     * Gl get materialfv.
     *
     * @param face the face
     * @param pname the pname
     * @param params the params
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL11#glGetMaterialfv(int, int, float[], int)
     */
    public void glGetMaterialfv(int face, int pname, float[] params,
            int offset) {
        begin("glGetMaterialfv");
        arg("face", face);
        arg("pname", pname);
        arg("params", params.toString());
        arg("offset", offset);
        end();
        mgl11.glGetMaterialfv(face, pname, params, offset);
        checkError();
    }

    /**
     * Gl get materialfv.
     *
     * @param face the face
     * @param pname the pname
     * @param params the params
     * @see javax.microedition.khronos.opengles.GL11#glGetMaterialfv(int, int, java.nio.FloatBuffer)
     */
    public void glGetMaterialfv(int face, int pname, FloatBuffer params) {
        begin("glGetMaterialfv");
        arg("face", face);
        arg("pname", pname);
        arg("params", params.toString());
        end();
        mgl11.glGetMaterialfv(face, pname, params);
        checkError();
    }

    /**
     * Gl get materialxv.
     *
     * @param face the face
     * @param pname the pname
     * @param params the params
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL11#glGetMaterialxv(int, int, int[], int)
     */
    public void glGetMaterialxv(int face, int pname, int[] params, int offset) {
        begin("glGetMaterialxv");
        arg("face", face);
        arg("pname", pname);
        arg("params", params.toString());
        arg("offset", offset);
        end();
        mgl11.glGetMaterialxv(face, pname, params, offset);
        checkError();
    }

    /**
     * Gl get materialxv.
     *
     * @param face the face
     * @param pname the pname
     * @param params the params
     * @see javax.microedition.khronos.opengles.GL11#glGetMaterialxv(int, int, java.nio.IntBuffer)
     */
    public void glGetMaterialxv(int face, int pname, IntBuffer params) {
        begin("glGetMaterialxv");
        arg("face", face);
        arg("pname", pname);
        arg("params", params.toString());
        end();
        mgl11.glGetMaterialxv(face, pname, params);
        checkError();
    }

    /**
     * Gl get tex enviv.
     *
     * @param env the env
     * @param pname the pname
     * @param params the params
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL11#glGetTexEnviv(int, int, int[], int)
     */
    public void glGetTexEnviv(int env, int pname, int[] params, int offset) {
        begin("glGetTexEnviv");
        arg("env", env);
        arg("pname", pname);
        arg("params", params.toString());
        arg("offset", offset);
        end();
        mgl11.glGetTexEnviv(env, pname, params, offset);
        checkError();
    }

    /**
     * Gl get tex enviv.
     *
     * @param env the env
     * @param pname the pname
     * @param params the params
     * @see javax.microedition.khronos.opengles.GL11#glGetTexEnviv(int, int, java.nio.IntBuffer)
     */
    public void glGetTexEnviv(int env, int pname, IntBuffer params) {
        begin("glGetTexEnviv");
        arg("env", env);
        arg("pname", pname);
        arg("params", params.toString());
        end();
        mgl11.glGetTexEnviv(env, pname, params);
        checkError();
    }

    /**
     * Gl get tex envxv.
     *
     * @param env the env
     * @param pname the pname
     * @param params the params
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL11#glGetTexEnvxv(int, int, int[], int)
     */
    public void glGetTexEnvxv(int env, int pname, int[] params, int offset) {
        begin("glGetTexEnviv");
        arg("env", env);
        arg("pname", pname);
        arg("params", params.toString());
        arg("offset", offset);
        end();
        mgl11.glGetTexEnviv(env, pname, params, offset);
        checkError();
    }

    /**
     * Gl get tex envxv.
     *
     * @param env the env
     * @param pname the pname
     * @param params the params
     * @see javax.microedition.khronos.opengles.GL11#glGetTexEnvxv(int, int, java.nio.IntBuffer)
     */
    public void glGetTexEnvxv(int env, int pname, IntBuffer params) {
        begin("glGetTexEnviv");
        arg("env", env);
        arg("pname", pname);
        arg("params", params.toString());
        end();
        mgl11.glGetTexEnvxv(env, pname, params);
        checkError();
    }

    /**
     * Gl get tex parameterfv.
     *
     * @param target the target
     * @param pname the pname
     * @param params the params
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL11#glGetTexParameterfv(int, int, float[], int)
     */
    public void glGetTexParameterfv(int target, int pname, float[] params, int offset) {
        begin("glGetTexParameterfv");
        arg("target", target);
        arg("pname", pname);
        arg("params", params.toString());
        arg("offset", offset);
        end();
        mgl11.glGetTexParameterfv(target, pname, params, offset);
        checkError();
    }

    /**
     * Gl get tex parameterfv.
     *
     * @param target the target
     * @param pname the pname
     * @param params the params
     * @see javax.microedition.khronos.opengles.GL11#glGetTexParameterfv(int, int, java.nio.FloatBuffer)
     */
    public void glGetTexParameterfv(int target, int pname, FloatBuffer params) {
        begin("glGetTexParameterfv");
        arg("target", target);
        arg("pname", pname);
        arg("params", params.toString());
        end();
        mgl11.glGetTexParameterfv(target, pname, params);
        checkError();
    }

    /**
     * Gl get tex parameteriv.
     *
     * @param target the target
     * @param pname the pname
     * @param params the params
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL11#glGetTexParameteriv(int, int, int[], int)
     */
    public void glGetTexParameteriv(int target, int pname, int[] params, int offset) {
        begin("glGetTexParameteriv");
        arg("target", target);
        arg("pname", pname);
        arg("params", params.toString());
        arg("offset", offset);
        end();
        mgl11.glGetTexEnviv(target, pname, params, offset);
        checkError();
    }

    /**
     * Gl get tex parameteriv.
     *
     * @param target the target
     * @param pname the pname
     * @param params the params
     * @see javax.microedition.khronos.opengles.GL11#glGetTexParameteriv(int, int, java.nio.IntBuffer)
     */
    public void glGetTexParameteriv(int target, int pname, IntBuffer params) {
        begin("glGetTexParameteriv");
        arg("target", target);
        arg("pname", pname);
        arg("params", params.toString());
        end();
        mgl11.glGetTexParameteriv(target, pname, params);
        checkError();
    }

    /**
     * Gl get tex parameterxv.
     *
     * @param target the target
     * @param pname the pname
     * @param params the params
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL11#glGetTexParameterxv(int, int, int[], int)
     */
    public void glGetTexParameterxv(int target, int pname, int[] params,
            int offset) {
        begin("glGetTexParameterxv");
        arg("target", target);
        arg("pname", pname);
        arg("params", params.toString());
        arg("offset", offset);
        end();
        mgl11.glGetTexParameterxv(target, pname, params, offset);
        checkError();
    }

    /**
     * Gl get tex parameterxv.
     *
     * @param target the target
     * @param pname the pname
     * @param params the params
     * @see javax.microedition.khronos.opengles.GL11#glGetTexParameterxv(int, int, java.nio.IntBuffer)
     */
    public void glGetTexParameterxv(int target, int pname, IntBuffer params) {
        begin("glGetTexParameterxv");
        arg("target", target);
        arg("pname", pname);
        arg("params", params.toString());
        end();
        mgl11.glGetTexParameterxv(target, pname, params);
        checkError();
    }

    /**
     * Gl is buffer.
     *
     * @param buffer the buffer
     * @return true, if gl is buffer
     * @see javax.microedition.khronos.opengles.GL11#glIsBuffer(int)
     */
    public boolean glIsBuffer(int buffer) {
        begin("glIsBuffer");
        arg("buffer", buffer);
        end();
        boolean result = mgl11.glIsBuffer(buffer);
        checkError();
        return result;
    }

    /**
     * Gl is enabled.
     *
     * @param cap the cap
     * @return true, if gl is enabled
     * @see javax.microedition.khronos.opengles.GL11#glIsEnabled(int)
     */
    public boolean glIsEnabled(int cap) {
        begin("glIsEnabled");
        arg("cap", cap);
        end();
        boolean result = mgl11.glIsEnabled(cap);
        checkError();
        return result;
    }

    /**
     * Gl is texture.
     *
     * @param texture the texture
     * @return true, if gl is texture
     * @see javax.microedition.khronos.opengles.GL11#glIsTexture(int)
     */
    public boolean glIsTexture(int texture) {
        begin("glIsTexture");
        arg("texture", texture);
        end();
        boolean result = mgl11.glIsTexture(texture);
        checkError();
        return result;
    }

    /**
     * Gl point parameterf.
     *
     * @param pname the pname
     * @param param the param
     * @see javax.microedition.khronos.opengles.GL11#glPointParameterf(int, float)
     */
    public void glPointParameterf(int pname, float param) {
        begin("glPointParameterf");
        arg("pname", pname);
        arg("param", param);
        end();
        mgl11.glPointParameterf( pname, param);
        checkError();
    }

    /**
     * Gl point parameterfv.
     *
     * @param pname the pname
     * @param params the params
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL11#glPointParameterfv(int, float[], int)
     */
    public void glPointParameterfv(int pname, float[] params, int offset) {
        begin("glPointParameterfv");
        arg("pname", pname);
        arg("params", params.toString());
        arg("offset", offset);
        end();
        mgl11.glPointParameterfv(pname, params, offset);
        checkError();
    }

    /**
     * Gl point parameterfv.
     *
     * @param pname the pname
     * @param params the params
     * @see javax.microedition.khronos.opengles.GL11#glPointParameterfv(int, java.nio.FloatBuffer)
     */
    public void glPointParameterfv(int pname, FloatBuffer params) {
        begin("glPointParameterfv");
        arg("pname", pname);
        arg("params", params.toString());
        end();
        mgl11.glPointParameterfv(pname, params);
        checkError();
    }

    /**
     * Gl point parameterx.
     *
     * @param pname the pname
     * @param param the param
     * @see javax.microedition.khronos.opengles.GL11#glPointParameterx(int, int)
     */
    public void glPointParameterx(int pname, int param) {
        begin("glPointParameterfv");
        arg("pname", pname);
        arg("param", param);
        end();
        mgl11.glPointParameterx( pname, param);
        checkError();
    }

    /**
     * Gl point parameterxv.
     *
     * @param pname the pname
     * @param params the params
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL11#glPointParameterxv(int, int[], int)
     */
    public void glPointParameterxv(int pname, int[] params, int offset) {
        begin("glPointParameterxv");
        arg("pname", pname);
        arg("params", params.toString());
        arg("offset", offset);
        end();
        mgl11.glPointParameterxv(pname, params, offset);
        checkError();
    }

    /**
     * Gl point parameterxv.
     *
     * @param pname the pname
     * @param params the params
     * @see javax.microedition.khronos.opengles.GL11#glPointParameterxv(int, java.nio.IntBuffer)
     */
    public void glPointParameterxv(int pname, IntBuffer params) {
        begin("glPointParameterxv");
        arg("pname", pname);
        arg("params", params.toString());
        end();
        mgl11.glPointParameterxv( pname, params);
        checkError();
    }

    /**
     * Gl point size pointer oes.
     *
     * @param type the type
     * @param stride the stride
     * @param pointer the pointer
     * @see javax.microedition.khronos.opengles.GL11#glPointSizePointerOES(int, int, java.nio.Buffer)
     */
    public void glPointSizePointerOES(int type, int stride, Buffer pointer) {
        begin("glPointSizePointerOES");
        arg("type", type);
        arg("stride", stride);
        arg("params", pointer.toString());
        end();
        mgl11.glPointSizePointerOES( type, stride, pointer);
        checkError();
    }

    
    /**
     * Gl tex envi.
     *
     * @param target the target
     * @param pname the pname
     * @param param the param
     * @see javax.microedition.khronos.opengles.GL11#glTexEnvi(int, int, int)
     */
    public void glTexEnvi(int target, int pname, int param) {
        begin("glTexEnvi");
        arg("target", target);
        arg("pname", pname);
        arg("param", param);
        end();
        mgl11.glTexEnvi(target, pname, param);
        checkError();
    }

    
    /**
     * Gl tex enviv.
     *
     * @param target the target
     * @param pname the pname
     * @param params the params
     * @param offset the offset
     * @see javax.microediton.khronos.opengles.GL11#glTexEnviv(int, int, int[], int)
     */
    public void glTexEnviv(int target, int pname, int[] params, int offset) {
        begin("glTexEnviv");
        arg("target", target);
        arg("pname", pname);
        arg("params", params.toString());
        arg("offset", offset);
        end();
        mgl11.glTexEnviv(target, pname, params, offset);
        checkError();
    }

    
    /**
     * Gl tex enviv.
     *
     * @param target the target
     * @param pname the pname
     * @param params the params
     * @see javax.microediton.khronos.opengles.GL11#glTexEnviv(int, int, java.nio.IntBuffer)
     */
    public void glTexEnviv(int target, int pname, IntBuffer params) {
        begin("glTexEnviv");
        arg("target", target);
        arg("pname", pname);
        arg("params", params.toString());
        end();
        mgl11.glTexEnviv( target, pname, params);
        checkError();
    }

    
    /**
     * Gl tex parameterfv.
     *
     * @param target the target
     * @param pname the pname
     * @param params the params
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL11#glTexParameterfv(int, int, float[], int)
     */
    public void glTexParameterfv(int target, int pname, float[] params,
            int offset) {
        begin("glTexParameterfv");
        arg("target", target);
        arg("pname", pname);
        arg("params", params.toString());
        arg("offset", offset);
        end();
        mgl11.glTexParameterfv( target, pname, params, offset);
        checkError();
    }

    
    /**
     * Gl tex parameterfv.
     *
     * @param target the target
     * @param pname the pname
     * @param params the params
     * @see javax.microedition.khronos.opengles.GL11#glTexParameterfv(int, int, java.nio.FloatBuffer)
     */
    public void glTexParameterfv(int target, int pname, FloatBuffer params) {
        begin("glTexParameterfv");
        arg("target", target);
        arg("pname", pname);
        arg("params", params.toString());
        end();
        mgl11.glTexParameterfv(target, pname, params);
        checkError();
    }

    
    /**
     * Gl tex parameteri.
     *
     * @param target the target
     * @param pname the pname
     * @param param the param
     * @see javax.microedition.khronos.opengles.GL11#glTexParameteri(int, int, int)
     */
    public void glTexParameteri(int target, int pname, int param) {
        begin("glTexParameterxv");
        arg("target", target);
        arg("pname", pname);
        arg("param", param);
        end();
        mgl11.glTexParameteri(target, pname, param);
        checkError();
    }

    
    /**
     * Gl tex parameterxv.
     *
     * @param target the target
     * @param pname the pname
     * @param params the params
     * @param offset the offset
     * @see  javax.microedition.khronos.opengles.GL11#glTexParameterxv(int, int, int[], int)
     */
    public void glTexParameterxv(int target, int pname, int[] params,
            int offset) {
        begin("glTexParameterxv");
        arg("target", target);
        arg("pname", pname);
        arg("params", params.toString());
        arg("offset", offset);
        end();
        mgl11.glTexParameterxv(target, pname, params, offset);
        checkError();
    }

    
    /**
     * Gl tex parameterxv.
     *
     * @param target the target
     * @param pname the pname
     * @param params the params
     * @see javax.microedition.khronos.opengles.GL11#glTexParameterxv(int, int, java.nio.IntBuffer)
     */
    public void glTexParameterxv(int target, int pname, IntBuffer params) {
        begin("glTexParameterxv");
        arg("target", target);
        arg("pname", pname);
        arg("params", params.toString());
        end();
        mgl11.glTexParameterxv(target, pname, params);
        checkError();
    }


    
    /**
     * Gl color pointer.
     *
     * @param size the size
     * @param type the type
     * @param stride the stride
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL11#glColorPointer(int, int, int, int)
     */
    public void glColorPointer(int size, int type, int stride, int offset) {
        begin("glColorPointer");
        arg("size", size);
        arg("type", type);
        arg("stride", stride);
        arg("offset", offset);
        end();
        mgl11.glColorPointer(size, type, stride, offset);
        checkError();
    }

    
    /**
     * Gl draw elements.
     *
     * @param mode the mode
     * @param count the count
     * @param type the type
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL11#glDrawElements(int, int, int, int)
     */
    public void glDrawElements(int mode, int count, int type, int offset) {
        begin("glDrawElements");
        arg("mode", mode);
        arg("count", count);
        arg("type", type);
        arg("offset", offset);
        end();
        mgl11.glDrawElements(mode, count, type, offset);
        checkError();
    }

    
    /**
     * Gl get pointerv.
     *
     * @param pname the pname
     * @param params the params
     * @see javax.microedition.khronos.opengles.GL11#glGetPOinterv(int, java.nio.Buffer[])
     */
    public void glGetPointerv(int pname, Buffer[] params) {
        begin("glGetPointerv");
        arg("pname", pname);
        arg("params", params.toString());
        end();
        mgl11.glGetPointerv(pname, params);
        checkError();
    }

    
    /**
     * Gl normal pointer.
     *
     * @param type the type
     * @param stride the stride
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL11#glNormalPointer(int, int, int)
     */
    public void glNormalPointer(int type, int stride, int offset) {
        begin("glNormalPointer");
        arg("type", type);
        arg("stride", stride);
        arg("offset", offset);
        end();
        mgl11.glNormalPointer(type, stride, offset);
    }

   
    /**
     * Gl tex coord pointer.
     *
     * @param size the size
     * @param type the type
     * @param stride the stride
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL11Ext#glTexCoordPointer(int, int, int, int)
     */
    public void glTexCoordPointer(int size, int type, int stride, int offset) {
        begin("glTexCoordPointer");
        arg("size", size);
        arg("type", type);
        arg("stride", stride);
        arg("offset", offset);
        end();
        mgl11.glTexCoordPointer(size, type, stride, offset);
    }

   
    /**
     * Gl vertex pointer.
     *
     * @param size the size
     * @param type the type
     * @param stride the stride
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL11Ext#glVertexPointer(int,int, int, int)
     */
    public void glVertexPointer(int size, int type, int stride, int offset) {
        begin("glVertexPointer");
        arg("size", size);
        arg("type", type);
        arg("stride", stride);
        arg("offset", offset);
        end();
        mgl11.glVertexPointer(size, type, stride, offset);
    }

   
    /**
     * Gl current palette matrix oes.
     *
     * @param matrixpaletteindex the matrixpaletteindex
     * @see javax.microeditioin.khronos.opengles.GL11Ext#glCurrentPaletteMatrixOES(int)
     */
    public void glCurrentPaletteMatrixOES(int matrixpaletteindex) {
        begin("glCurrentPaletteMatrixOES");
        arg("matrixpaletteindex", matrixpaletteindex);
        end();
        mgl11Ext.glCurrentPaletteMatrixOES(matrixpaletteindex);
        checkError();
    }

    
    /**
     * Gl load palette from model view matrix oes.
     *
     * @see javax.microedition.khronos.opengles.GL11Ext#glLoadPaletteFromModelViewMatrixOES()
     */
    public void glLoadPaletteFromModelViewMatrixOES() {
        begin("glLoadPaletteFromModelViewMatrixOES");
        end();
        mgl11Ext.glLoadPaletteFromModelViewMatrixOES();
        checkError();
    }

    
    /**
     * Gl matrix index pointer oes.
     *
     * @param size the size
     * @param type the type
     * @param stride the stride
     * @param pointer the pointer
     * @see javax.microedition.khronos.opengles.GL11Ext#glMatrixIndexPointerOES(int, int, int, java.nio.Buffer)
     */
    public void glMatrixIndexPointerOES(int size, int type, int stride,
            Buffer pointer) {
        begin("glMatrixIndexPointerOES");
        argPointer(size, type, stride, pointer);
        end();
        mgl11Ext.glMatrixIndexPointerOES(size, type, stride, pointer);
        checkError();
    }

   
    /**
     * Gl matrix index pointer oes.
     *
     * @param size the size
     * @param type the type
     * @param stride the stride
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL11Ext#glMatrixindexPointerOES(int, int, int, int)
     */
    public void glMatrixIndexPointerOES(int size, int type, int stride,
            int offset) {
        begin("glMatrixIndexPointerOES");
        arg("size", size);
        arg("type", type);
        arg("stride", stride);
        arg("offset", offset);
        end();
        mgl11Ext.glMatrixIndexPointerOES(size, type, stride, offset);
        checkError();
    }

    
    /**
     * Gl weight pointer oes.
     *
     * @param size the size
     * @param type the type
     * @param stride the stride
     * @param pointer the pointer
     * @see javax.microedition.khronos.opengles.GL11Ext#glWeightPointerOES(int, int, int, java.nio.Buffer)
     */
    public void glWeightPointerOES(int size, int type, int stride,
            Buffer pointer) {
        begin("glWeightPointerOES");
        argPointer(size, type, stride, pointer);
        end();
        mgl11Ext.glWeightPointerOES(size, type, stride, pointer);
        checkError();
    }

    
    /**
     * Gl weight pointer oes.
     *
     * @param size the size
     * @param type the type
     * @param stride the stride
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL11Ext#glWeightPointerOES(int, int, int, int)
     */
    public void glWeightPointerOES(int size, int type, int stride, int offset) {
        begin("glWeightPointerOES");
        arg("size", size);
        arg("type", type);
        arg("stride", stride);
        arg("offset", offset);
        end();
        mgl11Ext.glWeightPointerOES(size, type, stride, offset);
        checkError();
    }

    
    /**
     * Gl bind framebuffer oes.
     *
     * @param target the target
     * @param framebuffer the framebuffer
     * @see javax.microedition.khronos.opengles.GL11ExtensionPack#glBindFramebufferOES(int, int)
     */
    @Override
    public void glBindFramebufferOES(int target, int framebuffer) {
        begin("glBindFramebufferOES");
        arg("target", target);
        arg("framebuffer", framebuffer);
        end();
        mgl11ExtensionPack.glBindFramebufferOES(target, framebuffer);
        checkError();
    }

    
    /**
     * Gl bind renderbuffer oes.
     *
     * @param target the target
     * @param renderbuffer the renderbuffer
     * @see javax.microedition.khronos.opengles.GL11ExtensionPack#glBindRenderbufferOES(int, int)
     */
    @Override
    public void glBindRenderbufferOES(int target, int renderbuffer) {
        begin("glBindRenderbufferOES");
        arg("target", target);
        arg("renderbuffer", renderbuffer);
        end();
        mgl11ExtensionPack.glBindRenderbufferOES(target, renderbuffer);
        checkError();
    }

    
    /**
     * Gl blend equation.
     *
     * @param mode the mode
     * @see javax.microeditoin.khronos.opengles.GL11ExtensionPack#glBlendEquation(int)
     */
    @Override
    public void glBlendEquation(int mode) {
        begin("glBlendEquation");
        arg("mode", mode);
        end();
        mgl11ExtensionPack.glBlendEquation(mode);
        checkError();
    }

    
    /**
     * Gl blend equation separate.
     *
     * @param modeRGB the mode rgb
     * @param modeAlpha the mode alpha
     * @see javax.microediton.khronos.opengles.GL11ExtensionPack#glBelndequationSeparate(int, int)
     */
    @Override
    public void glBlendEquationSeparate(int modeRGB, int modeAlpha) {
        begin("glBlendEquationSeparate");
        arg("modeRGB", modeRGB);
        arg("modeAlpha", modeAlpha);
        end();
        mgl11ExtensionPack.glBlendEquationSeparate(modeRGB, modeAlpha);
        checkError();
    }

    
    /**
     * Gl blend func separate.
     *
     * @param srcRGB the src rgb
     * @param dstRGB the dst rgb
     * @param srcAlpha the src alpha
     * @param dstAlpha the dst alpha
     * @see javax.microedition.khronos.opengles.GL11ExtensionPack#glBlendfuncSeparate(int, int, int,int)
     */
    @Override
    public void glBlendFuncSeparate(int srcRGB, int dstRGB, int srcAlpha,
            int dstAlpha) {
        begin("glBlendFuncSeparate");
        arg("srcRGB", srcRGB);
        arg("dstRGB", dstRGB);
        arg("srcAlpha", srcAlpha);
        arg("dstAlpha", dstAlpha);
        end();
        mgl11ExtensionPack.glBlendFuncSeparate(srcRGB, dstRGB, srcAlpha, dstAlpha);
        checkError();
    }

   
    /**
     * Gl check framebuffer status oes.
     *
     * @param target the target
     * @return the int
     * @see  javax.microedition.khronos.opengles.GL11ExtensionPack#glCheckFramebufferStatusOES(int)
     */
    @Override
    public int glCheckFramebufferStatusOES(int target) {
        begin("glCheckFramebufferStatusOES");
        arg("target", target);
        end();
        int result = mgl11ExtensionPack.glCheckFramebufferStatusOES(target);
        checkError();
        return result;
    }

    
    /**
     * Gl delete framebuffers oes.
     *
     * @param n the n
     * @param framebuffers the framebuffers
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL11ExtensionPack#glDeleteFramebuffeersOES(int, int[], int)
     */
    @Override
    public void glDeleteFramebuffersOES(int n, int[] framebuffers, int offset) {
        begin("glDeleteFramebuffersOES");
        arg("n", n);
        arg("framebuffers", framebuffers.toString());
        arg("offset", offset);
        end();
        mgl11ExtensionPack.glDeleteFramebuffersOES(n, framebuffers, offset);
        checkError();
    }

   
    /**
     * Gl delete framebuffers oes.
     *
     * @param n the n
     * @param framebuffers the framebuffers
     * @see javax.microedition.khronos.opengles.GL11ExtensionPack#glDeleteFramebuffersOES(int, java.nio.IntBuffer)
     */
    @Override
    public void glDeleteFramebuffersOES(int n, IntBuffer framebuffers) {
        begin("glDeleteFramebuffersOES");
        arg("n", n);
        arg("framebuffers", framebuffers.toString());
        end();
        mgl11ExtensionPack.glDeleteFramebuffersOES(n, framebuffers);
        checkError();
    }

    
    /**
     * Gl delete renderbuffers oes.
     *
     * @param n the n
     * @param renderbuffers the renderbuffers
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL11ExtensionPack#glDeleteRenderbuffersOES(int, int[], int)
     */
    @Override
    public void glDeleteRenderbuffersOES(int n, int[] renderbuffers, int offset) {
        begin("glDeleteRenderbuffersOES");
        arg("n", n);
        arg("renderbuffers", renderbuffers.toString());
        arg("offset", offset);
        end();
        mgl11ExtensionPack.glDeleteRenderbuffersOES(n, renderbuffers, offset);
        checkError();
    }

   
    /**
     * Gl delete renderbuffers oes.
     *
     * @param n the n
     * @param renderbuffers the renderbuffers
     * @see javax.microedition.khronos.opengles.GL11ExtensionPack#glDeleteRenderbuffersOES(int, java.nio.IntBuffer)
     */
    @Override
    public void glDeleteRenderbuffersOES(int n, IntBuffer renderbuffers) {
        begin("glDeleteRenderbuffersOES");
        arg("n", n);
        arg("renderbuffers", renderbuffers.toString());
        end();
        mgl11ExtensionPack.glDeleteRenderbuffersOES(n, renderbuffers);
        checkError();
    }

    
    /**
     * Gl framebuffer renderbuffer oes.
     *
     * @param target the target
     * @param attachment the attachment
     * @param renderbuffertarget the renderbuffertarget
     * @param renderbuffer the renderbuffer
     * @see javax.microediton.khronos.opengles.GL11ExtensionPack#glFramebufferRenderbufferOES(int, int, int, int)
     */
    @Override
    public void glFramebufferRenderbufferOES(int target, int attachment,
            int renderbuffertarget, int renderbuffer) {
        begin("glFramebufferRenderbufferOES");
        arg("target", target);
        arg("attachment", attachment);
        arg("renderbuffertarget", renderbuffertarget);
        arg("renderbuffer", renderbuffer);
        end();
        mgl11ExtensionPack.glFramebufferRenderbufferOES(target, attachment, renderbuffertarget, renderbuffer);
        checkError();
    }

    
    /**
     * Gl framebuffer texture2 does.
     *
     * @param target the target
     * @param attachment the attachment
     * @param textarget the textarget
     * @param texture the texture
     * @param level the level
     * @see javax.microedition.khronos.opengles.GL11ExtensionPack#glFramebufferTexture2DOES(int, int, int, int, int)
     */
    @Override
    public void glFramebufferTexture2DOES(int target, int attachment,
            int textarget, int texture, int level) {
        begin("glFramebufferTexture2DOES");
        arg("target", target);
        arg("attachment", attachment);
        arg("textarget", textarget);
        arg("texture", texture);
        arg("level", level);
        end();
        mgl11ExtensionPack.glFramebufferTexture2DOES(target, attachment, textarget, texture, level);
        checkError();
    }

    
    /**
     * Gl generate mipmap oes.
     *
     * @param target the target
     * @see javax.microedition.khronos.opengles.GL11ExtensionPack#glGenerateMipmapOES(int)
     */
    @Override
    public void glGenerateMipmapOES(int target) {
        begin("glGenerateMipmapOES");
        arg("target", target);
        end();
        mgl11ExtensionPack.glGenerateMipmapOES(target);
        checkError();
    }

    
    /**
     * Gl gen framebuffers oes.
     *
     * @param n the n
     * @param framebuffers the framebuffers
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL11ExtensionPack#glGenframebuffersOES(int, int[], int)
     */
    @Override
    public void glGenFramebuffersOES(int n, int[] framebuffers, int offset) {
        begin("glGenFramebuffersOES");
        arg("n", n);
        arg("framebuffers", framebuffers.toString());
        arg("offset", offset);
        end();
        mgl11ExtensionPack.glGenFramebuffersOES(n, framebuffers, offset);
        checkError();
    }

   
    /**
     * Gl gen framebuffers oes.
     *
     * @param n the n
     * @param framebuffers the framebuffers
     * @see javax.microedition.khronos.opengles..GL11ExtensionPack#glGenFramebuffersOES(int, java.nio.IntBuffer)
     */
    @Override
    public void glGenFramebuffersOES(int n, IntBuffer framebuffers) {
        begin("glGenFramebuffersOES");
        arg("n", n);
        arg("framebuffers", framebuffers.toString());
        end();
        mgl11ExtensionPack.glGenFramebuffersOES(n, framebuffers);
        checkError();
    }

    
   /**
    * Gl gen renderbuffers oes.
    *
    * @param n the n
    * @param renderbuffers the renderbuffers
    * @param offset the offset
    * @see javax.microedition.khronos.openles.GL11ExtensionPack#glGenRenderbuffersOES(int, int[], int)
    */
    @Override
    public void glGenRenderbuffersOES(int n, int[] renderbuffers, int offset) {
        begin("glGenRenderbuffersOES");
        arg("n", n);
        arg("renderbuffers", renderbuffers.toString());
        arg("offset", offset);
        end();
        mgl11ExtensionPack.glGenRenderbuffersOES(n, renderbuffers, offset);
        checkError();
    }

   
    /**
     * Gl gen renderbuffers oes.
     *
     * @param n the n
     * @param renderbuffers the renderbuffers
     * @see javax.microedition.khronos.opengles.GL11ExtensionPack#glGenRenderbuffersOES(int, java.nio.IntBuffer)
     */
    @Override
    public void glGenRenderbuffersOES(int n, IntBuffer renderbuffers) {
        begin("glGenRenderbuffersOES");
        arg("n", n);
        arg("renderbuffers", renderbuffers.toString());
        end();
        mgl11ExtensionPack.glGenRenderbuffersOES(n, renderbuffers);
        checkError();
    }

    
    /**
     * Gl get framebuffer attachment parameteriv oes.
     *
     * @param target the target
     * @param attachment the attachment
     * @param pname the pname
     * @param params the params
     * @param offset the offset
     * @see javax.midcroedition.khronos.opengles.GL11ExtensionPack#glGetFramebufferAttachmentParameterifOES(int, int, int, int{}, int)
     */
    @Override
    public void glGetFramebufferAttachmentParameterivOES(int target,
            int attachment, int pname, int[] params, int offset) {
        begin("glGetFramebufferAttachmentParameterivOES");
        arg("target", target);
        arg("attachment", attachment);
        arg("pname", pname);
        arg("params", params.toString());
        arg("offset", offset);
        end();
        mgl11ExtensionPack.glGetFramebufferAttachmentParameterivOES(target, attachment, pname, params, offset);
        checkError();
    }

   
    /**
     * Gl get framebuffer attachment parameteriv oes.
     *
     * @param target the target
     * @param attachment the attachment
     * @param pname the pname
     * @param params the params
     * @see javax.microedition.khronos.opengles.GL11ExtensionPack#glGetFramebufferAttachmentParameterivOES(int,int, int, java.nio.IntBuffer)
     */
    @Override
    public void glGetFramebufferAttachmentParameterivOES(int target,
            int attachment, int pname, IntBuffer params) {
        begin("glGetFramebufferAttachmentParameterivOES");
        arg("target", target);
        arg("attachment", attachment);
        arg("pname", pname);
        arg("params", params.toString());
        end();
        mgl11ExtensionPack.glGetFramebufferAttachmentParameterivOES(target, attachment, pname, params);
        checkError();
    }

    
    /**
     * Gl get renderbuffer parameteriv oes.
     *
     * @param target the target
     * @param pname the pname
     * @param params the params
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL11ExtensionPack#glGetRenderbufferParameterivOES(int, int, int[], int)
     */
    @Override
    public void glGetRenderbufferParameterivOES(int target, int pname,
            int[] params, int offset) {
        begin("glGetRenderbufferParameterivOES");
        arg("target", target);
        arg("pname", pname);
        arg("params", params.toString());
        arg("offset", offset);
        end();
        mgl11ExtensionPack.glGetRenderbufferParameterivOES(target, pname, params, offset);
        checkError();
    }

   
    /**
     * Gl get renderbuffer parameteriv oes.
     *
     * @param target the target
     * @param pname the pname
     * @param params the params
     * @see javax.microedtion.khronos.opengles.GL11ExtensionPack#glGetrenderbufferParameterivOES(int, int, java.nio.IntBuffer)
     */
    @Override
    public void glGetRenderbufferParameterivOES(int target, int pname,
            IntBuffer params) {
        begin("glGetRenderbufferParameterivOES");
        arg("target", target);
        arg("pname", pname);
        arg("params", params.toString());
        end();
        mgl11ExtensionPack.glGetRenderbufferParameterivOES(target, pname, params);
        checkError();
    }

    
    /**
     * Gl get tex genfv.
     *
     * @param coord the coord
     * @param pname the pname
     * @param params the params
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL11ExtensionPack#glGetTexGenfv(int, int, float[], int)
     */
    @Override
    public void glGetTexGenfv(int coord, int pname, float[] params, int offset) {
        begin("glGetTexGenfv");
        arg("coord", coord);
        arg("pname", pname);
        arg("params", params.toString());
        arg("offset", offset);
        end();
        mgl11ExtensionPack.glGetTexGenfv(coord, pname, params, offset);
        checkError();
    }

    
    /**
     * Gl get tex genfv.
     *
     * @param coord the coord
     * @param pname the pname
     * @param params the params
     * @see javax.microedition.khronos.opengles.GL11ExtensionPack#glGetTexGenfv(int, int, java.nio.FloatBuffer)
     */
    @Override
    public void glGetTexGenfv(int coord, int pname, FloatBuffer params) {
        begin("glGetTexGenfv");
        arg("coord", coord);
        arg("pname", pname);
        arg("params", params.toString());
        end();
        mgl11ExtensionPack.glGetTexGenfv(coord, pname, params);
        checkError();
    }

    
    /**
     * Gl get tex geniv.
     *
     * @param coord the coord
     * @param pname the pname
     * @param params the params
     * @param offset the offset
     * @see{@link javax.microedition.khronos.opengles.GL11ExtensionPack#glGetTexGeniv(int, int, int[], int)}
     */
    @Override
    public void glGetTexGeniv(int coord, int pname, int[] params, int offset) {
        begin("glGetTexGeniv");
        arg("coord", coord);
        arg("pname", pname);
        arg("params", params.toString());
        arg("offset", offset);
        end();
        mgl11ExtensionPack.glGetTexGeniv(coord, pname, params, offset);
        checkError();
    }

    
    /**
     * Gl get tex geniv.
     *
     * @param coord the coord
     * @param pname the pname
     * @param params the params
     * @see{@link javax.microedition.khronos.opengles.GL11ExtensionPack#glGetTexGeniv(int, int, java.nio.IntBuffer)}
     */
    @Override
    public void glGetTexGeniv(int coord, int pname, IntBuffer params) {
        begin("glGetTexGeniv");
        arg("coord", coord);
        arg("pname", pname);
        arg("params", params.toString());
        end();
        mgl11ExtensionPack.glGetTexGeniv(coord, pname, params);
        checkError();
    }

   
    /**
     * Gl get tex genxv.
     *
     * @param coord the coord
     * @param pname the pname
     * @param params the params
     * @param offset the offset
     * @see{@link javax.microedition.khronos.opengles.GL11ExtensionPack#glGetTexGenxv(int, int, int[], int)}
     */
    @Override
    public void glGetTexGenxv(int coord, int pname, int[] params, int offset) {
        begin("glGetTexGenxv");
        arg("coord", coord);
        arg("pname", pname);
        arg("params", params.toString());
        arg("offset", offset);
        end();
        mgl11ExtensionPack.glGetTexGenxv(coord, pname, params, offset);
        checkError();
    }

    
    /**
     * Gl get tex genxv.
     *
     * @param coord the coord
     * @param pname the pname
     * @param params the params
     * @see{@link javax.microedition.khronos.opengles.GL11ExtensionPack#glGetTexGenxv(int, int, java.nio, IntBuffer)}
     */
    @Override
    public void glGetTexGenxv(int coord, int pname, IntBuffer params) {
        begin("glGetTexGenxv");
        arg("coord", coord);
        arg("pname", pname);
        arg("params", params.toString());
        end();
        mgl11ExtensionPack.glGetTexGenxv(coord, pname, params);
        checkError();
    }

    
    /**
     * Gl is framebuffer oes.
     *
     * @param framebuffer the framebuffer
     * @return true, if gl is framebuffer oes
     * @see{@link javax.microedition.khronos.opengles.GL11ExtensionPack#gllsFramebufferOES(int)}
     */
    @Override
    public boolean glIsFramebufferOES(int framebuffer) {
        begin("glIsFramebufferOES");
        arg("framebuffer", framebuffer);
        end();
        boolean result = mgl11ExtensionPack.glIsFramebufferOES(framebuffer);
        checkError();
        return result;
    }

   
    /**
     * Gl is renderbuffer oes.
     *
     * @param renderbuffer the renderbuffer
     * @return true, if gl is renderbuffer oes
     * @see{@link javax.microediton.khronos.opengles.GL11ExtensionPack#gllsRenderbufferOES(int)}
     */
    @Override
    public boolean glIsRenderbufferOES(int renderbuffer) {
        begin("glIsRenderbufferOES");
        arg("renderbuffer", renderbuffer);
        end();
        mgl11ExtensionPack.glIsRenderbufferOES(renderbuffer);
        checkError();
        return false;
    }

   
    /**
     * Gl renderbuffer storage oes.
     *
     * @param target the target
     * @param internalformat the internalformat
     * @param width the width
     * @param height the height
     * @see{@link javax.microedition.khronos.opengles.GL11ExtensionPack#glRenderbufferStorageOES(int, int, int, int)}
     */
    @Override
    public void glRenderbufferStorageOES(int target, int internalformat,
            int width, int height) {
        begin("glRenderbufferStorageOES");
        arg("target", target);
        arg("internalformat", internalformat);
        arg("width", width);
        arg("height", height);
        end();
        mgl11ExtensionPack.glRenderbufferStorageOES(target, internalformat, width, height);
        checkError();
    }

   
    /**
     * Gl tex genf.
     *
     * @param coord the coord
     * @param pname the pname
     * @param param the param
     * @see{@link javax.microediton.khronos.opengles.GL11ExtensionPack#glTexGenf(int, int, float)}
     */
    @Override
    public void glTexGenf(int coord, int pname, float param) {
        begin("glTexGenf");
        arg("coord", coord);
        arg("pname", pname);
        arg("param", param);
        end();
        mgl11ExtensionPack.glTexGenf(coord, pname, param);
        checkError();
    }

    
    /**
     * Gl tex genfv.
     *
     * @param coord the coord
     * @param pname the pname
     * @param params the params
     * @param offset the offset
     * @see{@link javax.microedition.khronos.opengles.GL11ExtensionPack#glGenfv(int, int, float[], int)}
     */
    @Override
    public void glTexGenfv(int coord, int pname, float[] params, int offset) {
        begin("glTexGenfv");
        arg("coord", coord);
        arg("pname", pname);
        arg("params", params.toString());
        arg("offset", offset);
        end();
        mgl11ExtensionPack.glTexGenfv(coord, pname, params, offset);
        checkError();
    }

    
    /**
     * Gl tex genfv.
     *
     * @param coord the coord
     * @param pname the pname
     * @param params the params
     * @see{@link  javax.microedition.khronos.opengles.GL11ExtensionPack#glTexGenfv(int, int, java.nio.FloatBuffer)}
     */
    @Override
    public void glTexGenfv(int coord, int pname, FloatBuffer params) {
        begin("glTexGenfv");
        arg("coord", coord);
        arg("pname", pname);
        arg("params", params.toString());
        end();
        mgl11ExtensionPack.glTexGenfv(coord, pname, params);
        checkError();
    }

   
    /**
     * Gl tex geni.
     *
     * @param coord the coord
     * @param pname the pname
     * @param param the param
     * @see{@link javax.microediton.khronos.opengles.GL11ExtensionPack#glTexGeni(int, int, int)}
     */
    @Override
    public void glTexGeni(int coord, int pname, int param) {
        begin("glTexGeni");
        arg("coord", coord);
        arg("pname", pname);
        arg("param", param);
        end();
        mgl11ExtensionPack.glTexGeni(coord, pname, param);
        checkError();
    }

    
    /**
     * Gl tex geniv.
     *
     * @param coord the coord
     * @param pname the pname
     * @param params the params
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL11ExtensionPack#glTexGeniv)int, int, int[], int)
     */
    @Override
    public void glTexGeniv(int coord, int pname, int[] params, int offset) {
        begin("glTexGeniv");
        arg("coord", coord);
        arg("pname", pname);
        arg("params", params.toString());
        arg("offset", offset);
        end();
        mgl11ExtensionPack.glTexGeniv(coord, pname, params, offset);
        checkError();
    }

  
    /**
     * Gl tex geniv.
     *
     * @param coord the coord
     * @param pname the pname
     * @param params the params
     * @see  javax.microedition.khronos.opengles.GL11ExtensionPack#glTexGeniv(int, int, java.nio.IntBuffer)
     */
    @Override
    public void glTexGeniv(int coord, int pname, IntBuffer params) {
        begin("glTexGeniv");
        arg("coord", coord);
        arg("pname", pname);
        arg("params", params.toString());
        end();
        mgl11ExtensionPack.glTexGeniv(coord, pname, params);
        checkError();
    }

    
    /**
     * Gl tex genx.
     *
     * @param coord the coord
     * @param pname the pname
     * @param param the param
     * @seee javax.microedition.khronos.opengles.GL11ExtensionPack#glTexGenx(int, int, int)
     */
    @Override
    public void glTexGenx(int coord, int pname, int param) {
        begin("glTexGenx");
        arg("coord", coord);
        arg("pname", pname);
        arg("param", param);
        end();
        mgl11ExtensionPack.glTexGenx(coord, pname, param);
        checkError();
    }

   
    /**
     * Gl tex genxv.
     *
     * @param coord the coord
     * @param pname the pname
     * @param params the params
     * @param offset the offset
     * @see javax.microedition.khronos.opengles.GL11ExtensionPack#glTexGenxv(int, int, int[], int)
     */
    @Override
    public void glTexGenxv(int coord, int pname, int[] params, int offset) {
        begin("glTexGenxv");
        arg("coord", coord);
        arg("pname", pname);
        arg("params", params.toString());
        arg("offset", offset);
        end();
        mgl11ExtensionPack.glTexGenxv(coord, pname, params, offset);
        checkError();
    }

    
    /**
     * Gl tex genxv.
     *
     * @param coord the coord
     * @param pname the pname
     * @param params the params
     * @see javax.microedition.khronos.opengles.GL11ExtensionPack#glTexGenxv(int, int, java.nio.IntBuffer)
     */
    @Override
    public void glTexGenxv(int coord, int pname, IntBuffer params) {
        begin("glTexGenxv");
        arg("coord", coord);
        arg("pname", pname);
        arg("params", params.toString());
        end();
        mgl11ExtensionPack.glTexGenxv(coord, pname, params);
        checkError();
    }

    /**
     * The Class PointerInfo.
     */
    private class PointerInfo {
        /**
         * The number of coordinates per vertex. 1..4
         */
        public int mSize;
        /**
         * The type of each coordinate.
         */
        public int mType;
        /**
         * The byte offset between consecutive vertices. 0 means mSize *
         * sizeof(mType)
         */
        public int mStride;
        
        /** The m pointer. */
        public Buffer mPointer;
        
        /** The m temp byte buffer. */
        public ByteBuffer mTempByteBuffer; // Only valid during glDrawXXX calls

        /**
         * Instantiates a new pointer info.
         */
        public PointerInfo() {
        }

        /**
         * Instantiates a new pointer info.
         *
         * @param size the size
         * @param type the type
         * @param stride the stride
         * @param pointer the pointer
         */
        public PointerInfo(int size, int type, int stride, Buffer pointer) {
            mSize = size;
            mType = type;
            mStride = stride;
            mPointer = pointer;
        }

        /**
         * Sizeof.
         *
         * @param type the type
         * @return the int
         */
        public int sizeof(int type) {
            switch (type) {
            case GL_UNSIGNED_BYTE:
                return 1;
            case GL_BYTE:
                return 1;
            case GL_SHORT:
                return 2;
            case GL_FIXED:
                return 4;
            case GL_FLOAT:
                return 4;
            default:
                return 0;
            }
        }

        /**
         * Gets the stride.
         *
         * @return the stride
         */
        public int getStride() {
            return mStride > 0 ? mStride : sizeof(mType) * mSize;
        }

        /**
         * Bind byte buffer.
         */
        public void bindByteBuffer() {
            mTempByteBuffer = mPointer == null ? null : toByteBuffer(-1, mPointer);
        }

        /**
         * Unbind byte buffer.
         */
        public void unbindByteBuffer() {
            mTempByteBuffer = null;
        }
    }

    /** The m log. */
    private Writer mLog;
    
    /** The m log argument names. */
    private boolean mLogArgumentNames;
    
    /** The m arg count. */
    private int mArgCount;

    /** The m color pointer. */
    private PointerInfo mColorPointer = new PointerInfo();
    
    /** The m normal pointer. */
    private PointerInfo mNormalPointer = new PointerInfo();
    
    /** The m tex coord pointer. */
    private PointerInfo mTexCoordPointer = new PointerInfo();
    
    /** The m vertex pointer. */
    private PointerInfo mVertexPointer = new PointerInfo();

    /** The m color array enabled. */
    boolean mColorArrayEnabled;
    
    /** The m normal array enabled. */
    boolean mNormalArrayEnabled;
    
    /** The m texture coord array enabled. */
    boolean mTextureCoordArrayEnabled;
    
    /** The m vertex array enabled. */
    boolean mVertexArrayEnabled;

    /** The m string builder. */
    StringBuilder mStringBuilder;
}

