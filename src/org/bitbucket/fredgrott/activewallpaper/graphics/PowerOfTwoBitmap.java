/*******************************************************************************
 * Copyright 2012 fredgrott
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package org.bitbucket.fredgrott.activewallpaper.graphics;

import org.bitbucket.fredgrott.activewallpaper.log.LogUtility;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;


/**
 * The Class PowerOfTwoBitmap.
 */
public class PowerOfTwoBitmap {
	
	/** The Constant TAG. */
	@SuppressWarnings("unused")
	private static final String TAG = LogUtility.setLOGTAG("PowerOfTwoBitmap");
	
	/**
	 * Creates the power of two bitmap.
	 *
	 * @param bitmap the bitmap
	 * @return the bitmap
	 */
	public static Bitmap createPowerOfTwoBitmap(Bitmap bitmap) {
		int w = bitmap.getWidth();
		int h = bitmap.getHeight();
		int w2 = getNextPowerOfTwo(w);
		int h2 = getNextPowerOfTwo(h);
		if(w == w2 && h == h2) {
			// What luck, it's already compatible.
			return bitmap;
		} else {
			Bitmap biggerBitmap = Bitmap.createBitmap(w2, h2, Config.ARGB_8888);
			Canvas canvas = new Canvas(biggerBitmap);
			canvas.drawBitmap(bitmap, 0, 0, null);
			return biggerBitmap;
		}
	}
	
	/**
	 * Returns the next power of two that is greater than or
	 * equal to the given value. If the given number is already
	 * a power of two, that number is returned.
	 *
	 * @param value the value
	 * @return the next power of two
	 */
	public static int getNextPowerOfTwo(int value) {
		int result = 1;
		value--;
		while (value > 0) {
			result = result << 1;
			value = value >> 1;
		}
		return result;
	}

}
