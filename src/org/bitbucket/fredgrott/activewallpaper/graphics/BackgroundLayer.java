/*******************************************************************************
 * Copyright 2012 fredgrott
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package org.bitbucket.fredgrott.activewallpaper.graphics;

import org.bitbucket.fredgrott.activewallpaper.log.LogUtility;

import android.graphics.Bitmap;
import android.graphics.Canvas;


/**
 * The Class BackgroundLayer.
 */
public class BackgroundLayer implements Drawable {

/** The TAG. */
public static String TAG = LogUtility.setLOGTAG("BackgroundLayer");
	
	/** The image. */
	private Bitmap image;
	
	/** The speed. */
	private float speed;
	
	/** The x. */
	private float x;
	
	/** The y. */
	private float y;
	
	/**
	 * The Constructor.
	 *
	 * @param image the image
	 * @param speed the speed
	 * @param y the y
	 */
	public BackgroundLayer(Bitmap image, float speed, float y) {
		this.image = image;
		this.speed = speed;
		this.y = y;
		this.x = 0;
	}

	
	/**
	 * @see org.bitbucket.fredgrott.activewallpaper.graphics.Drawable#onDraw(android.graphics.Canvas)
	 */
	@Override
	public void onDraw(Canvas canvas) {
		float pos = x;
		
		while(pos < canvas.getWidth()) {
			canvas.drawBitmap(image, pos, y, null);
			pos += image.getWidth();
		}
				
	}

	/**
	 * Scroll.
	 */
	public void scroll() {
		x = (x - speed) % image.getWidth(); 
	}

}
