/*******************************************************************************
 * Copyright 2012 fredgrott
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package org.bitbucket.fredgrott.activewallpaper.graphics;

import org.bitbucket.fredgrott.activewallpaper.log.LogUtility;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;


/**
 * The Class Sprite.
 */
public class Sprite extends Particle implements Drawable {
	
	/** The TAG. */
	public static String TAG = LogUtility.setLOGTAG("Sprite");
	
	
    /** The width. */
    protected int width;
    
    /** The height. */
    protected int height;
    
    /** The image. */
    protected Bitmap image;
    
    /**
     * The Constructor.
     *
     * @param x the x
     * @param y the y
     * @param image the image
     */
    public Sprite(int x, int y, Bitmap image) {
        super();       
        
        this.x = x;
        this.y = y;
        this.image = image;
        this.width = image.getWidth();
        this.height = image.getHeight();              
    }
    
    /**
     * Gets the width.
     *
     * @return the width
     */
    public int getWidth() {
        return width;
    }
    
    /**
     * Sets the width.
     *
     * @param width the width to set
     */
    public void setWidth(int width) {
        this.width = width;
    }
    
    /**
     * Gets the height.
     *
     * @return the height
     */
    public int getHeight() {
        return height;
    }
    
    /**
     * Sets the height.
     *
     * @param height the height to set
     */
    public void setHeight(int height) {
        this.height = height;
    }
    
    /**
     * Return the left most point of the sprite.
     *
     * @return the left
     */
    public int getLeft() {
        return (int) x;
    }
    
    /**
     * Return the top most point.
     *
     * @return the top
     */
    public int getTop() {
        return (int) y;
    }
    
    /**
     * Return the right most point.
     *
     * @return the right
     */
    public int getRight() {
        return (int) x + width;
    }
    
    /**
     * Return the bottom most point.
     *
     * @return the bottom
     */
    public int getBottom() {
        return (int) y + height;
    }
        
    /**
     * Calculate the over crossing areas and scan each pixel for a collision.
     *
     * @param sprite the sprite
     * @return true, if checks if is colliding with
     */
    public boolean isCollidingWith(Sprite sprite) {
        int left = Math.max(getLeft(), sprite.getLeft());
        int right = Math.min(getRight(), sprite.getRight());
        int top = Math.max(getTop(), sprite.getTop());
        int bottom = Math.min(getBottom(), sprite.getBottom());

        for (int x = left; x < right; x++) {
            for (int y = top; y < bottom; y++) {
                if (isFilled(x - getLeft(), y - getTop()) && 
                    sprite.isFilled(x - sprite.getLeft(), y - sprite.getTop())) {
                    return true;
                }
            }
        }
        
        return false; 
    }

    /**
     * Draw the Balloon.
     *
     * @param canvas the canvas
     */
    @Override
    public void onDraw(Canvas canvas) {
        canvas.drawBitmap(image, x, y, null);        
    }
    
    /**
     * If the given pixel is not transparent then it is filled.
     *
     * @param x the x
     * @param y the y
     * @return true, if checks if is filled
     */
    public boolean isFilled(int x, int y) {
        return image.getPixel(x, y) != Color.TRANSPARENT;
    } 
   
}
