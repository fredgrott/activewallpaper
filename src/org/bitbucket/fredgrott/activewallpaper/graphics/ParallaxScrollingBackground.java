/*******************************************************************************
 * Copyright 2012 fredgrott
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package org.bitbucket.fredgrott.activewallpaper.graphics;

import java.util.LinkedHashSet;
import java.util.Set;

import org.bitbucket.fredgrott.activewallpaper.log.LogUtility;


/**
 * The Class ParallaxScrollingBackground.
 */
public class ParallaxScrollingBackground implements LayerScene, LayerOnTickListener {
	
    /** The TAG. */
    public static String TAG = LogUtility.setLOGTAG("ParallaxScrollingBackground");
	
	/** The layers. */
	private LinkedHashSet<BackgroundLayer> layers;

	/**
	 * The Constructor.
	 *
	 * @param layers the layers
	 */
	public ParallaxScrollingBackground(LinkedHashSet<BackgroundLayer> layers) {
		this.layers = layers;
	}

	
	/**
	 * @see{@link org.bitbucket.fredgrott.activewallpaper.graphics.LayerOnTickListener#onTick()}
	 */
	@Override
	public void onTick() {
		for (BackgroundLayer layer : layers) {
			layer.scroll();
		}
		
	}

	
	/**
	 * @see{@link  org.bitbucket.fredgrott.activewallpaper.graphics.LayerScene#getDrawables()}
	 */
	@Override
	public Set<? extends Drawable> getDrawables() {
		
		return layers;
	}

}
