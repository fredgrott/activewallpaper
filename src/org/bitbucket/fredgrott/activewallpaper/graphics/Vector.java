/*******************************************************************************
 * Copyright 2012 fredgrott
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package org.bitbucket.fredgrott.activewallpaper.graphics;

import org.bitbucket.fredgrott.activewallpaper.log.LogUtility;


/**
 * The Class Vector.
 */
public class Vector {
	
	/** The TAG. */
	public static String TAG = LogUtility.setLOGTAG("Vector");
	
    /** The direction. */
    protected float direction;
    
    /** The magnitude. */
    protected float magnitude;
    
    /**
     * Create the vector with the given direction
     * and magnitude.
     *
     * @param direction the direction
     * @param magnitude the magnitude
     */
    public Vector(float direction, float magnitude) {
        this.direction = direction;
        this.magnitude = magnitude;
    }
    
    /**
     * Return the direction of the vector.
     *
     * @return the direction
     */
    public float getDirection() {
        return direction;
    }
    
    /**
     * Set the direction of the vector.
     *
     * @param direction the direction
     */
    public void setDirection(float direction) {
        this.direction = direction;
    }
    
    /**
     * Return the magnitude of the vector.
     *
     * @return the magnitude
     */
    public float getMagnitude() {
        return magnitude;
    }
    
    /**
     * Set the magnitude of the vector.
     *
     * @param magnitude the magnitude
     */
    public void setMagnitude(float magnitude) {
        this.magnitude = magnitude;
    }
    
}
