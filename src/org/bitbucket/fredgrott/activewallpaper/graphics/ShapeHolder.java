/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.bitbucket.fredgrott.activewallpaper.graphics;

import org.bitbucket.fredgrott.activewallpaper.log.LogUtility;

import android.graphics.Paint;
import android.graphics.RadialGradient;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.Shape;


/**
 * A data structure that holds a Shape and various properties that can be used to define
 * how the shape is drawn.
 */
public class ShapeHolder {
	
	/** The TAG. */
	public static String TAG = LogUtility.setLOGTAG("ShapeHolder");
    
    /** The x. */
    private float x = 0, y = 0;
    
    /** The shape. */
    private ShapeDrawable shape;
    
    /** The color. */
    private int color;
    
    /** The gradient. */
    private RadialGradient gradient;
    
    /** The alpha. */
    @SuppressWarnings("unused")
	private float alpha = 1f;
    
    /** The paint. */
    private Paint paint;

    /**
     * Sets the paint.
     *
     * @param value the new paint
     */
    public void setPaint(Paint value) {
        paint = value;
    }
    
    /**
     * Gets the paint.
     *
     * @return the paint
     */
    public Paint getPaint() {
        return paint;
    }

    /**
     * Sets the x.
     *
     * @param value the new x
     */
    public void setX(float value) {
        x = value;
    }
    
    /**
     * Gets the x.
     *
     * @return the x
     */
    public float getX() {
        return x;
    }
    
    /**
     * Sets the y.
     *
     * @param value the new y
     */
    public void setY(float value) {
        y = value;
    }
    
    /**
     * Gets the y.
     *
     * @return the y
     */
    public float getY() {
        return y;
    }
    
    /**
     * Sets the shape.
     *
     * @param value the new shape
     */
    public void setShape(ShapeDrawable value) {
        shape = value;
    }
    
    /**
     * Gets the shape.
     *
     * @return the shape
     */
    public ShapeDrawable getShape() {
        return shape;
    }
    
    /**
     * Gets the color.
     *
     * @return the color
     */
    public int getColor() {
        return color;
    }
    
    /**
     * Sets the color.
     *
     * @param value the new color
     */
    public void setColor(int value) {
        shape.getPaint().setColor(value);
        color = value;
    }
    
    /**
     * Sets the gradient.
     *
     * @param value the new gradient
     */
    public void setGradient(RadialGradient value) {
        gradient = value;
    }
    
    /**
     * Gets the gradient.
     *
     * @return the gradient
     */
    public RadialGradient getGradient() {
        return gradient;
    }

    /**
     * Sets the alpha.
     *
     * @param alpha the new alpha
     */
    public void setAlpha(float alpha) {
        this.alpha = alpha;
        shape.setAlpha((int)((alpha * 255f) + .5f));
    }

    /**
     * Gets the width.
     *
     * @return the width
     */
    public float getWidth() {
        return shape.getShape().getWidth();
    }
    
    /**
     * Sets the width.
     *
     * @param width the new width
     */
    public void setWidth(float width) {
        Shape s = shape.getShape();
        s.resize(width, s.getHeight());
    }

    /**
     * Gets the height.
     *
     * @return the height
     */
    public float getHeight() {
        return shape.getShape().getHeight();
    }
    
    /**
     * Sets the height.
     *
     * @param height the new height
     */
    public void setHeight(float height) {
        Shape s = shape.getShape();
        s.resize(s.getWidth(), height);
    }

    /**
     * Instantiates a new shape holder.
     *
     * @param s the s
     */
    public ShapeHolder(ShapeDrawable s) {
        shape = s;
    }
}