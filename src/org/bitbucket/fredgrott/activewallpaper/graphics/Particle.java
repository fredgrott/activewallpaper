/*******************************************************************************
 * Copyright 2012 fredgrott
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package org.bitbucket.fredgrott.activewallpaper.graphics;

import org.bitbucket.fredgrott.activewallpaper.log.LogUtility;


/**
 * The Class Particle.
 */
public class Particle {
	
	/** The TAG. */
	public static String TAG = LogUtility.setLOGTAG("Particle");
	
    /** The x. */
    protected float x;
    
    /** The y. */
    protected float y;
    
    /**
     * The Constructor.
     */
    public Particle() {
        this.x = 0;
        this.y = 0;
    }
    
    /**
     * The Constructor.
     *
     * @param x the x
     * @param y the y
     */
    public Particle(float x, float y) {        
        this.x = x;
        this.y = y;
    }    

    /**
     * Use some basic trigonometry to apply the given
     * vector to this particle.
     *
     * @param vector the vector
     */
    public void applyVector(Vector vector) {
        float adjacent = 0; //dx
        float opposite = 0; //dy
        float hypotenuse = vector.getMagnitude();
        double angle = Math.toRadians(vector.getDirection());
        
        adjacent = (float) (Math.cos(angle) * hypotenuse);
        opposite = (float) (Math.sin(angle) * hypotenuse);
        
        x += adjacent;
        y += opposite; 
    }
    
    /**
     * Gets the x.
     *
     * @return the X
     */
    public float getX() {
        return x;
    }

    /**
     * Sets the x.
     *
     * @param x the X
     */
    public void setX(float x) {
        this.x = x;
    }

    /**
     * Gets the y.
     *
     * @return the Y
     */
    public float getY() {
        return y;
    }

    /**
     * Sets the y.
     *
     * @param y the Y
     */
    public void setY(float y) {
        this.y = y;
    }    

}
