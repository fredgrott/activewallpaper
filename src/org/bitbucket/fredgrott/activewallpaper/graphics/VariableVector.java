/*******************************************************************************
 * Copyright 2012 fredgrott
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package org.bitbucket.fredgrott.activewallpaper.graphics;

import org.bitbucket.fredgrott.activewallpaper.log.LogUtility;


/**
 * The Class VariableVector.
 */
public class VariableVector extends Vector {
	
	/** The TAG. */
	public static String TAG = LogUtility.setLOGTAG("VariableVector");
	
    /** The min speed. */
    protected float minSpeed;
    
    /** The max speed. */
    protected float maxSpeed;
    
    /** The acceleration. */
    protected float acceleration;
    
    /** The deceleration. */
    protected float deceleration;

    /**
     * Create a variable force vector.
     *
     * @param direction the direction
     * @param magnitude the magnitude
     * @param minSpeed the min speed
     * @param maxSpeed the max speed
     * @param acceleration the acceleration
     * @param deceleration the deceleration
     */
    public VariableVector(float direction, 
                          float magnitude,
                          float minSpeed,
                          float maxSpeed,
                          float acceleration,
                          float deceleration) {
        super(direction, magnitude);
        this.minSpeed = minSpeed;
        this.maxSpeed = maxSpeed;
        this.acceleration = acceleration;
        this.deceleration = deceleration;
    }
    
    /**
     * Increase the magnitude of the vector.
     */
    public void accelerate() {
        if (magnitude < maxSpeed) {
            magnitude += acceleration;
            
            if (magnitude > maxSpeed) {
                magnitude = maxSpeed;
            }
        }
    }
    
    /**
     * Decrease the magnitude of the vector.
     */
    public void decelerate() {
        if (magnitude > minSpeed) {
            magnitude -= deceleration;
            
            if (magnitude < minSpeed) {
                magnitude = minSpeed;
            }
        }
    }

    /**
     * Gets the min speed.
     *
     * @return the minSpeed
     */
    public float getMinSpeed() {
        return minSpeed;
    }

    /**
     * Sets the min speed.
     *
     * @param minSpeed the minSpeed to set
     */
    public void setMinSpeed(float minSpeed) {
        this.minSpeed = minSpeed;
    }

    /**
     * Gets the max speed.
     *
     * @return the maxSpeed
     */
    public float getMaxSpeed() {
        return maxSpeed;
    }

    /**
     * Sets the max speed.
     *
     * @param maxSpeed the maxSpeed to set
     */
    public void setMaxSpeed(float maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    /**
     * Gets the acceleration.
     *
     * @return the acceleration
     */
    public float getAcceleration() {
        return acceleration;
    }

    /**
     * Sets the acceleration.
     *
     * @param acceleration the acceleration to set
     */
    public void setAcceleration(float acceleration) {
        this.acceleration = acceleration;
    }

    /**
     * Gets the deceleration.
     *
     * @return the deceleration
     */
    public float getDeceleration() {
        return deceleration;
    }

    /**
     * Sets the deceleration.
     *
     * @param deceleration the deceleration to set
     */
    public void setDeceleration(float deceleration) {
        this.deceleration = deceleration;
    }       

}
