/*******************************************************************************
 * Copyright 2012 fredgrott
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package org.bitbucket.fredgrott.activewallpaper.cache;

import java.io.Serializable;
import java.util.List;

import org.bitbucket.fredgrott.activewallpaper.log.AppLogger;
import org.bitbucket.fredgrott.activewallpaper.log.LogUtility;
import org.bitbucket.fredgrott.activewallpaper.app.AppHelper;


import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;


/**
 * The Class GenericStore.
 */
public class GenericStore {
    
	/** The Constant TAG. */
	private static final String TAG = LogUtility.setLOGTAG("GenericStore");
	
	/** The Constant TYPE_SHAREDPREF. */
	public static final int TYPE_SHAREDPREF = 0;
	
	/** The Constant TYPE_MEMDISKCACHE. */
	public static final int TYPE_MEMDISKCACHE = 1;
	
	/** The Constant KEY_IMAGECACHEFUPATH. */
	public static final String KEY_IMAGECACHEFUPATH = "IMAGECACHEFUPATH";
    
    /** The Constant KEY_OBJECTCACHEFUPATH. */
    public static final String KEY_OBJECTCACHEFUPATH = "OBJECTCACHEFUPATH";
    
	//set SharedPreferences file name. default value, change it as you wish
	/** The PRE f_ fil e_ name. */
	private static String PREF_FILE_NAME = "WareNinja_appPrefs";
	
	/** The Constant SETTINGS_ISCACHEHAVEDATA. */
	public static final String SETTINGS_ISCACHEHAVEDATA = "ISCACHEHAVEDATA";
    
	/**
	 * Clear all.
	 *
	 * @param type the type
	 * @param context the context
	 */
	public static void clearAll(int type, Context context) {
		clearAll(type, context, "unknown");
	}
	
	/**
	 * Clear all.
	 *
	 * @param type the type
	 * @param context the context
	 * @param caller the caller
	 */
	public static void clearAll(int type, Context context, String caller) {
		if (TYPE_MEMDISKCACHE == type) {
			clearCache(context, caller);
		} else {
			clearLocal(context, caller); 
		}
	}
	
	/**
	 * Clear cache.
	 *
	 * @param context the context
	 * @param caller the caller
	 */
	private static void clearCache(Context context, String caller) {
		((AppHelper) context.getApplicationContext()) .getObjectCache().removeAllObjects();
		((AppHelper) context.getApplicationContext()) .getImageCache().removeAllObjects();
		removeObject(SETTINGS_ISCACHEHAVEDATA, context);
	}
    
    /**
     * Clear local.
     *
     * @param context the context
     * @param caller the caller
     */
    private static void clearLocal(Context context, String caller) {
        Editor editor = 
            context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE).edit();
        editor.clear();
        editor.commit();
    }

    // ---
    /**
     * Save object.
     *
     * @param type the type
     * @param objKey the obj key
     * @param objData the obj data
     * @param context the context
     * @return true, if save object
     */
    public static boolean saveObject(int type, String objKey, Serializable objData, Context context) {
    	
    	if (TYPE_MEMDISKCACHE == type) {
    		return saveObjectInCache(objKey, objData, context);
    	} else {
    		return saveObject(objKey, objData, context);
    	}
    }
    
    /**
     * Save object in cache.
     *
     * @param objKey the obj key
     * @param objData the obj data
     * @param context the context
     * @return true, if save object in cache
     */
    private static boolean saveObjectInCache(String objKey, Serializable objData, Context context) {
    	if (!getCustomBoolean(SETTINGS_ISCACHEHAVEDATA, context)) {
			setCustomData(TYPE_SHAREDPREF, SETTINGS_ISCACHEHAVEDATA, true, context);
    	}
    	
    	ObjectCache objCache = ((AppHelper) context.getApplicationContext()).getObjectCache();
        return objCache.saveObject(objKey, objData);
    }
    
    /**
     * Save object.
     *
     * @param objKey the obj key
     * @param dataObj the data obj
     * @param context the context
     * @return true, if save object
     */
    private static boolean saveObject(String objKey, Serializable dataObj, Context context) {
        Editor editor =
            context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(objKey, ObjectSerializer.serialize(dataObj));
        
        return editor.commit(); 
    }
    
    /**
     * Gets the object.
     *
     * @param type the type
     * @param objKey the obj key
     * @param context the context
     * @return the object
     */
    public static Object getObject(int type, String objKey, Context context) {
    	if (TYPE_MEMDISKCACHE == type) {
    		return getObjectFromCache(objKey, context);
    	} else {
    		return getObject(objKey, context);
    	}
    }
    
    /**
     * Gets the object from cache.
     *
     * @param objKey the obj key
     * @param context the context
     * @return the object from cache
     */
    private static Object getObjectFromCache(String objKey, Context context) {
    	ObjectCache objCache = ((AppHelper) context.getApplicationContext()).getObjectCache();
    	return objCache.getObject(objKey);
    }
    
    /**
     * Gets the object.
     *
     * @param objKey the obj key
     * @param context the context
     * @return the object
     */
    private static Object getObject(String objKey, Context context) {
        SharedPreferences savedSession =
            context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        
        Object dataObj = ObjectSerializer.deserialize(savedSession.getString(objKey, null));
        
        return dataObj;
    }
    
    /**
     * Removes the object.
     *
     * @param type the type
     * @param objKey the obj key
     * @param context the context
     * @return the object
     */
    public static Object removeObject(int type, String objKey, Context context) {
    	if (TYPE_MEMDISKCACHE == type) {
    		return removeObjectFromCache(objKey, context);
    	} else {
    		return removeObject(objKey, context);
    	}
    }
    
    /**
     * Removes the object from cache.
     *
     * @param objKey the obj key
     * @param context the context
     * @return true, if removes the object from cache
     */
    private static boolean removeObjectFromCache(String objKey, Context context) {
    	ObjectCache objCache = ((AppHelper) context.getApplicationContext()).getObjectCache();
    	return objCache.removeObject(objKey);
    }
    
    /**
     * Removes the object.
     *
     * @param objKey the obj key
     * @param context the context
     * @return true, if removes the object
     */
    private static boolean removeObject(String objKey, Context context) {
        Editor editor =
            context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE).edit();
        editor.remove(objKey);
        
        //if(LOGGING.DEBUG)Log.d(TAG, "removedObject| objKey:"+objKey);
        
        return editor.commit(); 
    }
    
    /**
     * Checks if is custom key exist.
     *
     * @param type the type
     * @param customKey the custom key
     * @param context the context
     * @return true, if checks if is custom key exist
     */
    public static boolean isCustomKeyExist(int type, String customKey, Context context) {
    	if (TYPE_MEMDISKCACHE == type) {
        	return isCustomKeyExistInCache(customKey, context);
    	} else {
        	return isCustomKeyExistInLocal(customKey, context);
    	}
    }
    
    /**
     * Checks if is custom key exist in cache.
     *
     * @param objKey the obj key
     * @param context the context
     * @return true, if checks if is custom key exist in cache
     */
    private static boolean isCustomKeyExistInCache(String objKey, Context context) {
    	ObjectCache objCache = ((AppHelper) context.getApplicationContext()).getObjectCache();
    	return objCache.containsKey(objKey);
    }
    
    /**
     * Checks if is custom key exist in local.
     *
     * @param customKey the custom key
     * @param context the context
     * @return true, if checks if is custom key exist in local
     */
    private static boolean isCustomKeyExistInLocal(String customKey, Context context) {
        SharedPreferences savedSession =
            context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        
        return savedSession.contains(customKey);
    }
    // ---
    
    /**
     * Sets the custom data.
     *
     * @param type the type
     * @param key the key
     * @param value the value
     * @param context the context
     * @return true, if sets the custom data
     */
    public static boolean setCustomData(int type, String key, Object value, Context context) {
    	if (TYPE_MEMDISKCACHE == type) {
    		return saveObjectInCache(key, (Serializable) value, context);
    	} else {
    		return setCustomDataInLocal(key, value, context);
    	}
    }
    
    /**
     * Sets the custom data in local.
     *
     * @param key the key
     * @param value the value
     * @param context the context
     * @return true, if sets the custom data in local
     */
    private static boolean setCustomDataInLocal(String key, Object value, Context context) {
    	
    	Editor editor =
            context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE).edit();
    	
    	if (value instanceof Boolean) {
        	editor.putBoolean(key, (Boolean) value);
    	} else if (value instanceof Integer) {
        	editor.putInt(key, (Integer) value);
    	} else if (value instanceof String) {
        	editor.putString(key, (String)  value);
    	} else if (value instanceof Float) {
        	editor.putFloat(key, (Float) value);
    	} else if (value instanceof Long) {
        	editor.putLong(key, (Long) value);
    	}
    	
    	/*
    	final boolean booleanObj = false;
    	final int intObj = -1;
    	final String stringObj = "";
    	final float floatObj = 1F;
    	final long longObj = 1L;
    		
        if (value.getClass().isInstance(booleanObj))
        	editor.putBoolean(key, (Boolean)value);
        else if (value.getClass().isInstance(intObj))
        	editor.putInt(key, (Integer)value);
        else if (value.getClass().isInstance(stringObj))
        	editor.putString(key, (String)value);
        else if (value.getClass().isInstance(floatObj))
        	editor.putFloat(key, (Float)value);
        else if (value.getClass().isInstance(longObj))
        	editor.putLong(key, (Long)value);
        */
        
        return editor.commit(); 
    }
    
    /**
     * Gets the custom boolean.
     *
     * @param key the key
     * @param context the context
     * @return the custom boolean
     */
    public static boolean getCustomBoolean(String key, Context context) {
    	return getCustomBoolean(key, false, context);
    }
    
    /**
     * Gets the custom boolean.
     *
     * @param key the key
     * @param defValue the def value
     * @param context the context
     * @return the custom boolean
     */
    public static boolean getCustomBoolean(String key, boolean defValue, Context context) {
        SharedPreferences savedSession =
            context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        
        return (savedSession.getBoolean(key, defValue));
    }
    
    /**
     * Gets the custom string.
     *
     * @param key the key
     * @param context the context
     * @return the custom string
     */
    public static String getCustomString(String key, Context context) {
    	return getCustomString(key, null, context);
    }
    
    /**
     * Gets the custom string.
     *
     * @param key the key
     * @param defValue the def value
     * @param context the context
     * @return the custom string
     */
    public static String getCustomString(String key, String defValue, Context context) {
        SharedPreferences savedSession =
            context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        
        return (savedSession.getString(key, defValue));
    }
    
    /**
     * Gets the custom int.
     *
     * @param key the key
     * @param context the context
     * @return the custom int
     */
    public static int getCustomInt(String key, Context context) {
    	return getCustomInt(key, -1, context);
    }
    
    /**
     * Gets the custom int.
     *
     * @param key the key
     * @param defValue the def value
     * @param context the context
     * @return the custom int
     */
    public static int getCustomInt(String key, int defValue, Context context) {
        SharedPreferences savedSession =
            context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        
        return (savedSession.getInt(key, defValue));
    }
    
    /**
     * Gets the custom float.
     *
     * @param key the key
     * @param context the context
     * @return the custom float
     */
    public static float getCustomFloat(String key, Context context) {
    	return getCustomFloat(key, -1F, context);
    }
    
    /**
     * Gets the custom float.
     *
     * @param key the key
     * @param defValue the def value
     * @param context the context
     * @return the custom float
     */
    public static float getCustomFloat(String key, float defValue, Context context) {
        SharedPreferences savedSession =
            context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        
        return (savedSession.getFloat(key, defValue));
    }
    
    /**
     * Gets the custom long.
     *
     * @param key the key
     * @param context the context
     * @return the custom long
     */
    public static long getCustomLong(String key, Context context) {
    	return getCustomLong(key, -1L, context);
    }
    
    /**
     * Gets the custom long.
     *
     * @param key the key
     * @param defValue the def value
     * @param context the context
     * @return the custom long
     */
    public static long getCustomLong(String key, long defValue, Context context) {
        SharedPreferences savedSession =
            context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        
        return (savedSession.getLong(key, defValue));
    }
    

    // ------
    //-- <optional> you can use this to track&clear pre-defined set of data from cache
    /** The Constant CUSTOMPREFIXES_INCACHE. */
    public static final String CUSTOMPREFIXES_INCACHE = "CUSTOMPREFIXES_INCACHE"; // object: UsersInCache
    
    /**
     * Check add custom prefix.
     *
     * @param prefix the prefix
     * @param context the context
     */
    public static void checkAddCustomPrefix(String prefix, Context context) {
    	
    	CustomPrefixesInCache prefixesInCache = new CustomPrefixesInCache();
    	if (!isCustomKeyExist(GenericStore.TYPE_MEMDISKCACHE,CUSTOMPREFIXES_INCACHE, context)) {
    		
    		prefixesInCache.add(prefix);
    		AppLogger.d(TAG, CUSTOMPREFIXES_INCACHE + " doesNOT exist, create NEW|" + prefix + "|" + prefixesInCache.toString());
    		
    		saveObject(GenericStore.TYPE_MEMDISKCACHE,CUSTOMPREFIXES_INCACHE, prefixesInCache, context);
    	}
    	else {
    		prefixesInCache = (CustomPrefixesInCache) getObject(GenericStore.TYPE_MEMDISKCACHE,CUSTOMPREFIXES_INCACHE, context);
    		if (!prefixesInCache.contains(prefix)) {
    			prefixesInCache.add(prefix);
    			
    			AppLogger.d(TAG, CUSTOMPREFIXES_INCACHE+" do exist, add and save|" + prefix + "|" + prefixesInCache.toString());
    			
        		saveObject(GenericStore.TYPE_MEMDISKCACHE,CUSTOMPREFIXES_INCACHE,  prefixesInCache, context);
    		}
    	}
    }
    
    /**
     * Check remove custom prefix.
     *
     * @param prefix the prefix
     * @param context the context
     */
    public static void checkRemoveCustomPrefix(String prefix, Context context) {
    	CustomPrefixesInCache prefixesInCache = new CustomPrefixesInCache();
    	if (isCustomKeyExist(GenericStore.TYPE_MEMDISKCACHE,CUSTOMPREFIXES_INCACHE, context)) {
    		
    		prefixesInCache = (CustomPrefixesInCache) getObject(GenericStore.TYPE_MEMDISKCACHE,CUSTOMPREFIXES_INCACHE, context);
    		if (prefixesInCache.contains(prefix)) {
    			prefixesInCache.remove(prefix);
    			
    			AppLogger.d(TAG, CUSTOMPREFIXES_INCACHE + " do exist, add and save|" + prefix + "|" + prefixesInCache.toString());
    			
    			if (prefixesInCache.size() == 0)
    				removeObject(GenericStore.TYPE_MEMDISKCACHE,CUSTOMPREFIXES_INCACHE, context);
    			else
    				saveObject(GenericStore.TYPE_MEMDISKCACHE,CUSTOMPREFIXES_INCACHE, prefixesInCache, context);
    		}
    	}
    }
    
    /**
     * Removes the all custom prefixes.
     *
     * @param context the context
     */
    public static void removeAllCustomPrefixes(Context context) {
    	if ( isCustomKeyExist(GenericStore.TYPE_MEMDISKCACHE,CUSTOMPREFIXES_INCACHE, context)) {
    		CustomPrefixesInCache prefixesInCache = (CustomPrefixesInCache) getObject(GenericStore.TYPE_MEMDISKCACHE,CUSTOMPREFIXES_INCACHE, context);
    		List<String> customPrefixes = prefixesInCache.getCustomPrefixes();
    		
    		AppLogger.d(TAG, CUSTOMPREFIXES_INCACHE + "| will clear all data for " + prefixesInCache.toString());
    		for (String customPrefix:customPrefixes)
    			removeObject(GenericStore.TYPE_MEMDISKCACHE,customPrefix, context);
    		removeObject(GenericStore.TYPE_MEMDISKCACHE,CUSTOMPREFIXES_INCACHE, context);
    	}
    	else {
    		AppLogger.d(TAG, CUSTOMPREFIXES_INCACHE+" doesNOT exist, no action needed");
    	}
    }
    
}
