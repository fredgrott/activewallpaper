/*******************************************************************************
 * Copyright 2012 fredgrott
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package org.bitbucket.fredgrott.activewallpaper.cache;

import org.bitbucket.fredgrott.activewallpaper.log.LogUtility;



/**
 * The Class CacheHelper.
 */
public class CacheHelper {
	
	/** The TAG. */
	@SuppressWarnings("unused")
	private static String TAG = LogUtility.setLOGTAG("CacheHelper");

    /**
     * Gets the file name from url.
     *
     * @param url the url
     * @return the file name from url
     */
    public static String getFileNameFromUrl(String url) {
        // replace all special URI characters with a single + symbol
        return url.replaceAll("[.:/,%?&=]", "+").replaceAll("[+]+", "+");
    }
}
