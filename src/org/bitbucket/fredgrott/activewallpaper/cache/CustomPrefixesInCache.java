/***
 * 	Copyright (c) 2010-2011 WareNinja.com
 * 	Author: yg@wareninja.com
 * 	
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
*/

package org.bitbucket.fredgrott.activewallpaper.cache;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import org.bitbucket.fredgrott.activewallpaper.log.LogUtility;




/**
 * The Class CustomPrefixesInCache.
 */
public class CustomPrefixesInCache implements Serializable {
	
	
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
    
	/** The Constant TAG. */
	@SuppressWarnings("unused")
	private static final String TAG =  LogUtility.setLOGTAG("customPrefixesInCache");
	
	/** The custom prefixes. */
	private List<String> customPrefixes;
    
    /**
     * The Constructor.
     */
    public CustomPrefixesInCache() {
    	customPrefixes = new LinkedList<String>(); 
    }
    
	/**
	 * Gets the custom prefixes.
	 *
	 * @return the custom prefixes
	 */
	public List<String> getCustomPrefixes() {
		return customPrefixes;
	}
	
	/**
	 * Sets the custom prefixes.
	 *
	 * @param customPrefixes the custom prefixes
	 */
	public void setCustomPrefixes(List<String> customPrefixes) {
		this.customPrefixes = customPrefixes;
	}

	/**
	 * Contains.
	 *
	 * @param prefix the prefix
	 * @return true, if contains
	 */
	public boolean contains(String prefix) {
		return customPrefixes.contains(prefix);
	}
	
	/**
	 * Adds the.
	 *
	 * @param prefix the prefix
	 */
	public void add(String prefix) {
		customPrefixes.add(prefix);
	}
	
	/**
	 * Removes the.
	 *
	 * @param prefix the prefix
	 */
	public void remove(String prefix) {
		customPrefixes.remove(prefix);
	}
	
	/**
	 * Size.
	 *
	 * @return the int
	 */
	public int size() {
		return customPrefixes.size();
	}

	
	/**
	 * To string.
	 *
	 * @return the string
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CustomPrefixesInCache [customPrefixes=" + customPrefixes + "]";
	}
}