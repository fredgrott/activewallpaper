/*******************************************************************************
 * Copyright 2012 fredgrott
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
/* Copyright (c) 2009 Matthias Kaeppler
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.bitbucket.fredgrott.activewallpaper.cache;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.bitbucket.fredgrott.activewallpaper.log.LogUtility;



import android.graphics.Bitmap;
import android.graphics.BitmapFactory;


// TODO: Auto-generated Javadoc
/**
 * Implements a cache capable of caching image files. It exposes helper methods to immediately
 * access binary image data as {@link Bitmap} objects.
 * 
 * @author Matthias Kaeppler
 * 
 */
public class ImageCache extends AbstractCache<String, byte[]> {
	
	/** The TAG. */
	@SuppressWarnings("unused")
	private static String TAG = LogUtility.setLOGTAG("imageCache");

    /**
     * The Constructor.
     *
     * @param initialCapacity the initial capacity
     * @param expirationInMinutes the expiration in minutes
     * @param maxConcurrentThreads the max concurrent threads
     */
    public ImageCache(int initialCapacity, long expirationInMinutes, int maxConcurrentThreads) {
        super("ImageCache", initialCapacity, expirationInMinutes, maxConcurrentThreads);
    }

   
    /**
     * Gets the file name for key.
     *
     * @param imageUrl the image url
     * @return the file name for key
     * @see org.bitbucket.fredgrott.activewallpaper.cache.AbstractCache#getFileNameForKey(java.lang.Object)
     */
    @Override
    public String getFileNameForKey(String imageUrl) {
        return CacheHelper.getFileNameFromUrl(imageUrl);
    }

    
    /**
     * Read value from disk.
     *
     * @param file the file
     * @return the byte[]
     * @throws IOException the IO exception
     * @see  org.bitbucket.fredgrott.activewallpaper.cache.AbstractCache#readValueFromDisk(java.io.File)
     */
    @Override
    protected byte[] readValueFromDisk(File file) throws IOException {
        
    	//BufferedInputStream istream = new BufferedInputStream(new FileInputStream(file));
    	BufferedInputStream istream = new BufferedInputStream(new FileInputStream(file), 10240);//YG: 10k=10240
        long fileSize = file.length();
        if (fileSize > Integer.MAX_VALUE) {
            throw new IOException("Cannot read files larger than " + Integer.MAX_VALUE + " bytes");
        }

        int imageDataLength = (int) fileSize;

        byte[] imageData = new byte[imageDataLength];
        istream.read(imageData, 0, imageDataLength);
        istream.close();

        return imageData;
    }

    /**
     * Gets the bitmap.
     *
     * @param elementKey the element key
     * @return the bitmap
     */
    public synchronized Bitmap getBitmap(Object elementKey) {
        byte[] imageData = super.get(elementKey);
        if (imageData == null) {
            return null;
        }
        return BitmapFactory.decodeByteArray(imageData, 0, imageData.length);
    }

    
    /**
     * Write value to disk.
     *
     * @param ostream the ostream
     * @param imageData the image data
     * @throws IOException the IO exception
     * @see org.bitbucket.fredgrott.activewallpaper.cache.AbstractCache#writeValueToDisk(BufferedOutputStream, Object)
     */
    @Override
    protected void writeValueToDisk(BufferedOutputStream ostream, byte[] imageData)
            throws IOException {
        ostream.write(imageData);
    }
    
    // YG: added for my app
    /**
     * Removes the all objects.
     *
     * @return the int
     */
    public synchronized int removeAllObjects() {
    	int size = super.size();
    	
    	super.clear();
    	
    	return size;
    }
}
