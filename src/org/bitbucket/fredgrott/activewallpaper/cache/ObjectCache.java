/*******************************************************************************
 * Copyright 2012 fredgrott
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
/* Copyright (c) 2010-2011 WareNinja.com
 * Author: yg@wareninja.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.bitbucket.fredgrott.activewallpaper.cache;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;

import org.bitbucket.fredgrott.activewallpaper.log.LogUtility;




/**
 * Implements a cache capable of caching Serializable objects. 
 * It exposes helper methods to immediately access objects
 * 
 * @author yg@wareninja.com
 *  
 */
public class ObjectCache extends AbstractCache<String, byte[]> {
	
	/** The TAG. */
	@SuppressWarnings("unused")
	private static String TAG = LogUtility.setLOGTAG("ObjectCache");

    /**
     * The Constructor.
     *
     * @param initialCapacity the initial capacity
     * @param expirationInMinutes the expiration in minutes
     * @param maxConcurrentThreads the max concurrent threads
     */
    public ObjectCache(int initialCapacity, long expirationInMinutes, int maxConcurrentThreads) {
        super("ObjectCache", initialCapacity, expirationInMinutes, maxConcurrentThreads);
    }

    
    /**
     * Gets the file name for key.
     *
     * @param imageUrl the image url
     * @return the file name for key
     * @see org.bitbucket.fredgrott.activewallpaper.cache.AbstractCache#getFileNameForKey(Object)
     */
    @Override
    public String getFileNameForKey(String imageUrl) {
        return CacheHelper.getFileNameFromUrl(imageUrl);
    }

    
    /**
     * Read value from disk.
     *
     * @param file the file
     * @return the byte[]
     * @throws IOException the IO exception
     * @see org.bitbucket.fredgrott.activewallpaper.cache.AbstractCache#readValueFromDisk(java.io.File)
     */
    @Override
    protected byte[] readValueFromDisk(File file) throws IOException {
        BufferedInputStream istream = new BufferedInputStream(new FileInputStream(file), 10240); //YG: 10k=10240
        long fileSize = file.length();
        if (fileSize > Integer.MAX_VALUE) {
            throw new IOException("Cannot read files larger than " + Integer.MAX_VALUE + " bytes");
        }

        int objectDataLength = (int) fileSize;

        byte[] objectData = new byte[objectDataLength];
        istream.read(objectData, 0, objectDataLength);
        istream.close();

        return objectData;
    }

    /**
     * Gets the object.
     *
     * @param elementKey the element key
     * @return the object
     */
    public synchronized Object getObject(Object elementKey) {
        byte[] objectData = super.get(elementKey);
        if (objectData == null) {
            return null;
        }
        return ObjectSerializer.deserialize( new String(objectData) );
    }
    
    /**
     * Save object.
     *
     * @param key the key
     * @param obj the obj
     * @return true, if save object
     */
    public synchronized boolean saveObject(String key, Serializable obj) {
    	super.put(key, ObjectSerializer.serialize(obj).getBytes());
    	return true;
    }
    
    /**
     * Removes the object.
     *
     * @param key the key
     * @return true, if removes the object
     */
    public synchronized boolean removeObject(String key) {
    	super.remove(key);
    	return true;
    }
    
    /**
     * Removes the all objects.
     *
     * @return the int
     */
    public synchronized int removeAllObjects() {
    	//Log.d("ObjectCache", "clearing " + super.size() + " from ObjectCache");
    	int size = super.size();
    	super.clear();
    	
    	return size;
    }
    
   
    /**
     * Write value to disk.
     *
     * @param ostream the ostream
     * @param imageData the image data
     * @throws IOException the IO exception
     * @see  org.bitbucket.fredgrott.activewallpaper.cache.AbstractCache#writeValueToDisk(BufferedOutputStream, Object)
     */
    @Override
    protected void writeValueToDisk(BufferedOutputStream ostream, byte[] imageData)
            throws IOException {
        ostream.write(imageData);
    }
}
