/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.bitbucket.fredgrott.activewallpaper.view;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;


/**
 * Implements a surface view which writes updates to the surface's canvas using
 * a separate rendering thread.  This class is based heavily on GLSurfaceView.
 */
public class CanvasSurfaceView extends SurfaceView implements SurfaceHolder.Callback {
    
    /** The m size changed. */
    private boolean mSizeChanged = true;

    /** The m holder. */
    private SurfaceHolder mHolder;
    
    /** The m canvas thread. */
    private CanvasThread mCanvasThread;
    
    /**
     * The Constructor.
     *
     * @param context the context
     */
    public CanvasSurfaceView(Context context) {
        super(context);
        init();
    }

    /**
     * The Constructor.
     *
     * @param context the context
     * @param attrs the attrs
     */
    public CanvasSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    /**
     * Inits the.
     */
    private void init() {
        // Install a SurfaceHolder.Callback so we get notified when the
        // underlying surface is created and destroyed
        mHolder = getHolder();
        mHolder.addCallback(this);
        
    }

    /**
     * Gets the surface holder.
     *
     * @return the surface holder
     */
    public SurfaceHolder getSurfaceHolder() {
        return mHolder;
    }

    /**
     * Sets the user's renderer and kicks off the rendering thread.
     *
     * @param renderer the renderer
     */
    public void setRenderer(Renderer renderer) {
        mCanvasThread = new CanvasThread(mHolder, renderer);
        mCanvasThread.start();
    }

    
    /**
     * Surface created.
     *
     * @param holder the holder
     * @see android.view.SurfaceHolder.Callback#surfaceCreated(android.view.SurfaceHolder)
     */
    public void surfaceCreated(SurfaceHolder holder) {
        mCanvasThread.surfaceCreated();
    }

    
    /**
     * Surface destroyed.
     *
     * @param holder the holder
     * @see  android.view.SurfaceHolder.Callback#surfaceDestroyed(android.view.SurfaceHolder)
     */
    public void surfaceDestroyed(SurfaceHolder holder) {
        // Surface will be destroyed when we return
        mCanvasThread.surfaceDestroyed();
    }

   
    /**
     * Surface changed.
     *
     * @param holder the holder
     * @param format the format
     * @param w the w
     * @param h the h
     * @see  android.view.SurfaceHolder.Callback#surfaceChanged(android.view.SurfaceHolder, int, int, int)
     */
    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        // Surface size or format has changed. This should not happen in this
        // example.
        mCanvasThread.onWindowResize(w, h);
    }

    /** Inform the view that the activity is paused.*/
    public void onPause() {
        mCanvasThread.onPause();
    }

    /** Inform the view that the activity is resumed. */
    public void onResume() {
        mCanvasThread.onResume();
    }

    /**
     * Inform the view that the window focus has changed.
     *
     * @param hasFocus the has focus
     */
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        mCanvasThread.onWindowFocusChanged(hasFocus);
    }

    /**
     * Set an "event" to be run on the rendering thread.
     * @param r the runnable to be run on the rendering thread.
     */
    public void setEvent(Runnable r) {
        mCanvasThread.setEvent(r);
    }
    
    /** Clears the runnable event, if any, from the rendering thread. */
    public void clearEvent() {
        mCanvasThread.clearEvent();
    }

    
    /**
     * On detached from window.
     *
     * @see  android.view.SurfaceView#onDetachedFromWindow()
     */
    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        mCanvasThread.requestExitAndWait();
    }
    
    /**
     * Stop drawing.
     */
    protected void stopDrawing() {
        mCanvasThread.requestExitAndWait();
    }

    // ----------------------------------------------------------------------

    /** A generic renderer interface. */
    public interface Renderer {
       
        /**
         * Surface changed size.
         * Called after the surface is created and whenever
         * the surface size changes. Set your viewport here.
         *
         * @param width the width
         * @param height the height
         */
        void sizeChanged(int width, int height);
        /**
         * Draw the current frame.
         * @param canvas The target canvas to draw into.
         */
        void drawFrame(Canvas canvas);
    }


    /**
     * A generic Canvas rendering Thread. Delegates to a Renderer instance to do
     * the actual drawing.
     */
    class CanvasThread extends Thread {
        
        /** The m done. */
        private boolean mDone;
        
        /** The m paused. */
        private boolean mPaused;
        
        /** The m has focus. */
        private boolean mHasFocus;
        
        /** The m has surface. */
        private boolean mHasSurface;
        
        /** The m context lost. */
        private boolean mContextLost;
        
        /** The m width. */
        private int mWidth;
        
        /** The m height. */
        private int mHeight;
        
        /** The m renderer. */
        private Renderer mRenderer;
        
        /** The m event. */
        private Runnable mEvent;
        
        /** The m surface holder. */
        private SurfaceHolder mSurfaceHolder;
        
        /**
         * The Constructor.
         *
         * @param holder the holder
         * @param renderer the renderer
         */
        CanvasThread(SurfaceHolder holder, Renderer renderer) {
            super();
            mDone = false;
            mWidth = 0;
            mHeight = 0;
            mRenderer = renderer;
            mSurfaceHolder = holder;
            setName("CanvasThread");
        }

        
        /**
         * Run.
         *
         * @see  java.lang.Thread#run()
         */
        @Override
        public void run() {
           
            boolean tellRendererSurfaceChanged = true;

            /*
             * This is our main activity thread's loop, we go until
             * asked to quit.
             */
            final ProfileRecorder profiler = ProfileRecorder.sSingleton;
            while (!mDone) {
                profiler.start(ProfileRecorder.PROFILE_FRAME);
                /*
                 *  Update the asynchronous state (window size)
                 */
                int w;
                int h;
                synchronized (this) {
                    // If the user has set a runnable to run in this thread,
                    // execute it and record the amount of time it takes to 
                    // run.
                    if (mEvent != null) {
                        profiler.start(ProfileRecorder.PROFILE_SIM);
                        mEvent.run();
                        profiler.stop(ProfileRecorder.PROFILE_SIM);
                    }
                   
                    if (needToWait()) {
                        while (needToWait()) {
                            try {
                                wait();
                            } catch (InterruptedException e) {
                                
                            }
                        }
                    }
                    if (mDone) {
                        break;
                    }
                    tellRendererSurfaceChanged = mSizeChanged;
                    w = mWidth;
                    h = mHeight;
                    mSizeChanged = false;
                }
               
               
                if (tellRendererSurfaceChanged) {
                    mRenderer.sizeChanged(w, h);
                    tellRendererSurfaceChanged = false;
                }
                
                if ((w > 0) && (h > 0)) {
                    // Get ready to draw.
                    // We record both lockCanvas() and unlockCanvasAndPost()
                    // as part of "page flip" time because either may block
                    // until the previous frame is complete.
                    profiler.start(ProfileRecorder.PROFILE_PAGE_FLIP);
                    Canvas canvas = mSurfaceHolder.lockCanvas();
                    profiler.start(ProfileRecorder.PROFILE_PAGE_FLIP);
                    if (canvas != null) {
                        // Draw a frame!
                        profiler.start(ProfileRecorder.PROFILE_DRAW);
                        mRenderer.drawFrame(canvas);
                        profiler.stop(ProfileRecorder.PROFILE_DRAW);
                        
                        profiler.start(ProfileRecorder.PROFILE_PAGE_FLIP);
                        mSurfaceHolder.unlockCanvasAndPost(canvas);
                        profiler.stop(ProfileRecorder.PROFILE_PAGE_FLIP);
                        
                    }
                }
                profiler.stop(ProfileRecorder.PROFILE_FRAME);
                profiler.endFrame();
            }
        }

        /**
         * Need to wait.
         *
         * @return true, if need to wait
         */
        private boolean needToWait() {
            return (mPaused || (!mHasFocus) || (!mHasSurface) || mContextLost)
                && (!mDone);
        }

        /**
         * Surface created.
         */
        public void surfaceCreated() {
            synchronized (this) {
                mHasSurface = true;
                mContextLost = false;
                notify();
            }
        }

        /**
         * Surface destroyed.
         */
        public void surfaceDestroyed() {
            synchronized (this) {
                mHasSurface = false;
                notify();
            }
        }

        /**
         * On pause.
         */
        public void onPause() {
            synchronized (this) {
                mPaused = true;
            }
        }

        /**
         * On resume.
         */
        public void onResume() {
            synchronized (this) {
                mPaused = false;
                notify();
            }
        }

        /**
         * On window focus changed.
         *
         * @param hasFocus the has focus
         */
        public void onWindowFocusChanged(boolean hasFocus) {
            synchronized (this) {
                mHasFocus = hasFocus;
                if (mHasFocus == true) {
                    notify();
                }
            }
        }
        
        /**
         * On window resize.
         *
         * @param w the w
         * @param h the h
         */
        public void onWindowResize(int w, int h) {
            synchronized (this) {
                mWidth = w;
                mHeight = h;
                mSizeChanged = true;
            }
        }

        /**
         * Request exit and wait.
         */
        public void requestExitAndWait() {
            // don't call this from CanvasThread thread or it is a guaranteed
            // deadlock!
            synchronized (this) {
                mDone = true;
                notify();
            }
            try {
                join();
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
        }

        /**
         * Queue an "event" to be run on the rendering thread.
         * @param r the runnable to be run on the rendering thread.
         */
        public void setEvent(Runnable r) {
            synchronized (this) {
                mEvent = r;
            }
        }
        
        /**
         * Clear event.
         */
        public void clearEvent() {
            synchronized (this) {
                mEvent = null;
            }
        }
        
    }

    
}
