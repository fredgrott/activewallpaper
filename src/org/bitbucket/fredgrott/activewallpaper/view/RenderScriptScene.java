/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.bitbucket.fredgrott.activewallpaper.view;



import org.bitbucket.fredgrott.activewallpaper.log.LogUtility;

import android.content.res.Resources;
import android.os.Bundle;
import android.renderscript.RenderScriptGL;
import android.renderscript.ScriptC;


/**
 * The Class RenderScriptScene. The original is from the Android OpenSource 
 * Project.
 */
public abstract class RenderScriptScene {
	
	/** The TAG. */
	@SuppressWarnings("unused")
	private static String TAG = LogUtility.setLOGTAG("RenderScriptScene");
    
    /** The m width. */
    protected int mWidth;
    
    /** The m height. */
    protected int mHeight;
    
    /** The m preview. */
    protected boolean mPreview;
    
    /** The m resources. */
    protected Resources mResources;
    
    /** The m rs. */
    protected RenderScriptGL mRS;
    
    /** The m script. */
    protected ScriptC mScript;

    /**
     * Instantiates a new render script scene.
     *
     * @param width the width
     * @param height the height
     */
    public RenderScriptScene(int width, int height) {
        mWidth = width;
        mHeight = height;
    }

    /**
     * Inits the.
     *
     * @param rs the rs
     * @param res the res
     * @param isPreview the is preview
     */
    public void init(RenderScriptGL rs, Resources res, boolean isPreview) {
        mRS = rs;
        mResources = res;
        mPreview = isPreview;
        mScript = createScript();
    }

    /**
     * Checks if is preview.
     *
     * @return true, if is preview
     */
    public boolean isPreview() {
        return mPreview;
    }

    /**
     * Gets the width.
     *
     * @return the width
     */
    public int getWidth() {
        return mWidth;
    }

    /**
     * Gets the height.
     *
     * @return the height
     */
    public int getHeight() {
        return mHeight;
    }

    /**
     * Gets the resources.
     *
     * @return the resources
     */
    public Resources getResources() {
        return mResources;
    }

    /**
     * Gets the rS.
     *
     * @return the rS
     */
    public RenderScriptGL getRS() {
        return mRS;
    }

    /**
     * Gets the script.
     *
     * @return the script
     */
    public ScriptC getScript() {
        return mScript;
    }

    /**
     * Creates the script.
     *
     * @return the script c
     */
    protected abstract ScriptC createScript();

    /**
     * Stop.
     */
    public void stop() {
        mRS.bindRootScript(null);
    }

    /**
     * Start.
     */
    public void start() {
        mRS.bindRootScript(mScript);
    }

    /**
     * Resize.
     *
     * @param width the width
     * @param height the height
     */
    public void resize(int width, int height) {
        mWidth = width;
        mHeight = height;
    }

    /**
     * Sets the offset.
     *
     * @param xOffset the x offset
     * @param yOffset the y offset
     * @param xPixels the x pixels
     * @param yPixels the y pixels
     */
    
    public void setOffset(float xOffset, float yOffset, int xPixels, int yPixels) {
    }

    /**
     * On command.
     *
     * @param action the action
     * @param x the x
     * @param y the y
     * @param z the z
     * @param extras the extras
     * @param resultRequested the result requested
     * @return the bundle
     */
    
    public Bundle onCommand(String action, int x, int y, int z, Bundle extras,
            boolean resultRequested) {
        return null;
    }
}
