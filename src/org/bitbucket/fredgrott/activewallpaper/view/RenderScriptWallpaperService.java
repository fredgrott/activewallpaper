/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.bitbucket.fredgrott.activewallpaper.view;



import org.bitbucket.fredgrott.activewallpaper.log.LogUtility;

import android.os.Bundle;
import android.renderscript.RenderScript;
import android.renderscript.RenderScriptGL;
import android.view.SurfaceHolder;




/**
 * The Class RenderScriptWallpaperService.
 *
 * @param <T> the generic type
 */
public abstract class RenderScriptWallpaperService<T extends RenderScriptScene>  extends GLWallpaperService {

/** The TAG. */
@SuppressWarnings("unused")
private static String TAG = LogUtility.setLOGTAG("RenderScriptWallpaperService");
	

	
   /**
    * On create engine.
    *
    * @return the engine
    * @see  android.service.wallpaper.WallpaperService#onCreateEngine()
    */
	public Engine onCreateEngine() {
        return new RenderScriptEngine();
    }

	/**
	 * Creates the scene.
	 *
	 * @param width the width
	 * @param height the height
	 * @return the T
	 */
	protected abstract T createScene(int width, int height);
	
	/**
	 * The Class RenderScriptEngine.
	 */
	private class RenderScriptEngine extends Engine {
        
        /** The m rs. */
        private RenderScriptGL mRs;
        
        /** The m renderer. */
        private T mRenderer;

      
        /**
         * On create.
         *
         * @param surfaceHolder the surface holder
         * @see  android.service.wallpaper.WallpaperService.Engine#onCreate(android.view.SurfaceHolder)
         */
        @Override
        public void onCreate(SurfaceHolder surfaceHolder) {
            super.onCreate(surfaceHolder);
            setTouchEventsEnabled(false);
            surfaceHolder.setSizeFromLayout();
        }

        
        /**
         * On destroy.
         *
         * @see  android.service.wallpaper.WallpaperService.Engine#onDestroy()
         */
        @Override
        public void onDestroy() {
            super.onDestroy();
            destroyRenderer();
        }

        /**
         * Destroy renderer.
         */
        private void destroyRenderer() {
            if (mRenderer != null) {
                mRenderer.stop();
                mRenderer = null;
            }
            if (mRs != null) {
                mRs.destroy();
                mRs = null;
            }
        }

        
        /**
         * On visibility changed.
         *
         * @param visible the visible
         * @see  android.service.wallpaper.WallpaperService.Engine#onVisibilityChanged(boolean)
         */
        @Override
        public void onVisibilityChanged(boolean visible) {
            super.onVisibilityChanged(visible);
            if (mRenderer != null) {
                if (visible) {
                    mRenderer.start();
                } else {
                    mRenderer.stop();
                }
            }
        }

       
        /**
         * On surface changed.
         *
         * @param holder the holder
         * @param format the format
         * @param width the width
         * @param height the height
         * @see android.service.walllpaper.WallpaperService.Engine#onSurfaceChanged(android.view.SurfaceHolder, int, int, int)
         */
        @Override
        public void onSurfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            super.onSurfaceChanged(holder, format, width, height);
            if (mRs != null) {
                mRs.setSurface(holder, width, height);
            }
            if (mRenderer == null) {
                mRenderer = createScene(width, height);
                mRenderer.init(mRs, getResources(), isPreview());
                mRenderer.start();
            } else {
                mRenderer.resize(width, height);
            }
        }

        
        /**
         * On offsets changed.
         *
         * @param xOffset the x offset
         * @param yOffset the y offset
         * @param xStep the x step
         * @param yStep the y step
         * @param xPixels the x pixels
         * @param yPixels the y pixels
         * @see  android.service.wallpaper.WallpaperService.Engine#onOffsetsChanged(float, float, float, float, int, int)
         */
        @Override
        public void onOffsetsChanged(float xOffset, float yOffset,
                float xStep, float yStep, int xPixels, int yPixels) {
            mRenderer.setOffset(xOffset, yOffset, xPixels, yPixels);
        }

      
        /**
         * On surface created.
         *
         * @param holder the holder
         * @see  android.service.wallpaper.WallpaperService.Engine#onSurfaceCreated(android.view.SurfaceHolder)
         */
        @Override
        public void onSurfaceCreated(SurfaceHolder holder) {
            super.onSurfaceCreated(holder);

            RenderScriptGL.SurfaceConfig sc = new RenderScriptGL.SurfaceConfig();
            mRs = new RenderScriptGL(RenderScriptWallpaperService.this, sc);
            mRs.setPriority(RenderScript.Priority.LOW);
        }

        
        /**
         * On surface destroyed.
         *
         * @param holder the holder
         * @see  android.service.wallpaper.WallpaperService.Engine#onSurfaceDestroyed(android.view.SurfaceHolder)
         */
        @Override
        public void onSurfaceDestroyed(SurfaceHolder holder) {
            super.onSurfaceDestroyed(holder);
            destroyRenderer();
        }

        
        /**
         * On command.
         *
         * @param action the action
         * @param x the x
         * @param y the y
         * @param z the z
         * @param extras the extras
         * @param resultRequested the result requested
         * @return the bundle
         * @see android.service.wallpaper.WallpaperService.Engine#onCommand(java.lang.String, int, int, int, android.os.Bundle, boolean)
         */
        @Override
        public Bundle onCommand(String action, int x, int y, int z,
                Bundle extras, boolean resultRequested) {
            return mRenderer.onCommand(action, x, y, z, extras, resultRequested);
        }

    }
}
