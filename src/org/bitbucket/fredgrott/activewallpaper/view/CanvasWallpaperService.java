/*******************************************************************************
 * Copyright 2012 fredgrott
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package org.bitbucket.fredgrott.activewallpaper.view;

import java.util.ArrayList;
import java.util.List;

import android.service.wallpaper.WallpaperService;
import android.view.SurfaceHolder;


/**
 * The Class CanvasWallpaperService.
 */
public abstract class CanvasWallpaperService extends WallpaperService {
	
	/**
	 * The Interface Renderer.
	 */
	public interface Renderer extends CanvasSurfaceView.Renderer {
		
	}

	/**
	 * The Class CanvasEngine.
	 */
	public class CanvasEngine extends WallpaperService.Engine {
		
		/** The lock. */
		private Object lock = new Object();
		
		/** The m canvas surface view. */
		private CanvasSurfaceView mCanvasSurfaceView;
		 
 		/** The pending operations. */
 		private List<Runnable> pendingOperations = new ArrayList<Runnable>();
		 
		 /**
 		 * The Constructor.
 		 */
 		public CanvasEngine(){
			 
		 }
		 
		 /**
 		 * Sets the renderer.
 		 *
 		 * @param renderer the renderer
 		 */
 		public void setRenderer(final CanvasSurfaceView.Renderer renderer ) {
			 synchronized(lock) {
				 if(mCanvasSurfaceView != null) {
					 mCanvasSurfaceView.setRenderer(renderer);
					 if(!isVisible()) {
						 mCanvasSurfaceView.onPause();
					 }
				 } else {
					 pendingOperations.add(new Runnable() {
	                        public void run() {
	                            setRenderer(renderer);
	                        }
	                    });
				 }
			 }
		 }
		 
		 /**
 		 * Queue event.
 		 *
 		 * @param r the r
 		 */
 		public void queueEvent(final Runnable r) {
			 synchronized(lock) {
				 if(mCanvasSurfaceView != null) {
					 mCanvasSurfaceView.setEvent(r);
				 } else {
					 pendingOperations.add(new Runnable() {
	                        public void run() {
	                            queueEvent(r);
	                        }
	                    });
				 }
			 }
		 }
		 
		 
 		/**
		  * On visibility changed.
		  *
		  * @param visible the visible
		  * @see android.service.wallpaper.WallpaperService.Engine#onVisibilityChanged(boolean)
		  */
 		@Override
	        public void onVisibilityChanged(final boolean visible) {
	            super.onVisibilityChanged(visible);
	            
	            synchronized(lock) {
	            	if(mCanvasSurfaceView != null) {
	            		if(visible) {
	            			mCanvasSurfaceView.onResume();
	            		} else {
	            			mCanvasSurfaceView.onPause();
	            		}
	            	} else{
	            		pendingOperations.add(new Runnable() {
	                        public void run() {
	                            if (visible) {
	                            	mCanvasSurfaceView.onResume();
	                            } else {
	                            	mCanvasSurfaceView.onPause();
	                            }
	                        }
	                    });
	            	}
		 }
	}
		 
		
 		/**
		  * On surface changed.
		  *
		  * @param holder the holder
		  * @param format the format
		  * @param width the width
		  * @param height the height
		  * @see android.service.wallpaper.WallpaperService.Engine#onSurfaceChanged(android.view.SurfaceHolder, int, int, int)
		  */
 		@Override
	        public void onSurfaceChanged(final SurfaceHolder holder, final int format, final int width, final int height) {
	            synchronized (lock) {
	            	if(mCanvasSurfaceView != null) {
	            		mCanvasSurfaceView.surfaceChanged(holder, format, width, height);
	            	} else {
	                    pendingOperations.add(new Runnable() {
	                        public void run() {
	                            onSurfaceChanged(holder, format, width, height);
	                        }
	                    });
	                }
	            }
	        }
		 
		 
		
 		/**
		  * On surface created.
		  *
		  * @param holder the holder
		  * @see  android.service.wallpaper.WallpaperService.Engine#onSurfaceCreated(android.view.SurfaceHolder)
		  */
 		@Override
	        public void onSurfaceCreated(SurfaceHolder holder) {
	            synchronized (lock) {
	            	 if (mCanvasSurfaceView == null) {
	                     mCanvasSurfaceView = new CanvasSurfaceView(CanvasWallpaperService.this) {
	                         @Override
	                         public SurfaceHolder getHolder() {
	                             return CanvasEngine.this.getSurfaceHolder();
	                         }
	                     };
	                     for (Runnable pendingOperation: pendingOperations) {
	                         pendingOperation.run();
	                     }
	                     pendingOperations.clear();
	                 }
	                 mCanvasSurfaceView.surfaceCreated(holder);
	             }
	         }
		 
		
 		/**
		  * On surface destroyed.
		  *
		  * @param holder the holder
		  * @see android.service.wallpaper.WallpaperService.Engine#onSurfaceDestroyed(android.view.SurfaceHolder)
		  */
 		@Override
	        public void onSurfaceDestroyed(SurfaceHolder holder) {
	            synchronized (lock) {
	                if (mCanvasSurfaceView != null) {
	                    mCanvasSurfaceView.surfaceDestroyed(holder);
	                }
	            }
	        }
	}
	
}
