
/**
 * ActiveWallpaper is an Android Project Library designed to make it easier to 
 * develop Live Wallpaper Applications for Android devices. Typically the core
 * class you want to extend as your entry point is the AppHelper class
 * found in the app package.
 * 
 * You will than either extend a GLWallpaperService or CanvasWallpaperService 
 * class from the view package depending upon whether your LWP is GL based or
 * Canvas based.
 */
package org.bitbucket.fredgrott.activewallpaper;
