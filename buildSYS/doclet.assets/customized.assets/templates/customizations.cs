<?cs # placeholder for custom clearsilver code. ?>
<?cs
def:codeqa_toc() ?>
<ul>
<li><a href="<?cs var:toroot ?>androidlint.html">AndroidLint</a></li>
<li><a href="<?cs var:toroot ?>androidcheckstyle.html">Checkstyle</a></li>
<li><a href="<?cs var:toroot ?>androidpmd.html">PMD</a></li>
<li><a href="<?cs var:toroot ?>androidclassycle.html">Classycle</a></li>
<li><a href="<?cs var:toroot ?>androidjdepend.html">JDepend</a></li>
</ul>
<?cs /def ?>
<?cs
def:guide_toc() ?>
<ul>
<li><a href="<?cs var:toroot ?>guidesetup.html">SetUp</a></li>
<li><a href="<?cs var:toroot ?>guideusage.html">Usage</a></li>
</ul>
<?cs /def ?>
<?cs
def:licensecredits_toc() ?>
<ul>
<li><a href="<?cs var:toroot ?>license.html">License</a></li>
<li><a href="<?cs var:toroot ?>credits.html">Credits</a></li>
</ul>
<?cs /def ?>

<?cs 
def:codeqa_nav() ?>
  <div class="g-section g-tpl-240" id="body-content">
    <div class="g-unit g-first" id="side-nav">
      <div id="devdoc-nav"><?cs 
        include:"<?cs var:toroot ?><?cs call:codeqa_toc() ?>" ?>
      </div>
    </div> <!-- end side-nav -->
<?cs /def ?>
<?cs 
def:guide_nav() ?>
  <div class="g-section g-tpl-240" id="body-content">
    <div class="g-unit g-first" id="side-nav">
      <div id="devdoc-nav"><?cs 
        include:"<?cs var:toroot ?><?cs call:guide_toc() ?>" ?>
      </div>
    </div> <!-- end side-nav -->
<?cs /def ?>
<?cs 
def:licensecredits_nav() ?>
  <div class="g-section g-tpl-240" id="body-content">
    <div class="g-unit g-first" id="side-nav">
      <div id="devdoc-nav"><?cs 
        include:"<?cs var:toroot ?><?cs call:licensecredits_toc() ?>" ?>
      </div>
    </div> <!-- end side-nav -->
<?cs /def ?>

<?cs # The default side navigation for the reference docs ?><?cs 
def:default_left_nav() ?>
  <div class="g-section g-tpl-240" id="body-content">
    <div class="g-unit g-first" id="side-nav">
      <div id="swapper">
        <div id="nav-panels">
          <div id="resize-packages-nav">
            <div id="packages-nav">
              <div id="index-links"><nobr>
                <a href="<?cs var:toroot ?>reference/packages.html" <?cs if:(page.title == "Package Index") ?>class="selected"<?cs /if ?> >Package Index</a> | 
                <a href="<?cs var:toroot ?>reference/classes.html" <?cs if:(page.title == "Class Index") ?>class="selected"<?cs /if ?>>Class Index</a></nobr>
              </div>
              <ul>
                <?cs call:package_link_list(docs.packages) ?>
              </ul><br/>
            </div> <!-- end packages -->
          </div> <!-- end resize-packages -->
          <div id="classes-nav"><?cs 
            if:subcount(class.package) ?>
            <ul>
              <?cs call:list("Interfaces", class.package.interfaces) ?>
              <?cs call:list("Classes", class.package.classes) ?>
              <?cs call:list("Enums", class.package.enums) ?>
              <?cs call:list("Exceptions", class.package.exceptions) ?>
              <?cs call:list("Errors", class.package.errors) ?>
            </ul><?cs 
            elif:subcount(package) ?>
            <ul>
              <?cs call:class_link_list("Interfaces", package.interfaces) ?>
              <?cs call:class_link_list("Classes", package.classes) ?>
              <?cs call:class_link_list("Enums", package.enums) ?>
              <?cs call:class_link_list("Exceptions", package.exceptions) ?>
              <?cs call:class_link_list("Errors", package.errors) ?>
            </ul><?cs 
            else ?>
              <script>
                /*addLoadEvent(maxPackageHeight);*/
              </script>
              <p style="padding:10px">Select a package to view its members</p><?cs 
            /if ?><br/>
          </div><!-- end classes -->
        </div><!-- end nav-panels -->
        <div id="nav-tree" style="display:none">
          <div id="index-links"><nobr>
            <a href="<?cs var:toroot ?>reference/packages.html" <?cs if:(page.title == "Package Index") ?>class="selected"<?cs /if ?> >Package Index</a> | 
            <a href="<?cs var:toroot ?>reference/classes.html" <?cs if:(page.title == "Class Index") ?>class="selected"<?cs /if ?>>Class Index</a></nobr>
          </div>
        </div><!-- end nav-tree -->
      </div><!-- end swapper -->
    </div> <!-- end side-nav -->
    <script>
      if (!isMobile) {
        $("<a href='#' id='nav-swap' onclick='swapNav();return false;' style='font-size:10px;line-height:9px;margin-left:1em;text-decoration:none;'><span id='tree-link'>Use Tree Navigation</span><span id='panel-link' style='display:none'>Use Panel Navigation</span></a>").appendTo("#side-nav");
        chooseDefaultNav();
        if ($("#nav-tree").is(':visible')) {
          init_default_navtree("<?cs var:toroot ?>");
        } else {
          addLoadEvent(function() {
            scrollIntoView("packages-nav");
            scrollIntoView("classes-nav");
          });
        }
        $("#swapper").css({borderBottom:"2px solid #aaa"});
      } else {
        swapNav(); // tree view should be used on mobile
      }
    </script><?cs 
/def ?>

<?cs 
def:custom_left_nav() ?><?cs 
  if:guide ?><?cs 
    call:guide_nav() ?><?cs 
  elif:licensecredits ?><?cs 
    call:licensecredits_tab_nav() ?><?cs 
  elif:codeqa?><?cs 
    call:codeqa_nav() ?><?cs 
  else ?><?cs 
    call:default_left_nav() ?><?cs 
  /if ?><?cs 
/def ?>
<?cs 
def:custom_copyright() ?>
  Except as noted, this content is licensed under <a
  href="http://www.apache.org/licenses/LICENSE-2.0">Apache 2.0</a>. 
  For details and restrictions, see the <a href="<?cs var:toroot ?>license.html">
  Content License</a>.<?cs 
/def ?>
<?cs 
def:custom_footerlinks() ?>
  <p>
    <a href="http://shareme.github.com">GrottWorkShop</a> -
    
  </p><?cs 
/def ?>
<?cs # appears on the right side  at the bottom off every page ?><?cs 
def:custom_buildinfo() ?>
  Android <?cs var:apilevel.top ?>&nbsp;r<?cs var:apilevel.bottom ?> - <?cs var:page.now ?>
<?cs /def ?>
